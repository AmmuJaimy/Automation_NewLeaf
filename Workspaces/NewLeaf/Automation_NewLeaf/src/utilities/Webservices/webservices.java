package utilities.Webservices;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import testng.*;

public class webservices {
	public static String result="";
	public static java.io.InputStream response;
	public static URLConnection connection;
	public static JSONArray array;
	public static String data="";
	
	static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}
	
	public static void createPost(String targetURL) throws MalformedURLException, IOException, JSONException {
		connection = new URL(targetURL).openConnection();
		//connection.setRequestProperty("Accept-Charset", charset);
		response = connection.getInputStream();
		result=convertStreamToString(response);
		try
		{
		
		array = new JSONArray(result);
		
		}
		catch(Exception e)
		{
			AutomateTestNG.DeveloperLog.info(e);
			
		}
	}
	public static String executePost(String targetURL,String key,int rownum) throws MalformedURLException, IOException, JSONException {
		createPost(targetURL);
		//for(int i=0;i<array.length();i++){
		    // System.out.println("RunMode  "+array.getJSONObject(i).getString("RunMode"));
		    // System.out.println("FirstName "+array.getJSONObject(i).getString("FirstName"));
		     //System.out.println("LastName  "+array.getJSONObject(i).getString("LastName"));
		     //JSONArray innerArray = array.getJSONObject(i);
		      //for(int k=0;k<innerArray.length();k++){
		        JSONObject innerObj = array.getJSONObject(rownum);
		        AutomateTestNG.DeveloperLog.info("Object "+rownum+"   "+innerObj);
		      //   System.out.println("RunMode "+innerObj.getString("RunMode"));
		      //   System.out.println("FirstName "+innerObj.getString("FirstName"));
		      //    System.out.println("LastName "+innerObj.getString("LastName"));    
		          data=innerObj.getString(key);
		  		AutomateTestNG.DeveloperLog.info(key+":"+data);
		  		//break;
		  		//return data;

		     // }
		//System.out.println(myObject1);
		//System.out.println(myObject1.get("FirstName"));
		//System.out.println(myObject1.get("LastName"));
		//String data=jsonobj.getString(key);
		//AutomateTestNG.DeveloperLog.info(key+":"+data);
		return data;
		}
	public static JSONObject ReturnPost(String targetURL,int rownum) throws MalformedURLException, IOException, JSONException {
		createPost(targetURL);
		//for(int i=0;i<array.length();i++){
		    // System.out.println("RunMode  "+array.getJSONObject(i).getString("RunMode"));
		    // System.out.println("FirstName "+array.getJSONObject(i).getString("FirstName"));
		     //System.out.println("LastName  "+array.getJSONObject(i).getString("LastName"));
		     //JSONArray innerArray = array.getJSONObject(i);
		      //for(int k=0;k<innerArray.length();k++){
		        JSONObject innerObj = array.getJSONObject(rownum);
		       
		  
		
		return innerObj;
		}
	public static int rownum(String targetURL)
	{
		return 1;
	}
	
	public static int arraylen(String targetURL)throws MalformedURLException, IOException, JSONException
	{
		createPost(targetURL);
		AutomateTestNG.DeveloperLog.info("Arraylength from Webservices" +array.length());
		return array.length();
	}
		}
	/*public static void main(String[] args) throws Exception {
		excutePost("http://52.72.40.149/DataService/GetData.ashx");
	}*/







