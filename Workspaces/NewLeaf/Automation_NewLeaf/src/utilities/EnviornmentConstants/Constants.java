//Method to define the constants used in the program including sheet names, file names column names
package utilities.EnviornmentConstants;

import java.io.FileNotFoundException;
import java.util.HashMap;

import org.apache.log4j.Logger;

import utilities.common.Xls_Reader;

public class Constants {
	
	public static String SCENARIO_PATH=null;
	public static String CONFIG_PATH=null;
	public static String NEWPURCHASE_PATH=null;
	public static String OR_PATH=null;
    public static String NEW_REFINANCE_PATH=null;
    public static String REPORT_WRITER_PATH=null;
    public static String REPORT_READER_PATH=null;
    public static String LOAN_NUMBER_WRITER_PATH=null;
    public static String REO_BOOK =null;
    public static String LOGFILE_PATH=null;
    public static String DEVLOGFILE_PATH=null;
    public static String CUSTOMREPORT_PATH=null;
    public static String NEW_PROSPECT_PURCHASE=null;
    public static String NEW_PROSPECT_REFINANCE=null;
    public static String RESULT_PATH=null;
	public static String source=null;
	public static String EXPECTEDRESULT_PATH=null;
	public static String DOWNLOADPATH=null;
	public static String DOWNLOADEDFILE=null;
	public static String COMPARISON_ARCHIVE_PATH=null;
	public static String RESULTS_ARCHIVE_PATH=null;
	public static String VALUES_PATH=null;
	public static String COMPARISON_PATH=null;
	public static String LENDINGTREE=null;
    //Satya Paths
    public static final String REPORT_COLUMN_SHEET="COLUMNNAMES";
    public static final String REPORT_COLUMN_NAMES="COLUMN NAMES";
    public static final String LOAN_NUMBERS_SHEET="LOAN_NUMBERS";
    public static final String LOAN_NUMBER="LOAN NUMBER";
    public static final String APPLICANT_NAME="APPLICANT NAME";
    public static final String PRODUCT="PRODUCT";
    
	//paths
	/*public static final String SCENARIO_PATH="D:\\Programming\\Version_Controlled\\Version_1.1\\Automation_NewLeaf\\Excel\\Test\\Scenario.xlsx";
	public static final String CONFIG_PATH="D:\\Programming\\Version_Controlled\\Version_1.1\\Automation_NewLeaf\\Excel\\Config\\config.xlsx";
	public static final String NEWPURCHASE_PATH="D:\\Programming\\Version_Controlled\\Version_1.1\\Automation_NewLeaf\\Excel\\Test\\NewPurchase.xlsx";
	public static final String OR_PATH="D:\\Programming\\Version_Controlled\\Version_1.1\\Automation_NewLeaf\\Excel\\Config\\OR_MASTER.xlsx";
    public static final String NEW_REFINANCE_PATH="D:\\Programming\\Version_Controlled\\Version_1.1\\Automation_NewLeaf\\Excel\\Test\\NewRefinance.xlsx";
	public static final String NEW_PROSPECT_PURCHASE="D:\\Programming\\Version_Controlled\\Version_1.1\\Automation_NewLeaf\\Excel\\Test\\NewProspectPurchase.xlsx";
    public static final String NEW_PROSPECT_REFINANCE="D:\\Programming\\Version_Controlled\\Version_1.1\\Automation_NewLeaf\\Excel\\Test\\NewProspectRefinance.xlsx";
    */
    
    //Hashmaps
    public static HashMap<String, String> hmap ;
	//Sheet names
	public static final String SCENARIO="Scenario";
	public static final String CONFIG="Config";
	public static final String TESTCASES="TESTCASES";
	public static final String TESTSTEPS="TESTSTEPS";
	public static final String OR_MASTER="OR_MASTER";
	public static final String CREATENEWPURCHASE="CreateNewPurchase";
	public static final String CREATEREFINANCE="CreateNewRefinance";
	public static final String LOGIN="LOGIN";
	public static final String CREATEPROSPECTPURCHASE="CreateNewProspetPurchase";
	public static final String CREATEPROSPECTREFINANCE="CreateNewProspectRefinance";
	public static String RESULTSHEET;
	public static String RESULT_SHEET;
	public static String RESULT_SHEET_ARCHIVE;
	public static final String EXPECTEDRESULTSHEET="EXPECTED_RESULT";
	public static String COMPARISON;
	public static String COMPARISON_ARCHIVE;
	//Column Names
	public static final String BROWSER="Browser";
	public static final String URL="URL";
	//public static final String SNAPSHOTS="D:\\Automation\\Screenshots\\Screenhots_1";
	public static final String SCID="SCID";
	public static final String RUNMODE="Runmode";
	public static final String TCID="TCID";
	public static final String ELEMENT="ELEMENT";
	public static final String ACTION="ACTION";
	public static final String DATAFILE="DATAFILE";
	public static final String VALIDATION="VALIDATION";
	public static final String OBJECT="ELEMENT";
	public static final String XPATH="XPATH";
	public static final String SNAPSHOT="Snapshots";
	public static final String DATA_SOURCE="DATA_SOURCE";
	public static final String TSID="TSID";
	public static final String TDID="TDID";
	public static final String ELEMENT_NAME="ELEMENT_NAME";

	public static final String EXPECTED_RESULT="EXPECTED_RESULT";
	public static final String ACTUAL_RESULT="ACTUAL_RESULT";
	public static final String PASSFAIL="PASS/FAIL";
	public static final String DESTINATIONDOWNLOAD="DOWNLOADDESTINATION";
	public static final String CONDITIONS="USER GIVEN CONDITIONS";
	//SCENARIOS
	public static final String NEWPURCHASE="NEWPURCHASE";
	public static final String NEWREFINANCE="NEWREFINANCE";
	public static final String NEWPROSPECTPURCHASE="NEWPROSPECTPURCHASE";
	public static final String NEWPROSPECTREFINANCE="NEWPROSPECTREFINANCE";
	public static final String LENDING_TREE="LENDINGTREE";
	//ACTION 
	public static final String OPENBROWSER="OpenBrowser";
	public static final String NAVIGATE="Navigate";
	public static final String MOUSEOVER="MouseOver";
	public static final String DROP_SELECT="Select";
	public static final String CLICK="Click";
	public static final String CHECK="CHECK";
	public static final String UNCHECK="Uncheck";
	public static final String TYPE="Type";
	public static final String TYPEZIP="TypeZip";
	public static final String RADIO_CLICK="Radio Click";
	public static final String TAKE_SCREENSHOT="Take Screenshot";
	public static final String GENERATE_REPORT="GenerateReport";
	public static final String GIVE_DELAY="Give Delay";
	public static final String ADD_PRODUCT="Add Product";
	public static final String GRAB_COMPARE="Grab and Compare";
	public static final String WAITTILLCLICKABLE="WaitTillClickable";
	public static final String SUBSTEPS="SubSteps";
	public static final String COMPAREXML="CompareXML";
	public static final String SWITCHTAB="SwitchTab";
	public static final String DOWNLOAD="Download";
	public static final String COMPAREWITHEXPECTED="ComparewithExpected";
	public static final String COMPARE="COMPARE";
	public static final String CLOSINGCOSTDETAILS="CLOSINGCOSTDETAILS";
	public static final String GRABFROMXML="GrabFromXML";
	public static final String GRABFROMUI="GrabFromUI";
	//LOG FILES
	// public static Logger DeveloperLog = null;
	 //public static Logger ErrorLog = null;
	 
	//REO SHEETS
	//public static final String REO_BOOK=
	public static final String OR="OR";
	public static final String REO_TESTSTEPS="TESTCASES";
	public static final String REO_PURCHASE="RealEstatePurchase";
	//public static final String REO_BOOK ="D:\\Programming\\Version_Controlled\\Version_1.1\\Automation_NewLeaf\\Excel\\Test\\REO.xlsx";;
	
	//REO COLUMNS
	public static final String OCCUPANCY_TYPE="Occupancy Type";
	
	public Constants(String path_location) throws FileNotFoundException {
		Xls_Reader allpathReader= new Xls_Reader(path_location);
		System.out.println("rown count"+allpathReader.getRowCount("PATHS"));
		SCENARIO_PATH=allpathReader.getCellData("PATHS","PATH",allpathReader.getCellRowNum("PATHS","NAMES","SCENARIO_PATH"));
		CONFIG_PATH=allpathReader.getCellData("PATHS","PATH",allpathReader.getCellRowNum("PATHS","NAMES","CONFIG_PATH"));
		NEWPURCHASE_PATH=allpathReader.getCellData("PATHS","PATH",allpathReader.getCellRowNum("PATHS","NAMES","NEWPURCHASE_PATH"));
		OR_PATH=allpathReader.getCellData("PATHS","PATH",allpathReader.getCellRowNum("PATHS","NAMES","OR_PATH"));
	    NEW_REFINANCE_PATH=allpathReader.getCellData("PATHS","PATH",allpathReader.getCellRowNum("PATHS","NAMES", "NEW_REFINANCE_PATH"));
	    NEW_PROSPECT_PURCHASE=allpathReader.getCellData("PATHS","PATH",allpathReader.getCellRowNum("PATHS","NAMES", "NEW_PROSPECT_PURCHASE"));
	    NEW_PROSPECT_REFINANCE=allpathReader.getCellData("PATHS","PATH",allpathReader.getCellRowNum("PATHS","NAMES", "NEW_PROSPECT_REFINANCE"));
	    REPORT_WRITER_PATH=allpathReader.getCellData("PATHS","PATH",allpathReader.getCellRowNum("PATHS","NAMES", "REPORT_WRITER_PATH"));
	    REPORT_READER_PATH=allpathReader.getCellData("PATHS","PATH",allpathReader.getCellRowNum("PATHS","NAMES", "REPORT_READER_PATH"));
	    LOAN_NUMBER_WRITER_PATH=allpathReader.getCellData("PATHS","PATH",allpathReader.getCellRowNum("PATHS","NAMES", "LOAN_NUMBER_WRITER_PATH"));
	    REO_BOOK=allpathReader.getCellData("PATHS","PATH",allpathReader.getCellRowNum("PATHS","NAMES", "REO_BOOK"));
	    LOGFILE_PATH=allpathReader.getCellData("PATHS","PATH",allpathReader.getCellRowNum("PATHS","NAMES","LOGFILE_PATH"));
	    DEVLOGFILE_PATH=allpathReader.getCellData("PATHS","PATH",allpathReader.getCellRowNum("PATHS","NAMES","DEVLOGFILE_PATH"));
	    CUSTOMREPORT_PATH=allpathReader.getCellData("PATHS","PATH",allpathReader.getCellRowNum("PATHS","NAMES","CUSTOMREPORT_PATH"));
	    RESULT_PATH=allpathReader.getCellData("PATHS","PATH",allpathReader.getCellRowNum("PATHS","NAMES","RESULT_PATH"));
	    EXPECTEDRESULT_PATH=allpathReader.getCellData("PATHS","PATH",allpathReader.getCellRowNum("PATHS","NAMES","EXPECTEDRESULT_PATH"));
	    COMPARISON_ARCHIVE_PATH=allpathReader.getCellData("PATHS","PATH",allpathReader.getCellRowNum("PATHS","NAMES","COMPARISON_ARCHIVE_PATH"));
	    RESULTS_ARCHIVE_PATH=allpathReader.getCellData("PATHS","PATH",allpathReader.getCellRowNum("PATHS","NAMES","RESULTS_ARCHIVE_PATH"));
	    VALUES_PATH=allpathReader.getCellData("PATHS","PATH",allpathReader.getCellRowNum("PATHS","NAMES","VALUES_PATH"));
	    COMPARISON_PATH=allpathReader.getCellData("PATHS","PATH",allpathReader.getCellRowNum("PATHS","NAMES","COMPARISON_PATH"));
	    LENDINGTREE=allpathReader.getCellData("PATHS","PATH",allpathReader.getCellRowNum("PATHS","NAMES","LENDINGTREE"));
	    // String source=source;
	}
	
}









