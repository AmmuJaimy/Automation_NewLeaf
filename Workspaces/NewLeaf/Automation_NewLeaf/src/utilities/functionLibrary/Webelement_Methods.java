// Class modified by Aruna @ May 23 2016 to write methods to call actions on Web Elements
//Modified
package utilities.functionLibrary;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

import applicationDependent.RunCredit;
import freemarker.template.utility.NumberUtil;
import testng.AutomateTestNG;
import utilities.EnviornmentConstants.Constants;
import utilities.common.Xls_Reader;
import org.apache.commons.lang3.math.NumberUtils.*;
public class Webelement_Methods {

		public static WebDriver openBrowser(String Browser) throws FileNotFoundException
		{
			WebDriver driver=null;
			if(driver==null)
			{
			if (Browser.equalsIgnoreCase("Firefox"))
			{
				driver = new FirefoxDriver();			
				driver.manage().window().maximize();
				driver.manage().window().setSize(new Dimension(1920,1080));		
				}
			else if(Browser.equalsIgnoreCase("Chrome")||Browser.equalsIgnoreCase("GoogleChrome"))
			{
				Xls_Reader config_reader=new Xls_Reader(Constants.CONFIG_PATH);
				Constants.DOWNLOADPATH=config_reader.getCellData(Constants.CONFIG,Constants.DESTINATIONDOWNLOAD,2);
				System.setProperty("webdriver.chrome.driver", "C:\\chromedriver_win32\\chromedriver.exe");
				//System.setProperty("webdriver.chrome.driver", "D:\\DropboxDhiraj\\Dropbox\\SandeepQAWork\\Automation\\chromedriver_win32\\chromedriver.exe");
				 System.out.println("Download destination   "+Constants.DOWNLOADPATH);
				 AutomateTestNG.DeveloperLog.info("Download destination   "+Constants.DOWNLOADPATH); 
				 HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
			       chromePrefs.put("profile.default_content_settings.popups", 0);
			       chromePrefs.put("download.default_directory", Constants.DOWNLOADPATH);
			       chromePrefs.put("safebrowsing.enabled", "true"); 
			       ChromeOptions options = new ChromeOptions();
			       HashMap<String, Object> chromeOptionsMap = new HashMap<String, Object>();
			       options.setExperimentalOption("prefs", chromePrefs);
			       options.addArguments("--test-type");
			       options.addArguments("user-data-dir=D:\\");
			       options.addArguments("--start-maximized");
			       DesiredCapabilities cap = DesiredCapabilities.chrome();
			       cap.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);
			       cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			       cap.setCapability(ChromeOptions.CAPABILITY, options);
			       System.out.println("Chrome Browser before driver");
			       AutomateTestNG.DeveloperLog.info("Chrome Browser before driver");
			       driver = new ChromeDriver(cap);
				 System.out.println("Chrome Browser before maximize");
				 AutomateTestNG.DeveloperLog.info("Chrome Browser before maximize");
				//driver.manage().window().maximize();
				 System.out.println("Chrome Browser");
				 AutomateTestNG.DeveloperLog.info("Chrome Browser");
				//driver.manage().window().setSize(new Dimension(1376,768));	
				 JavascriptExecutor js = (JavascriptExecutor) driver;
				 js.executeScript("document.body.style.zoom=0.9");
				//	action.keyDown(Keys.CONTROL).sendKeys(Keys.CONTROL, Keys.SUBTRACT);
				// AutomateTestNG.DeveloperLog.info("Completed Openbrowser successfully");
			}
			else if(Browser.equalsIgnoreCase("IE")||Browser.equalsIgnoreCase("InternetExplorer"))
			{
				System.setProperty("webdriver.ie.driver","C:\\Users\\Srinivas\\Downloads\\IEDriverServer.exe");
				driver = new InternetExplorerDriver();
				driver.manage().window().maximize();
				//driver.manage().window().setSize(new Dimension(1920,1080));		
				}
			}
			return driver;
		}
		
		public static WebDriver navigate(WebDriver driver, String Url,String TCID) throws Exception
		{
			driver.get(Url);
			CreateRESULT_SHEET(TCID);
			CreateWorkbooks(TCID);
			JavascriptExecutor javascript = (JavascriptExecutor) driver;
		    
			  //Get current page title
			  String pagetitle=(String)javascript.executeScript("return document.title");  
			//String hyperlink=driver.findElement(By.xpath(xpath)).getAttribute("href");
			System.out.println("Hyper link "+pagetitle);
			AutomateTestNG.DeveloperLog.info("Hyper link "+pagetitle);
			//WebElement html = driver.findElement(By.tagName("html"));
			
	    	return driver;
		}
		public static void CreateWorkbooks(String TCID) throws IOException
		{
			String timeStamp = new SimpleDateFormat("yyyyMMMdd_HHmmss").format(Calendar.getInstance().getTime());
			Constants.RESULT_SHEET = TCID + timeStamp;
			Constants.COMPARISON =TCID+ timeStamp;
			  System.out.println("Constants.RESULT_SHEET  "+Constants.RESULT_SHEET);
			  AutomateTestNG.DeveloperLog.info("Constants.RESULT_SHEET  "+Constants.RESULT_SHEET);
			Xls_Reader result_writer=new Xls_Reader(Constants.VALUES_PATH,Constants.RESULT_SHEET);
			result_writer.addSheet(Constants.RESULT_SHEET);
			result_writer.addColumn(Constants.RESULT_SHEET, Constants.TCID);
			result_writer.addColumn(Constants.RESULT_SHEET, Constants.TSID);
			result_writer.addColumn(Constants.RESULT_SHEET, Constants.TDID);
			result_writer.addColumn(Constants.RESULT_SHEET, Constants.ELEMENT_NAME);
			result_writer.addColumn(Constants.RESULT_SHEET, "VALUE");
			Xls_Reader result_writer1=new Xls_Reader(Constants.COMPARISON_PATH,Constants.COMPARISON);
			result_writer1.addSheet(Constants.COMPARISON);
			result_writer1.addColumn(Constants.COMPARISON, Constants.TCID);
			result_writer1.addColumn(Constants.COMPARISON, Constants.TSID);
			result_writer1.addColumn(Constants.COMPARISON, Constants.TDID);
			result_writer1.addColumn(Constants.COMPARISON, "ELEMENT_NAME_1");
			result_writer1.addColumn(Constants.COMPARISON,"ELEMENT_NAME_2");
			result_writer1.addColumn(Constants.COMPARISON, "VALUE_1");
			result_writer1.addColumn(Constants.COMPARISON, "VALUE_2");
			result_writer1.addColumn(Constants.COMPARISON, Constants.PASSFAIL);
			
		}
		public static void slider(WebDriver driver,String xpath) throws InterruptedException
		{
			WebElement slider = driver.findElement(By.xpath(xpath));
			WebElement slide=driver.findElement(By.xpath("//*[@id='ig-estproperty-value']/div/div/div[2]/div/div[2]")); 
			int width=slide.getSize().getWidth(); 
			 System.out.println("Wdth  "+width);
			 int currentloc=slider.getLocation().getX();
			 int currentY=slider.getLocation().getY();
			 System.out.println("Currentlocation  "+currentloc );
			 System.out.println("CurrentY  "+currentY);
			 //Using Action Class
		       Actions move = new Actions(driver);
		     /* Action dragAndDrop= move.clickAndHold(slider).moveByOffset(-(width/2),0).
               moveByOffset((int)((width/100)*10.638),0).
               release().build();
		       dragAndDrop.perform();*/
		       
		       move.dragAndDropBy(slider,(((int)(width/100)*52)-currentloc),0).build().perform();
		       int movedloc=slider.getLocation().getX();
				 System.out.println("Moved to location  "+movedloc);
		}
		public static void CreateRESULT_SHEET(String TCID) throws Exception
		{
			Xls_Reader result_writer=new Xls_Reader(Constants.RESULTS_ARCHIVE_PATH);
	 		String timeStamp = new SimpleDateFormat("yyyyMMMdd_HHmmss").format(Calendar.getInstance().getTime());
			Constants.RESULT_SHEET_ARCHIVE = TCID + timeStamp;	
			result_writer.addSheet(Constants.RESULT_SHEET_ARCHIVE);
			result_writer.addColumn(Constants.RESULT_SHEET_ARCHIVE, Constants.TCID);
			result_writer.addColumn(Constants.RESULT_SHEET_ARCHIVE, Constants.TSID);
			result_writer.addColumn(Constants.RESULT_SHEET_ARCHIVE, Constants.TDID);
			result_writer.addColumn(Constants.RESULT_SHEET_ARCHIVE, Constants.ELEMENT_NAME);
			result_writer.addColumn(Constants.RESULT_SHEET_ARCHIVE, "VALUE");
			
			Xls_Reader writer=new Xls_Reader("C:\\Users\\Martin George\\Desktop\\Ammu\\Rityush Technology\\Automation\\Jar\\OUTSIDE JAR\\Reports\\xmlcomparison.xlsx");
			//String timeStamp = new SimpleDateFormat("yyyyMMMdd_HHmmss").format(Calendar.getInstance().getTime());
			Constants.RESULTSHEET = TCID + timeStamp;	
			writer.addSheet(Constants.RESULTSHEET);
			writer.addColumn(Constants.RESULTSHEET, "ELEMENT_NAME");
			writer.addColumn(Constants.RESULTSHEET,"VALUE FROM WEBPAGE");
			writer.addColumn(Constants.RESULTSHEET, "VALUE FROM XML");
			writer.addColumn(Constants.RESULTSHEET, Constants.PASSFAIL);
			Constants.COMPARISON_ARCHIVE=TCID + timeStamp;	
			Xls_Reader result_writer1=new Xls_Reader(Constants.COMPARISON_ARCHIVE_PATH);
			result_writer1.addSheet(Constants.COMPARISON_ARCHIVE);
			result_writer1.addColumn(Constants.COMPARISON_ARCHIVE, Constants.TCID);
			result_writer1.addColumn(Constants.COMPARISON_ARCHIVE, Constants.TSID);
			result_writer1.addColumn(Constants.COMPARISON_ARCHIVE, Constants.TDID);
			result_writer1.addColumn(Constants.COMPARISON_ARCHIVE, "ELEMENT_NAME_1");
			result_writer1.addColumn(Constants.COMPARISON_ARCHIVE,"ELEMENT_NAME_2");
			result_writer1.addColumn(Constants.COMPARISON_ARCHIVE, "VALUE_1");
			result_writer1.addColumn(Constants.COMPARISON_ARCHIVE, "VALUE_2");
			result_writer1.addColumn(Constants.COMPARISON_ARCHIVE, Constants.PASSFAIL);
			//Xls_Reader result_writer2=new Xls_Reader("C:\\Users\\Martin George\\Desktop\\Ammu\\Rityush Technology\\Automation\\Jar\\OUTSIDE JAR\\Reports\\ExpectedComparison.xlsx");
		}
		//Method to take screenshot of the webpage
		public static void takeScreenshotMethod(WebDriver driver, String Destination)
				throws Exception {
				File f = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(f, new File(Destination));
				}
	 	public static void selectElementByVisibleTextMethod(WebElement element, String Name) {
			AutomateTestNG.DeveloperLog.info("Inside Select");
	 		Select selectitem = new Select(element);
			selectitem.selectByVisibleText(Name);
			}
	 	public static void selectElementByValueMethod(WebElement element, String value) {
	 			Select selectitem = new Select(element);
	 			selectitem.selectByValue(value);
	 			}

	 	public static String selectElementByIndexMethod(WebElement element, int index) {
	 			Select selectitem = new Select(element);
	 			selectitem.selectByIndex(index);
	 			WebElement option=selectitem.getFirstSelectedOption();
	 			
	 			return option.getText();
	 			}
	 	public static void Select_The_Checkbox(WebElement element) {
			try {
	            if (element.isSelected()) {
	               System.out.println("Checkbox: " + element + "is already selected");
	               AutomateTestNG.DeveloperLog.info("Checkbox: " + element + "is already selected");
	            } else {
	            	// Select the checkbox
	                element.click();
	            }
	        } catch (Exception e) {
	        	System.out.println("Unable to select the checkbox: " + element);
	        	AutomateTestNG.ErrorLog.error("Unable to select the checkbox: " + element);
	        }
			
		}
	 	public static void Checkbox(WebElement element,String data) 
	 	{
	 		if (data.equalsIgnoreCase("CHECK"))
	 			Select_The_Checkbox(element);
	 		else if (data.equalsIgnoreCase("UNCHECK"))
	 		{
	 			DeSelect_The_Checkbox(element);
	 		}
	 	/*	try
	 		{
	 			element.click();
	 		}
	 		catch(Exception e)
	 		{
	 			
	 		}*/
	 	}
	 	public static WebDriver GrabfromUI(WebDriver driver, String xpath,String data, String TSID,String TCID,String TDID) throws FileNotFoundException
	 	{
	 		String element_data=driver.findElement(By.xpath(xpath)).getText();
	 		Xls_Reader result_writer1=new Xls_Reader(Constants.RESULTS_ARCHIVE_PATH);
	 		int temp=result_writer1.getRowCount(Constants.RESULT_SHEET_ARCHIVE) + 1;
	 		result_writer1.setCellData(Constants.RESULT_SHEET_ARCHIVE,Constants.TCID,temp,TCID);
	 		result_writer1.setCellData(Constants.RESULT_SHEET_ARCHIVE,Constants.TSID,temp,TSID);
	 		result_writer1.setCellData(Constants.RESULT_SHEET_ARCHIVE,Constants.TDID,temp,TDID);
	 		result_writer1.setCellData(Constants.RESULT_SHEET_ARCHIVE,Constants.ELEMENT_NAME,temp,data);
	 		result_writer1.setCellData(Constants.RESULT_SHEET_ARCHIVE,"VALUE",temp,element_data);
	 		Constants.hmap.put(data, element_data);
	 		
	 		Xls_Reader result_writer=new Xls_Reader(Constants.VALUES_PATH);
	 		String sheetname=result_writer.getsheetname(0);
	 		int temp1=result_writer.getRowCount(sheetname) + 1;
	 		result_writer.setCellData(sheetname,Constants.TCID,temp1,TCID);
	 		result_writer.setCellData(sheetname,Constants.TSID,temp1,TSID);
	 		result_writer.setCellData(sheetname,Constants.TDID,temp1,TDID);
	 		result_writer.setCellData(sheetname,Constants.ELEMENT_NAME,temp1,data);
	 		result_writer.setCellData(sheetname,"VALUE",temp1,element_data);
	 		return driver;
	 		
	 	}
	 
	 	public static void GrabCompare (WebDriver driver,String xpath,String data,String TSID,String TCID,String TDID)throws Exception
	 	{
	 		System.out.println("Inside grab and dumb");
	 		Xls_Reader result_writer1=new Xls_Reader(Constants.RESULT_PATH);
	 		Xls_Reader expected_reader=new Xls_Reader(Constants.EXPECTEDRESULT_PATH);
	 		
	 		String element_data=driver.findElement(By.xpath(xpath)).getText();
	 		int temp=result_writer1.getRowCount(Constants.RESULT_SHEET) + 1;
	 		result_writer1.setCellData(Constants.RESULT_SHEET,Constants.TCID,temp,TCID);
	 		result_writer1.setCellData(Constants.RESULT_SHEET,Constants.TSID,temp,TSID);
	 		result_writer1.setCellData(Constants.RESULT_SHEET,Constants.TDID,temp,TDID);
	 		result_writer1.setCellData(Constants.RESULT_SHEET,Constants.ELEMENT_NAME,temp,data);
	 		result_writer1.setCellData(Constants.RESULT_SHEET,Constants.ACTUAL_RESULT,temp,element_data);
	 		System.out.println(" RESULT TCID  "+TCID);
 			System.out.println("RESULT TSID  "+TSID);
 			System.out.println("RESULT TDID  "+TDID);
 			System.out.println("RESULT expectedresult"+element_data);
 			AutomateTestNG.DeveloperLog.info(" RESULT TCID  "+TCID);
 			AutomateTestNG.DeveloperLog.info("RESULT TSID  "+TSID);
 			AutomateTestNG.DeveloperLog.info("RESULT TDID  "+TDID);
 			AutomateTestNG.DeveloperLog.info("RESULT expectedresult  "+element_data);
	 		for(int i=2;i<=expected_reader.getRowCount(Constants.EXPECTEDRESULTSHEET);i++)
	 		{
	 			String expected_tcid=expected_reader.getCellData(Constants.EXPECTEDRESULTSHEET, Constants.TCID, i);
	 			String expected_tsid=expected_reader.getCellData(Constants.EXPECTEDRESULTSHEET, Constants.TSID, i);
	 			String expected_expectedresult=expected_reader.getCellData(Constants.EXPECTEDRESULTSHEET, Constants.EXPECTED_RESULT, i);
	 			String expected_tdid=expected_reader.getCellData(Constants.EXPECTEDRESULTSHEET, Constants.TDID, i);
	 			
	 			if((TCID.equalsIgnoreCase(expected_tcid))&&(TSID.equalsIgnoreCase(expected_tsid))&&(TDID.equalsIgnoreCase(expected_tdid)))
	 			{
	 				System.out.println("Matching tcid and tsid");
	 				
	 				System.out.println(" Expected TCID  "+expected_tcid);
		 			System.out.println("Expected TSID  "+expected_tsid);
		 			System.out.println("Expected TDID  "+expected_tdid);
		 			System.out.println("Expected expectedresult"+expected_expectedresult);
AutomateTestNG.DeveloperLog.info("Matching tcid and tsid");
	 				
	 				AutomateTestNG.DeveloperLog.info(" Expected TCID  "+expected_tcid);
	 				AutomateTestNG.DeveloperLog.info("Expected TSID  "+expected_tsid);
	 				AutomateTestNG.DeveloperLog.info("Expected TDID  "+expected_tdid);
	 				AutomateTestNG.DeveloperLog.info("Expected expectedresult"+expected_expectedresult);
		 			result_writer1.setCellData(Constants.RESULT_SHEET,Constants.EXPECTED_RESULT,temp,expected_expectedresult);
	 				break;
	 			}
	 			else
	 				result_writer1.setCellData(Constants.RESULT_SHEET,Constants.EXPECTED_RESULT,temp,"NULL");
	 		}
	 		for(int i=2;i<=result_writer1.getRowCount(Constants.RESULT_SHEET);i++)
	 		{
	 			String expected_result=result_writer1.getCellData(Constants.RESULT_SHEET, Constants.EXPECTED_RESULT, i);
	 			String actual_result=result_writer1.getCellData(Constants.RESULT_SHEET, Constants.ACTUAL_RESULT, i);
	 			if(!expected_result.equalsIgnoreCase("NULL"))
	 			{
	 				String regexnum = "[0-9]+";
	 				String regexdec = "[0-9]+.[0-9]+";
	 				String regexper = "[0-9]+ %" ;
	 				String regexdecper ="[0-9]+.[0-9]+ %";
	 				if(((expected_result.matches(regexnum) || (expected_result.matches(regexper)) || (expected_result.matches(regexdec)) || (expected_result.matches(regexdecper)))&& ((actual_result.matches(regexnum)) || (actual_result.matches(regexper)) || (actual_result.matches(regexdec)) || actual_result.matches(regexdecper))))
	 				{
	 					AutomateTestNG.DeveloperLog.info("Inside the compare.. double detected");
	 					if(expected_result.matches(regexper) || (expected_result.matches(regexdecper)))
	 					{
	 						int p=expected_result.indexOf(' ');
	 						expected_result=expected_result.substring(0, p);
	 					}
	 					if(actual_result.matches(regexper) || actual_result.matches(regexdecper))
	 					{
	 						int q=actual_result.indexOf(' ');
	 						actual_result=actual_result.substring(0,q);
	 					}
	 					double exp_result=Double.parseDouble(expected_result);
	 					double act_result=Double.parseDouble(actual_result);
	 					AutomateTestNG.DeveloperLog.info("Actual result"+act_result);
	 					AutomateTestNG.DeveloperLog.info("Expected result"+exp_result);
	 					if(exp_result == act_result)
	 						result_writer1.setCellData(Constants.RESULT_SHEET, Constants.PASSFAIL, i, "PASS");
	 					else
	 						result_writer1.setCellData(Constants.RESULT_SHEET, Constants.PASSFAIL, i, "FAIL");
	 				}
	 				else
	 				if(expected_result.equalsIgnoreCase(actual_result))
	 					result_writer1.setCellData(Constants.RESULT_SHEET, Constants.PASSFAIL, i, "PASS");
	 				else
	 					result_writer1.setCellData(Constants.RESULT_SHEET, Constants.PASSFAIL, i, "FAIL");
	 			}
	 			else
	 				result_writer1.setCellData(Constants.RESULT_SHEET, Constants.PASSFAIL, i, "N/A");
	 		}
	 	}
	 	public static void Compare(Xls_Reader result_writer1,String sheetname,String first_element,String second_element,String first_value,String second_value,String TCID,String TSID,String TDID) throws FileNotFoundException
	 	{
	 		//Xls_Reader result_writer1=new Xls_Reader("C:\\Users\\Martin George\\Desktop\\Ammu\\Rityush Technology\\Automation\\Jar\\OUTSIDE JAR\\Reports\\Comparison.xlsx");
	 		Xls_Reader result_writer=new Xls_Reader(Constants.COMPARISON_PATH);	
	 		String sheetname1=result_writer.getsheetname(0);
	 		int temp1=result_writer.getRowCount(sheetname1) + 1;
	 		int temp=result_writer1.getRowCount(sheetname)+1;
	 			result_writer1.setCellData(sheetname,Constants.TCID,temp,TCID);
		 		result_writer1.setCellData(sheetname,Constants.TSID,temp,TSID);
		 		result_writer1.setCellData(sheetname,Constants.TDID,temp,TDID);
	 			result_writer1.setCellData(sheetname, "ELEMENT_NAME_1", temp, first_element);
	 			result_writer1.setCellData(sheetname, "ELEMENT_NAME_2", temp, second_element);
	 			result_writer1.setCellData(sheetname, "VALUE_1", temp, first_value);
	 			result_writer1.setCellData(sheetname, "VALUE_2", temp, second_value);
	 			result_writer.setCellData(sheetname1,Constants.TCID,temp1,TCID);
		 		result_writer.setCellData(sheetname1,Constants.TSID,temp1,TSID);
		 		result_writer.setCellData(sheetname1,Constants.TDID,temp1,TDID);
	 			result_writer.setCellData(sheetname1, "ELEMENT_NAME_1", temp1, first_element);
	 			result_writer.setCellData(sheetname1, "ELEMENT_NAME_2", temp1, second_element);
	 			result_writer.setCellData(sheetname1, "VALUE_1", temp1, first_value);
	 			result_writer.setCellData(sheetname1, "VALUE_2", temp1, second_value);
	 			if(!first_value.equals(null)&&(!second_value.equals(null)))
	 			{
	 				String regexnum = "^([0-9]+|[0-9]+.[0-9]+|[0-9]+ %|[0-9]+.[0-9]+ %|[0-9]+,[0-9]+|[0-9]+,[0-9]+.[0-9]+|[0-9]+)$";
	 				String regexdec = "";
	 				String regexper = "" ;
	 				String regexdecper ="";
	 				if((first_value.matches(regexnum) ||(second_value.matches(regexnum))))
	 				{
	 					AutomateTestNG.DeveloperLog.info("Inside the compare.. double detected");
	 					first_value=first_value.replaceAll("\\s+", "");
	 					second_value=second_value.replaceAll("\\s+", "");
	 					first_value=first_value.replaceAll("%","");
	 					second_value=second_value.replaceAll("%","");
	 					first_value=first_value.replaceAll(",","");
	 					second_value=second_value.replaceAll(",","");
	 					System.out.println(first_value);
	 					System.out.println(second_value);
	 					if(first_value.equals(""))
	 						first_value ="0.0000";
	 					if(second_value.equals(""))
	 						second_value="0.0000";
	 					
	 					
	 					double exp_result=Double.parseDouble(first_value);
	 					double act_result=Double.parseDouble(second_value);
	 					AutomateTestNG.DeveloperLog.info("Actual result"+act_result);
	 					AutomateTestNG.DeveloperLog.info("Expected result"+exp_result);
	 					if(exp_result == act_result){
	 						result_writer1.setCellData(sheetname, Constants.PASSFAIL, temp, "PASS");
	 						result_writer.setCellData(sheetname1, Constants.PASSFAIL, temp1, "PASS");
	 					}
	 					else
	 					{
	 						result_writer1.setCellData(sheetname, Constants.PASSFAIL, temp, "FAIL");
	 						result_writer.setCellData(sheetname1, Constants.PASSFAIL, temp1, "FAIL");
	 					}
	 					
	 					}
	 				else
	 				if(first_value.equalsIgnoreCase(second_value))
	 				{
	 					result_writer1.setCellData(sheetname, Constants.PASSFAIL, temp, "PASS");
	 					result_writer.setCellData(sheetname1, Constants.PASSFAIL, temp1, "PASS");
	 				}
	 				else
	 				{
	 					result_writer1.setCellData(sheetname, Constants.PASSFAIL, temp, "FAIL");
	 					result_writer.setCellData(sheetname1, Constants.PASSFAIL, temp1, "FAIL");
	 				}
	 			}
	 			else{
	 				result_writer1.setCellData(sheetname, Constants.PASSFAIL, temp, "N/A");
	 				result_writer.setCellData(sheetname1, Constants.PASSFAIL, temp1, "N/A");
	 			}
	 	}

		public static void DeSelect_The_Checkbox(WebElement element) {
			try {
	            if (element.isSelected()) {
	            	//De-select the checkbox
	                element.click();
	            } else {
	            	System.out.println("Checkbox: "+element+"is already deselected");
	            }
	        } catch (Exception e) {
	        	System.out.println("Unable to deselect checkbox: "+element);
	        }
	    }		
        public static WebDriver SelectRadioYesNo(WebDriver driver, String data, String xpath) {
			
			List <WebElement> navButton = driver.findElements(By.xpath(xpath));
			int count = navButton.size();
				System.out.println("count"+count);
			  	for(int i=0;i<count;++i)
			  	{
			  		String YesNoValue = navButton.get(i).getText();
			  		System.out.println("YesNoVAlue   " +YesNoValue);
			  		if (YesNoValue.toLowerCase().equals(data.toLowerCase()))
			  		{
			  			navButton.get(i).click();
			  			break;
			  		}
			  	}
			  	return driver;
			  }
        public static void enterText (WebElement element, String data)throws Exception {
	
        	
        	if(element.isEnabled())
        	{
				element.clear();
	
                  element.sendKeys(data);
                  Thread.sleep(300);
              }	
        }

        public static void ClickNext(WebDriver driver){
        	
               	String xpath = ".//button[contains(@ng-click,'nextPreviousNavigation')]";
	            List <WebElement> navButton = driver.findElements(By.xpath(xpath));
	            int count = navButton.size();
	
	              for (int i=0;i<count;++i)
	                 {
		               String str1 = navButton.get(i).getText();
		               if (str1.equals("Next"))
		                  {  
			                 navButton.get(i).click();
		}
	}
}
        	
		public static void Click(WebDriver driver, String xpath) {
			//Modified by Ammu on 26th May 2016 for introduing try catch loop
			try
			{
        	List <WebElement> navButton = driver.findElements(By.xpath(xpath));
        	System.out.println("Inside click n webelements");
        	int count = navButton.size();
        	// AutomateTestNG.DeveloperLog.info("Count of button  "+count);
        	AutomateTestNG.DeveloperLog.info(count);
        	System.out.println("Count of button  "+count);
        	 Actions builder = new Actions(driver);
        	
        	if(count>1)
			for (int x=0;x<count;++x)
			{
				String str1 = navButton.get(x).getText();
				//System.out.println(str1);
				if (str1.equals("Next"))
				{
					//navButton.get(x).click();
					//executor.executeScript("arguments[x].click();", navButton);
					builder.moveToElement(navButton.get(x), 1, 1).click().build().perform();
					AutomateTestNG.DeveloperLog.info("Clicked");
					break;
				}
			}
			
			else{
		//driver.findElement(By.xpath(xpath)).click();
			//	executor.executeScript("arguments[0].click();", navButton);
				//builder.moveToElement(driver.findElement(By.xpath(xpath)), 1, 1).click().build().perform();
				WebElement element= driver.findElement(By.xpath(xpath));

				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", element);
				AutomateTestNG.DeveloperLog.info("Clicked outside the count");
		System.out.println("Clicked outside the count");
			}
			
			}
		catch (Exception e)
			{
			AutomateTestNG.ErrorLog.error("Exception caught"+e);
			System.out.println("Exception caught"+e);
			System.out.println("The xpath caused exception"+xpath);
			}
		}
}



