package utilities.xml;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import utilities.EnviornmentConstants.Constants;
import utilities.common.Xls_Reader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
public class ReadXMLXPath {
	public static void GrabfromXML(Xls_Reader writer,String xpath,String element_name,String TCID,String TSID, String TDID) throws FileNotFoundException
	{
	  	
		DocumentBuilderFactory builderFactory =
		        DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		int temp=writer.getRowCount(Constants.RESULT_SHEET_ARCHIVE)+1;
		Xls_Reader result_writer=new Xls_Reader(Constants.VALUES_PATH);
		String sheetname=result_writer.getsheetname(0);
		int temp1=result_writer.getRowCount(sheetname) + 1;
 		result_writer.setCellData(sheetname,Constants.TCID,temp1,TCID);
 		result_writer.setCellData(sheetname,Constants.TSID,temp1,TSID);
 		result_writer.setCellData(sheetname,Constants.TDID,temp1,TDID);
 		
		writer.setCellData(Constants.RESULT_SHEET_ARCHIVE,Constants.TCID,temp,TCID);
 		writer.setCellData(Constants.RESULT_SHEET_ARCHIVE,Constants.TSID,temp,TSID);
 		writer.setCellData(Constants.RESULT_SHEET_ARCHIVE,Constants.TDID,temp,TDID);
		try {
			//Xls_Reader writer=new Xls_Reader("C:\\Users\\Martin George\\Desktop\\Ammu\\Rityush Technology\\Automation\\Jar\\OUTSIDE JAR\\Reports\\xmlcomparison.xlsx");
			builder = builderFactory.newDocumentBuilder();
		    Document document = builder.parse(new FileInputStream(Constants.DOWNLOADEDFILE));
		    XPath xPath =  XPathFactory.newInstance().newXPath();
		   // String expression = "/RESPONSE/PAYLOAD/CREATE_QUICK_GFE_RESPONSE/HudSummary/HudItem[@Name='SettlementEscrowRate']/@BuyerAmount";

		  //read a string value
		  String value = xPath.compile(xpath).evaluate(document);
		  System.out.println("The item is"+value);
		  
		  writer.setCellData(Constants.RESULT_SHEET_ARCHIVE, Constants.ELEMENT_NAME, temp, element_name);
		  writer.setCellData(Constants.RESULT_SHEET_ARCHIVE, "VALUE", temp, value);
		  result_writer.setCellData(sheetname,Constants.ELEMENT_NAME,temp1,element_name);
	 		result_writer.setCellData(sheetname,"VALUE",temp1,value);
		  Constants.hmap.put(element_name, value);
		} catch (Exception e) {
		    e.printStackTrace();  
		}
	}

}
