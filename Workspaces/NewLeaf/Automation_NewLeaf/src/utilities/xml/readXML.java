package utilities.xml;

import java.io.File;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.w3c.dom.Element;

import testng.AutomateTestNG;
import utilities.EnviornmentConstants.Constants;
import utilities.common.Xls_Reader;

public class readXML {
	 public static void readwritexml(Xls_Reader testreader,String subsheet){

	      try 
	      {	
	         File inputFile = new File(Constants.DOWNLOADEDFILE);
	         Xls_Reader writer=new Xls_Reader("C:\\Users\\Martin George\\Desktop\\Ammu\\Rityush Technology\\Automation\\Jar\\OUTSIDE JAR\\Reports\\xmlcomparison.xlsx");
	         DocumentBuilderFactory dbFactory= DocumentBuilderFactory.newInstance();
	         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	         Document doc = dBuilder.parse(inputFile);
	         doc.getDocumentElement().normalize();
	        // System.out.println("Root element :" 
	          //  + doc.getDocumentElement().getNodeName());
	         for(int i=2;i<=testreader.getRowCount(subsheet);i++)
	         {
	        	 String tagname=testreader.getCellData(subsheet,"TAGNAME" , i);
	        	 String attribute_match=testreader.getCellData(subsheet,"ATTRIBUTE_MATCH" , i);
	        	 String value_match=testreader.getCellData(subsheet,"VALUE_MATCH" , i);
	        	 String attribute_read=testreader.getCellData(subsheet,"ATTRIBUTE_READ" , i);
	        	 System.out.println("tagname    "+tagname);
	        	 System.out.println("attribute read    "+attribute_read);
	        	 NodeList nList = doc.getElementsByTagName(tagname);
	         
	        	 for (int temp = 0; temp < nList.getLength(); temp++)
	        	 {
	        		 Node nNode = nList.item(temp);
	        		 System.out.println("tagname    "+tagname);
		        	 System.out.println("attribute read    "+attribute_read);
	        		 if (nNode.getNodeType() == Node.ELEMENT_NODE) 
	        		 {
	        			 String value="";
	        			 System.out.println("attribute match   "+attribute_match);
	        			 String name= nList
	                    	.item(temp)
	                        .getAttributes().getNamedItem(attribute_match).getNodeValue();
	        			 System.out.println("Name     "+name);
	        			 System.out.println("value match   "+value_match);
	        			 if(name.equalsIgnoreCase(value_match))
	        			 {
	        				 //System.out.println("Match found ");
	        				 value= nList
	                    	.item(temp)
	                        .getAttributes().getNamedItem(attribute_read).getNodeValue();
	        			 
	        			
	        	
	        			// System.out.println("Name :"+name+"  Value:"+value);
	        			 writer.setCellData(Constants.RESULTSHEET, "VALUE FROM XML", i, value);
	        			 break;
	        			 }
	        			 }
	        	 }
	         }
	         for(int i=2;i<=writer.getRowCount(Constants.RESULTSHEET);i++)
		 		{
		 			String expected_result=writer.getCellData(Constants.RESULTSHEET, "VALUE FROM WEBPAGE", i);
		 			String actual_result=writer.getCellData(Constants.RESULTSHEET, "VALUE FROM XML", i);
		 			if(!expected_result.equalsIgnoreCase("NULL"))
		 			{
		 				String regexnum = "[0-9]+";
		 				String regexdec = "[0-9]+.[0-9]+";
		 				String regexper = "[0-9]+ %" ;
		 				String regexdecper ="[0-9]+.[0-9]+ %";
		 				if(((expected_result.matches(regexnum) || (expected_result.matches(regexper)) || (expected_result.matches(regexdec)) || (expected_result.matches(regexdecper)))&& ((actual_result.matches(regexnum)) || (actual_result.matches(regexper)) || (actual_result.matches(regexdec)) || actual_result.matches(regexdecper))))
		 				{
		 					AutomateTestNG.DeveloperLog.info("Inside the compare.. double detected");
		 					if(expected_result.matches(regexper) || (expected_result.matches(regexdecper)))
		 					{
		 						int p=expected_result.indexOf(' ');
		 						expected_result=expected_result.substring(0, p);
		 					}
		 					if(actual_result.matches(regexper) || actual_result.matches(regexdecper))
		 					{
		 						int q=actual_result.indexOf(' ');
		 						actual_result=actual_result.substring(0,q);
		 					}
		 					double exp_result=Double.parseDouble(expected_result);
		 					double act_result=Double.parseDouble(actual_result);
		 					AutomateTestNG.DeveloperLog.info("Actual result"+act_result);
		 					AutomateTestNG.DeveloperLog.info("Expected result"+exp_result);
		 					if(exp_result == act_result)
		 						writer.setCellData(Constants.RESULTSHEET, Constants.PASSFAIL, i, "PASS");
		 					else
		 						writer.setCellData(Constants.RESULTSHEET, Constants.PASSFAIL, i, "FAIL");
		 				}
		 				else
		 				if(expected_result.equalsIgnoreCase(actual_result))
		 					writer.setCellData(Constants.RESULTSHEET, Constants.PASSFAIL, i, "PASS");
		 				else
		 					writer.setCellData(Constants.RESULTSHEET, Constants.PASSFAIL, i, "FAIL");
		 			}
		 			else
		 				writer.setCellData(Constants.RESULTSHEET, Constants.PASSFAIL, i, "N/A");
		 		}
	         
	      }
	      catch (Exception e) 
	      {
	         e.printStackTrace();
	      }
	
      } 
}		
	


