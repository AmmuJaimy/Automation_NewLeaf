package testng;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.testng.IReporter;
import org.testng.IResultMap;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.xml.XmlSuite;

import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.NetworkMode;

import applicationDependent.ReportWriters;
import applicationDependent.ReportWriters.*;
import utilities.EnviornmentConstants.Constants;
import utilities.common.GenerateZip;
import utilities.common.SendMail;
import utilities.common.Xls_Reader;
public class CustomReporter implements IReporter {
	ExtentReports extent;
	public int i=0;
	@Override
	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
		String timeStamp = new SimpleDateFormat("yyyyMMMdd_HHmmss").format(Calendar.getInstance().getTime());
		File reportFile= new File(Constants.LOAN_NUMBER_WRITER_PATH);
		String folder=reportFile.getParent();
		String report=folder+"\\CustomReport"+timeStamp;
		String htmlreport=report+".html";
		//String extentreportpath=folder+"\\extentreports";
		//String zippath=report+".zip";
		//System.out.println("Zip path"+zippath);
		extent =  new ExtentReports(htmlreport, true, DisplayOrder.OLDEST_FIRST, NetworkMode.OFFLINE, Locale.ENGLISH);
		//AutomateTestNG.ErrorLog.info("Custom report generated");
		System.out.println("Custom report generated");
		for (ISuite suite : suites) {
			Map<String, ISuiteResult> result = suite.getResults();

			for (ISuiteResult r : result.values()) {
				ITestContext context = r.getTestContext();

				buildTestNodes(context.getPassedTests(), LogStatus.PASS);
				buildTestNodes(context.getFailedTests(), LogStatus.FAIL);
				buildTestNodes(context.getSkippedTests(), LogStatus.SKIP);
			}
		}

		extent.flush();
		extent.close();
		try {
			//GenerateZip.zipAll(htmlreport,extentreportpath,zippath);
			//SendMail.execute(zippath);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AutomateTestNG.ErrorLog.info("Exception caught"+e.getMessage());
		}

	}

	private void buildTestNodes(IResultMap tests, LogStatus status) {
		ExtentTest test;

		if (tests.size() > 0) {
			for (ITestResult result : tests.getAllResults()) {
				String testcasename=(String) AutomateTestNG.data[i][0];
				i++;
				test= extent.startTest(testcasename);
				test.getTest().setStartedTime(getTime(result.getStartMillis()));
				test.getTest().setEndedTime(getTime(result.getEndMillis()));

				for (String group : result.getMethod().getGroups())
					test.assignCategory(group);
				//test.setDescription(testcasename);
				String message = "Test " + status.toString().toLowerCase() + "ed";
				Xls_Reader config = null;
				try {
					config = new Xls_Reader(Constants.CONFIG_PATH);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					AutomateTestNG.ErrorLog.info("Exception caught"+e.getMessage());
				}
				String screenshotfolder= config.getCellData(Constants.CONFIG,"Snapshots",2);
				test.log(status,"<a href=\""+screenshotfolder+"\">"+"<span='label info'>SCREENSHOTS</span></a>");
				File reportFile= new File(Constants.LOAN_NUMBER_WRITER_PATH);
				String folder=reportFile.getParent();
				test.log(status,"<a href=\""+folder+"\">"+"<span='label info'>REPORTS</span></a>");
				//test.log(status,"<a href=\"#\">Expand Details <iframe src=\""+errorlog+"\" height='200' weight='200'>DESRCIPTION</iframe></a>");
				//test.log(status, "<iframe src=\""+errorlog+"\" height='200' weight='200'>DESRCIPTION</iframe>");
				//test.log(status,testcasename);
				//test.log(status, ReportWriters.dummyLoannumber);
				if (result.getThrowable() != null)
				{
					String errorlog=Constants.LOGFILE_PATH;
					test.log(status.ERROR,"<a href=\""+errorlog+"\">"+"<span='label info'>ERROR LOG</span></a>");
					test.log(status.ERROR,"Error");
					//+AutomateTestNG.test_reader.getCellData("TESTSTEPS","Description", AutomateTestNG.errorRowNum);
				}  
				extent.endTest(test);
			}
		}
	}
	private Date getTime(long millis) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(millis);
		return calendar.getTime();        
	}

}
