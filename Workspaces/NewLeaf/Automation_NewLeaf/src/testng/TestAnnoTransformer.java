package testng;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Properties;

import utilities.EnviornmentConstants.*;
import org.testng.IAnnotationTransformer;
import org.testng.annotations.ITestAnnotation;

import utilities.EnviornmentConstants.Constants;
import utilities.common.Xls_Reader;

public class TestAnnoTransformer implements IAnnotationTransformer {
	@Override
	public void transform (ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod)
	  {
		//public Xls_Reader config_reader= new Xls_Reader(Constants.CONFIG_PATH);;
		try
		{
		Properties prop = new Properties();
		String propFileName = "setup.properties";
		ClassLoader loader = Thread.currentThread().getContextClassLoader();           
		InputStream inputStream = loader.getResourceAsStream(propFileName);
		prop.load(inputStream);	
		String paths_location = prop.getProperty("paths_location");
		//Constants.source=prop.getProperty("source");
		
		Constants c = new Constants(paths_location);
		Xls_Reader config_reader=new Xls_Reader(Constants.CONFIG_PATH);
		Constants.source=config_reader.getCellData(Constants.CONFIG,Constants.DATA_SOURCE,2);
		System.out.println("Source"+Constants.source);
		System.out.println("Annotation function"+testMethod.getName());
		if ((testMethod.getName()).equalsIgnoreCase("automateLoanCenter")&&Constants.source.equalsIgnoreCase("Webservice")) {
            System.out.println("testname in annotation"+testMethod.getName());
			annotation.setEnabled(false);
	  }
		if ((testMethod.getName()).equalsIgnoreCase("automateLoanCenterJSON")&&Constants.source.equalsIgnoreCase("ExcelSheet")) {
            System.out.println("testname in annotation"+testMethod.getName());
			annotation.setEnabled(false);
	  }
		}
		catch(Exception e)
		{
		}
		}
	  }
		
		



