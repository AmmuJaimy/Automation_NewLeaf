package testng;
//To be exported
import org.testng.annotations.Test;

import applicationDependent.ReportWriters;
import applicationDependent.RunCredit;
import utilities.Webservices.*;
import org.apache.poi.xssf.usermodel.*;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import utilities.EnviornmentConstants.Constants;
import utilities.common.WaitTool;
import utilities.common.Xls_Reader;
import utilities.functionLibrary.Webelement_Methods;
import utilities.xml.*;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Properties;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class AutomateTestNG {
	public Xls_Reader or_reader;
	public Xls_Reader config_reader;
	public Xls_Reader scenario_reader;
	public Xls_Reader test_reader;
	public Xls_Reader test_reader_NewPurchase;
	public Xls_Reader test_reader_NewRefinance;
	public WebDriver driver=null;
	public String scenario=null;
	public ArrayList<String> testcaseid=new ArrayList<String>();
	public static Logger DeveloperLog = null;
	public static Logger ErrorLog = null;
	public static Object[][] data;
    public String xpath="";
    public String data1="";
    public String element="";
    public String lookupsheet="";
    public String lookupcol="";
    public String action="";
    @Test(dataProvider = "dataProvider")
	public void automateLoanCenterJSON(String testcaseid, JSONObject dataRow,int dataSheetRowNum) throws Exception {
		System.out.println("Testcase is"+testcaseid);
		AutomateTestNG.DeveloperLog.info("Testcase is"+testcaseid);
		String occupancy_type=null;
		config_reader=new Xls_Reader(Constants.CONFIG_PATH);
		Constants.hmap=new HashMap<String, String>();
		for(int i=2; i<=test_reader.getRowCount(Constants.TESTSTEPS);i++)//Loop to iterate through Teststeps sheet
		{
			String tcid=test_reader.getCellData(Constants.TESTSTEPS, Constants.TCID, i);//Read TCID from the teststeps sheet sequentially
			if(tcid.equalsIgnoreCase(testcaseid))//Checks whether the TCID from teststeps sheet and testcase sheet match
			{
				xpath="";
				lookupcol="";
				lookupsheet="";
				String lookupfile="";
				element=test_reader.getCellData(Constants.TESTSTEPS, Constants.ELEMENT, i);//Read the element corresponding to the Test step
				action= test_reader.getCellData(Constants.TESTSTEPS, Constants.ACTION, i);//Read the action corresponding to the teststep
				String tsid=test_reader.getCellData(Constants.TESTSTEPS, Constants.TSID, i);
				String condition=test_reader.getCellData(Constants.TESTSTEPS, Constants.CONDITIONS, i);
				System.out.println("Condition  " +condition);
				System.out.println("action"+action);
				System.out.println("value of orid"+element);
				AutomateTestNG.DeveloperLog.info("Condition  " +condition);
				AutomateTestNG.DeveloperLog.info("Action   "+action);
				AutomateTestNG.DeveloperLog.info("Value of orid  "+element);
				data1="";
				lookupsheet= test_reader.getCellData(Constants.TESTSTEPS, Constants.TCID, i);//Reads the TCID as the name of the datasheet
				String datafile=test_reader.getCellData(Constants.TESTSTEPS,Constants.DATAFILE,i);//Reads the datafile column to look for the datasheet and column pipe seperated values
				System.out.println("Lookupsheet"+lookupsheet);
				System.out.println("datafile"+datafile);
				AutomateTestNG.DeveloperLog.info("Lookupsheet  "+lookupsheet);
				AutomateTestNG.DeveloperLog.info("Datafile  "+datafile);
				int p=datafile.indexOf('|');
				if((!(datafile.equals("")))&& (!(action.equalsIgnoreCase(Constants.GIVE_DELAY))) && (!(action.equalsIgnoreCase(Constants.COMPARE))) && (!(action.equalsIgnoreCase(Constants.GRABFROMUI))) &&(!(action.equalsIgnoreCase(Constants.GRABFROMXML))))
				{
					lookupcol=datafile.substring(p+1);
					lookupfile=datafile.substring(0, p);
	
					System.out.println("Lookupfile   "+lookupfile);
					System.out.println("Row count of lookupsheet   "+test_reader.getRowCount(lookupsheet));
					AutomateTestNG.DeveloperLog.info("Lookupfile   "+lookupfile);
					AutomateTestNG.DeveloperLog.info("Row count of lookupsheet   "+test_reader.getRowCount(lookupsheet));
				
				}
				
				
				if(!(lookupfile.equalsIgnoreCase("Result"))){
				if((!(lookupcol.equals("")))&&(!(lookupfile.equalsIgnoreCase("Config"))))
				{
					System.out.println("Lookup col not null");
					AutomateTestNG.DeveloperLog.info("Lookup col not null");
					if((tcid.equalsIgnoreCase("Login"))&&(lookupfile.equalsIgnoreCase("Login")))
						data1=webservices.executePost("http://52.72.40.149/Login/Login.ashx",lookupcol,dataSheetRowNum);
					else
					{
						TDID=test_reader.getCellData(lookupsheet, Constants.TDID, dataSheetRowNum);
						data1=webservices.executePost("http://52.72.40.149/DataService/GetData.ashx",lookupcol,dataSheetRowNum);
						if(lookupcol.equalsIgnoreCase(Constants.OCCUPANCY_TYPE))
						{
							occupancy_type=data1;
						}
					}
						System.out.println("data from webservice" +data1);
						AutomateTestNG.DeveloperLog.info("Data from webservice" +data1);
				}
				}
				if((! (condition.equals("")))&& (!(condition.equals(null))))
				{
					int index=condition.indexOf('=');
					String rhs=condition.substring(index+1);
					String lhs=condition.substring(0, index);
					AutomateTestNG.DeveloperLog.info("Condition as given in the teststeps  " +condition);
					AutomateTestNG.DeveloperLog.info("Lhs  " +lhs);
					AutomateTestNG.DeveloperLog.info("Rhs  " +rhs);
					
					String datasheetvalue=test_reader.getCellData(tcid, lhs, dataSheetRowNum);;
					AutomateTestNG.DeveloperLog.info("Value of RHS as from Datasheet  " +datasheetvalue);
					if(!(datasheetvalue.matches(rhs)))
					{
						AutomateTestNG.DeveloperLog.info("Condition doesnot match");
						continue;
					}
					
				}
				teststeps(tcid,tsid,occupancy_type,i);
			}	
	
	
		}	
		System.out.println("The created hashmap is "+Constants.hmap);
		AutomateTestNG.DeveloperLog.info("The created hashmap is "+Constants.hmap);
		
	}
	public String validation="";
    public String TDID="";
	@Test(dataProvider = "dataProvider")
	public void automateLoanCenter(String testcaseid, XSSFRow dataRow) throws Exception {
		int i=0;
		//try{
		System.out.println("Testcase is"+testcaseid);
		System.out.println("Row is"+dataRow.getRowNum());
		AutomateTestNG.DeveloperLog.info("Testcase is  "+testcaseid);
		
		int dataSheetRowNum=dataRow.getRowNum();
		Constants.hmap=new HashMap<String, String>();
		String occupancy_type=null;
		config_reader=new Xls_Reader(Constants.CONFIG_PATH);
		for(i=2; i<=test_reader.getRowCount(Constants.TESTSTEPS);i++)//Loop to iterate through Teststeps sheet
		{
			String tcid=test_reader.getCellData(Constants.TESTSTEPS, Constants.TCID, i);//Read TCID from the teststeps sheet sequentially
			if(tcid.equalsIgnoreCase(testcaseid))//Checks whether the TCID from teststeps sheet and testcase sheet match
			{
				xpath="";
				element=test_reader.getCellData(Constants.TESTSTEPS, Constants.ELEMENT, i);//Read the element corresponding to the Test step
				action= test_reader.getCellData(Constants.TESTSTEPS, Constants.ACTION, i);//Read the action corresponding to the teststep
				String tsid=test_reader.getCellData(Constants.TESTSTEPS, Constants.TSID, i);
				String condition=test_reader.getCellData(Constants.TESTSTEPS, Constants.CONDITIONS, i);
				System.out.println("Value f ORID  "+element);
				System.out.println("Condition  " +condition);
				AutomateTestNG.DeveloperLog.info("Value of ORID  "+element);
				AutomateTestNG.DeveloperLog.info("Condition as given in the teststeps " +condition);
				data1="";
				AutomateTestNG.DeveloperLog.info("Action is "+action);
				lookupsheet= test_reader.getCellData(Constants.TESTSTEPS, Constants.TCID, i);//Reads the TCID as the name of the datasheet
				String datafile=test_reader.getCellData(Constants.TESTSTEPS,Constants.DATAFILE,i);//Reads the datafile column to look for the datasheet and column pipe seperated values
				int p=datafile.indexOf('|');
				String lookupsheet1=null;
				AutomateTestNG.DeveloperLog.info("Datafile   "+datafile);
				if((!(datafile.equals("")))&& (!(action.equalsIgnoreCase(Constants.GIVE_DELAY))) && (!(action.equalsIgnoreCase(Constants.COMPARE))) && (!(action.equalsIgnoreCase(Constants.GRABFROMUI))) &&(!(action.equalsIgnoreCase(Constants.GRABFROMXML))))
				{
				lookupcol=datafile.substring(p+1);
				lookupsheet1=datafile.substring(0, p);
				}
				
				if((!lookupsheet.equals(null))||(!lookupcol.equals(null)))
				{
					TDID=test_reader.getCellData(lookupsheet, Constants.TDID, dataSheetRowNum);
					data1=test_reader.getCellData(lookupsheet, lookupcol, dataSheetRowNum);
					if(lookupcol.equalsIgnoreCase(Constants.OCCUPANCY_TYPE))
					{
						occupancy_type=data1;
					}
					}
				if((! (condition.equals("")))&& (!(condition.equals(null))))
				{
					if(condition.contains("AND"))
					{
					String st[]=condition.split(" AND | OR ");
					System.out.println("Length  "+st.length);
	
					int checkAND=0;
					for(int il=0;il<st.length;il++)
					{
					
					int index=st[il].indexOf('=');
					String rhs=st[il].substring(index+1);
					String lhs=st[il].substring(0, index);
					AutomateTestNG.DeveloperLog.info("Condition as given in the teststeps  " +condition);
					AutomateTestNG.DeveloperLog.info("lhs  " +lhs);
					AutomateTestNG.DeveloperLog.info("rhs  " +rhs);
					
					String datasheetvalue=test_reader.getCellData(tcid, lhs, dataSheetRowNum);;
					AutomateTestNG.DeveloperLog.info("Value of RHS from the datasheet  " +datasheetvalue);
					if(!(datasheetvalue.matches(rhs)))
					{
						AutomateTestNG.DeveloperLog.info("Condition doesnot match");
						System.out.println("Condition doesnot match");
						checkAND=1;
						break;
					}
					
					}
					if(checkAND==1)
					{
						AutomateTestNG.DeveloperLog.info("Condition doesnot match");
						System.out.println("Condition doesnot match");
						continue;
					}
					
						
					}
					else if(condition.contains("OR"))
					{
					String st[]=condition.split(" AND | OR ");
					System.out.println("Length  "+st.length);
					
					int checkOR=0;
					for(int il=0;il<st.length;il++)
					{
						int index=st[il].indexOf('=');
					String rhs=st[il].substring(index+1);
					String lhs=st[il].substring(0, index);
					AutomateTestNG.DeveloperLog.info("Condition as given in the teststeps " +condition);
					AutomateTestNG.DeveloperLog.info("lhs  " +lhs);
					AutomateTestNG.DeveloperLog.info("rhs  " +rhs);
					
					String datasheetvalue=test_reader.getCellData(tcid, lhs, dataSheetRowNum);;
					AutomateTestNG.DeveloperLog.info("Value of RHS as in the Datasheet  " +datasheetvalue);
					if((datasheetvalue.matches(rhs)))
					{
						AutomateTestNG.DeveloperLog.info("Condition doesnot match");
						System.out.println("Condition doesnot match");
						checkOR=1;
						break;
					}
					
					}
					if(checkOR==0)
					{
						AutomateTestNG.DeveloperLog.info("Condition doesnot match");
						System.out.println("Condition doesnot match");
						continue;
					}
					}
					else
					{
						
							
							int index=condition.indexOf('=');
							String rhs=condition.substring(index+1);
							String lhs=condition.substring(0, index);
							AutomateTestNG.DeveloperLog.info("Condition as given in the teststeps " +condition);
							AutomateTestNG.DeveloperLog.info("lhs  " +lhs);
							AutomateTestNG.DeveloperLog.info("rhs  " +rhs);
							
							String datasheetvalue=test_reader.getCellData(tcid, lhs, dataSheetRowNum);;
							AutomateTestNG.DeveloperLog.info("Value of RHS as in the datasheet  " +datasheetvalue);
							if(!(datasheetvalue.matches(rhs)))
							{
								AutomateTestNG.DeveloperLog.info("Condition doesnot match");
								System.out.println("Condition doesnot match");
								continue;
							}
					}
						
				}
				System.out.println("Data1   "+data1);
				AutomateTestNG.DeveloperLog.info("Data from datasheet   "+data1);
				validation=test_reader.getCellData("TESTSTEPS", "VALIDATION",i);
				AutomateTestNG.DeveloperLog.info("Validation   "+validation);
				teststeps(tcid,tsid,occupancy_type,i);

			}	


		}	
		System.out.println("The created hashmap is "+Constants.hmap);
		AutomateTestNG.DeveloperLog.info("The created hashmap is "+Constants.hmap);
	}

	public void teststeps(String tcid,String tsid,String occupancy_type,int rownum)throws Exception
	{
		AutomateTestNG.DeveloperLog.info("TSID   "+tsid);
		if(!element.equals(null)&&(! (action.equalsIgnoreCase(Constants.COMPARE))))
			xpath=readORMaster(element);
		if (action.equalsIgnoreCase(Constants.OPENBROWSER)){
			driver= Webelement_Methods.openBrowser(config_reader.getCellData(Constants.CONFIG,Constants.BROWSER,2));
		}
		else if(action.equalsIgnoreCase(Constants.NAVIGATE))		{
			String Url= config_reader.getCellData(Constants.CONFIG,Constants.URL,2);
			driver= Webelement_Methods.navigate(driver,Url,scenario);
			//String validation=test_reader.getCellData(Constants.TESTSTEPS, Constants.VALIDATION,i);
			System.out.println("Validation  "+validation);
			AutomateTestNG.DeveloperLog.info("Validation for this step  "+validation);
			if(!validation.equals(null))
			{
				String validation_xpath=readORMaster(validation);
				System.out.println("validation xpath  "+validation_xpath);
				AutomateTestNG.DeveloperLog.info("validation xpath  "+validation_xpath);
				if(!validation_xpath.equals(null))
					WaitTool.waitForElementPresent(driver, By.xpath(validation_xpath),20);
			}

		}
		else if(action.equalsIgnoreCase(Constants.MOUSEOVER)){
			try
			{
				//WebElement html = driver.findElement(By.tagName("html"));
				
				
				WebElement menu=driver.findElement(By.xpath(xpath));
				Actions action1=new Actions(driver);
				action1.moveToElement(menu).perform();
				if(!validation.equals(null))
				{
					String validation_xpath=readORMaster(validation);
					if(!validation_xpath.equals(null))
						WaitTool.waitForElementPresent(driver, By.xpath(validation_xpath),20);
				}
				//if(element.equalsIgnoreCase("button_Loan"))
				/*{
					System.out.println("Button loan.. zoom out");
					for(int i=0;i<2;i++ )
					{
						System.out.println("Button loan.. zoom out.. begin ");
					html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
					System.out.println("Button loan.. zoom out done ");
					}
					}*/
			}
			catch(Exception e)
			{
				AutomateTestNG.ErrorLog.info("The exception caught is at step "+tsid);
				AutomateTestNG.ErrorLog.info("The error is "+e);
				
			}
		}
		else if(action.equalsIgnoreCase(Constants.CLICK))			{

			try
			{
				// Function call modified by Aruna @ May 23 2016
				String Button_Text=driver.findElement(By.xpath(xpath)).getText();
				Webelement_Methods.Click(driver,xpath);
				System.out.println("Button Text is"+Button_Text);
				AutomateTestNG.DeveloperLog.info("Button Text is  "+Button_Text);
				String savexpath=readORMaster("button_Save");
				if(Button_Text.contains("New"))
				{
					if(driver.findElements(By.xpath(savexpath)).size()>0)
					{
						driver.findElement(By.xpath(savexpath)).click();
					}
				}
			}
			catch(Exception e)
			{
				AutomateTestNG.ErrorLog.info("The exception caught is at step "+tsid);
				AutomateTestNG.ErrorLog.info("The error is "+e);
			}
			String validation_xpath=readORMaster(validation);
			if(!validation.equals(null))
			{
				if(validation.equalsIgnoreCase("button_CreditScore"))
				{
					WaitTool.waitForElementClickable(driver, By.xpath(validation_xpath),120);

					WebElement elementcredit=driver.findElement(By.xpath("//*[@id='CreditTab']/a/div"));
					if(!(elementcredit.isSelected()))
					{
						driver=RunCredit.REO(driver,scenario,occupancy_type);
					}
				}
				else
				{
					if(!validation_xpath.equals(null))
						WaitTool.waitForElementPresent(driver, By.xpath(validation_xpath),360);
						
				}
			}

		}
		else if (action.equalsIgnoreCase(Constants.CHECK))
		{
			//Modified by Shirisha on 21/05/2016 Start
			Webelement_Methods.Checkbox(driver.findElement(By.xpath(xpath)),data1);
		}
		else if(action.equalsIgnoreCase(Constants.TYPE))
		{       // Function call written by Aruna @ May 23 2016
			System.out.println("data  "+data1);
			Webelement_Methods.enterText(driver.findElement(By.xpath(xpath)),data1);
		}
		else if (action.equalsIgnoreCase(Constants.DROP_SELECT)){
			// Function call written by Satya @ April 30 2016
			Webelement_Methods.selectElementByVisibleTextMethod(driver.findElement(By.xpath(xpath)),data1);
		}
		else if(action.equalsIgnoreCase("ClickNext"))
		{
			// Function call modified by Aruna @ May 23 2016
			Webelement_Methods.ClickNext(driver);
			WaitTool.waitForElementPresent(driver, By.xpath(validation),20);
		}
		else if(action.equalsIgnoreCase(Constants.RADIO_CLICK))
			// Function call written by Aruna @ May 23 2016
			driver = Webelement_Methods.SelectRadioYesNo(driver,data1,xpath);
		else if(action.equalsIgnoreCase(Constants.TYPEZIP))
		{
			// Function call written by Aruna @ May 23 2016
			Webelement_Methods.enterText(driver.findElement(By.xpath(xpath)),data1);
		}
		else if(action.equalsIgnoreCase(Constants.TAKE_SCREENSHOT))
		{
			String DestFolder=config_reader.getCellData(Constants.CONFIG,Constants.SNAPSHOT,2);
			String timeStamp = new SimpleDateFormat("yyyyMMMdd_HHmmss").format(Calendar.getInstance().getTime());
			String Destination = DestFolder+"\\"+"TCID"+tcid+timeStamp+".png";
			Webelement_Methods.takeScreenshotMethod(driver, Destination);
		}
		else if(action.equalsIgnoreCase("Slider"))
			
		{
			AutomateTestNG.DeveloperLog.info("Inside slider");
			Webelement_Methods.slider(driver,xpath);
		}
		else if (action.equalsIgnoreCase(Constants.SWITCHTAB))
		{
			System.out.println("Inside switch tab   ");
			ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
			   
		   // driver.findElement(By.linkText("Twitter Advertising Blog")).click();
			
			    System.out.println("Inside switch tab   aray size"+tabs.size());
			    AutomateTestNG.DeveloperLog.info("Inside switch tab   aray size"+tabs.size());
			    System.out.println(tabs);
			    AutomateTestNG.DeveloperLog.info(tabs);
			    Thread.sleep(5000);
			    
			 driver.switchTo().window(tabs.get(0));
			    System.out.println(driver.getCurrentUrl());
			    AutomateTestNG.DeveloperLog.info("The current URL is  "+driver.getCurrentUrl());
			    driver.switchTo().window(tabs.get(1));
			    //  System.out.println(driver.getTitle());
		    // Do what you want here, you are in the new tab
		   String escrow=driver.findElement(By.xpath(xpath)).getText();
		   System.out.println("Settlement Escrow   "+escrow);
		    //action.keyDown(Keys.CONTROL).keyDown(Keys.SHIFT).sendKeys(Keys.TAB).build().perform();
		}
		else if (action.equalsIgnoreCase(Constants.GRAB_COMPARE))
		{
			System.out.println("Data "+data1);
			Webelement_Methods.GrabCompare(driver,xpath,lookupcol,tsid,tcid,TDID);
		}
		else if(action.equalsIgnoreCase(Constants.GENERATE_REPORT))
		{
			Thread.sleep(1000);
			ReportWriters.GenerateReport(driver,tcid);
		}
		else if(action.equalsIgnoreCase(Constants.GIVE_DELAY))
			{
				System.out.println("Inside give delay");
				AutomateTestNG.DeveloperLog.info("Inside give delay");
				String time="5000";
				for(int i=2;i<=test_reader.getRowCount(Constants.TESTSTEPS);i++)
				{
					String teststepid=test_reader.getCellData(Constants.TESTSTEPS, Constants.TSID, i);
					if(teststepid.equalsIgnoreCase(tsid))
						{
						time=test_reader.getCellData(Constants.TESTSTEPS, Constants.DATAFILE, i);
						System.out.println("Waittime is "+time);
						AutomateTestNG.DeveloperLog.info("Waittime is "+time);
						break;
						}
					
				
				}
				System.out.println("time is "+time);
			AutomateTestNG.DeveloperLog.info("time is "+time);
				if(time.equalsIgnoreCase(""))
					time="5000";
				long waittime=Long.parseLong(time);
				System.out.println("Waittime is "+waittime);
				AutomateTestNG.DeveloperLog.info("Waittime is "+waittime);
				Thread.sleep(waittime); 
				
			}
		else if(action.equalsIgnoreCase(Constants.DOWNLOAD))
		{
			ReportWriters.download(driver, xpath);
			Thread.sleep(1000);
		}
		else if(action.equals(Constants.COMPAREWITHEXPECTED))
		{
			Xls_Reader result_writer1=new Xls_Reader(Constants.COMPARISON_ARCHIVE_PATH);
			Xls_Reader expected_reader=new Xls_Reader(Constants.EXPECTEDRESULT_PATH);
			String elements_to_be_compared=test_reader.getCellData(Constants.TESTSTEPS, Constants.DATAFILE, rownum);
			int p=elements_to_be_compared.indexOf('|');
			String first_element=null;
			String second_element=null;
			
			AutomateTestNG.DeveloperLog.info("Datafile   "+elements_to_be_compared);
			if(!(elements_to_be_compared.equals("")))
			{
			first_element=elements_to_be_compared.substring(p+1);
			second_element=elements_to_be_compared.substring(0, p);
			}
			String second_value=Constants.hmap.get(second_element);
			String first_value=null;
			for(int i=2;i<=expected_reader.getRowCount(Constants.EXPECTEDRESULTSHEET);i++)
	 		{
	 			String expected_tcid=expected_reader.getCellData(Constants.EXPECTEDRESULTSHEET, Constants.TCID, i);
	 			String expected_tsid=expected_reader.getCellData(Constants.EXPECTEDRESULTSHEET, Constants.TSID, i);
	 			String expected_expectedelement=expected_reader.getCellData(Constants.EXPECTEDRESULTSHEET,"ELEMENT_NAME", i);
	 			String expected_tdid=expected_reader.getCellData(Constants.EXPECTEDRESULTSHEET, Constants.TDID, i);
	 			
	 			if((tcid.equalsIgnoreCase(expected_tcid))&&(tsid.equalsIgnoreCase(expected_tsid))&&(TDID.equalsIgnoreCase(expected_tdid)) &&(first_element.equalsIgnoreCase(expected_expectedelement)))
	 			{
	 				System.out.println("Matching tcid and tsid");
	 				first_value=expected_reader.getCellData(Constants.EXPECTEDRESULTSHEET, "EXPECTED_RESULT", i);
	 				System.out.println(" Expected TCID  "+expected_tcid);
		 			System.out.println("Expected TSID  "+expected_tsid);
		 			System.out.println("Expected TDID  "+expected_tdid);
		 			System.out.println("Expected expectedresult"+expected_expectedelement);
		 			AutomateTestNG.DeveloperLog.info(" Expected TCID  "+expected_tcid);
		 			AutomateTestNG.DeveloperLog.info("Expected TSID  "+expected_tsid);
		 			AutomateTestNG.DeveloperLog.info("Expected TDID  "+expected_tdid);
		 			AutomateTestNG.DeveloperLog.info("Expected expectedresult   "+expected_expectedelement);
	 				break;
	 			}
	 			
	 		}
			
			Webelement_Methods.Compare(result_writer1,Constants.COMPARISON_ARCHIVE,first_element,second_element ,  first_value,second_value,tcid,tsid,TDID);
		}
		else if(action.equalsIgnoreCase(Constants.COMPARE))
				{
					//String Workbook=test_reader.getCellData(Constants.TESTSTEPS, Constants.ELEMENT, rownum);
					Xls_Reader result_writer1=new Xls_Reader(Constants.COMPARISON_ARCHIVE_PATH);
					String elements_to_be_compared=test_reader.getCellData(Constants.TESTSTEPS, Constants.DATAFILE, rownum);
					int p=elements_to_be_compared.indexOf('|');
					String first_element=null;
					String second_element=null;
					
					AutomateTestNG.DeveloperLog.info("Datafile   "+elements_to_be_compared);
					if(!(elements_to_be_compared.equals("")))
					{
					first_element=elements_to_be_compared.substring(p+1);
					second_element=elements_to_be_compared.substring(0, p);
					}
					String first_value=null;
					String second_value=null;
					first_value=Constants.hmap.get(first_element);
					second_value=Constants.hmap.get(second_element);
					
					Webelement_Methods.Compare(result_writer1,Constants.COMPARISON_ARCHIVE,first_element,second_element ,  first_value,second_value,tcid,tsid,TDID);
				}
		else if (action.equalsIgnoreCase(Constants.CLOSINGCOSTDETAILS))
		{
			driver=ReportWriters.ClosingCostSubmenu(driver,  lookupcol, data1, test_reader);
		}
		else if(action.contains(Constants.SUBSTEPS))
		{
			String substepfile="";
			for(int i=2;i<=test_reader.getRowCount(Constants.TESTSTEPS);i++)
			{
				String teststepid=test_reader.getCellData(Constants.TESTSTEPS, Constants.TSID, i);
				if(teststepid.equalsIgnoreCase(tsid))
					{
					substepfile=test_reader.getCellData(Constants.TESTSTEPS, Constants.ELEMENT, i);
					System.out.println("Substepfile is "+substepfile);
					AutomateTestNG.DeveloperLog.info("Substepfile is "+substepfile);
					break;
					}
			}
			ReportWriters.ClosingCost(driver, substepfile, lookupcol, data1, test_reader);
		}
		else if(action.equalsIgnoreCase(Constants.COMPAREXML))
		{
			String substepfile="";
			for(int i=2;i<=test_reader.getRowCount(Constants.TESTSTEPS);i++)
			{
				String teststepid=test_reader.getCellData(Constants.TESTSTEPS, Constants.TSID, i);
				String testcaseid1=test_reader.getCellData(Constants.TESTSTEPS, Constants.TCID, i);
				if(teststepid.equalsIgnoreCase(tsid) && testcaseid1.equalsIgnoreCase(tcid))
					{
					substepfile=test_reader.getCellData(Constants.TESTSTEPS, Constants.ELEMENT, i);
					System.out.println("Substepfile is "+substepfile);
					AutomateTestNG.DeveloperLog.info("Substepfile is "+substepfile);
					break;
					}
			}
			readXML.readwritexml(test_reader, substepfile);
		}
		else if(action.equalsIgnoreCase(Constants.GRABFROMXML))
		{
			String elementname="";
			Xls_Reader result_writer1=new Xls_Reader(Constants.RESULTS_ARCHIVE_PATH);
			for(int i=2;i<=test_reader.getRowCount(Constants.TESTSTEPS);i++)
			{
				String teststepid=test_reader.getCellData(Constants.TESTSTEPS, Constants.TSID, i);
				String testcaseid1=test_reader.getCellData(Constants.TESTSTEPS, Constants.TCID, i);
				if(teststepid.equalsIgnoreCase(tsid) && testcaseid1.equalsIgnoreCase(tcid))
					{
					elementname=test_reader.getCellData(Constants.TESTSTEPS, Constants.DATAFILE, i);
					System.out.println("ELEMENT_NAME is "+elementname);
					break;
					}
				
			
			}
			ReadXMLXPath.GrabfromXML(result_writer1,xpath, elementname,tcid,tsid,TDID);
		}
		else if(action.equalsIgnoreCase(Constants.GRABFROMUI))
		{
			String elementname="";
			for(int i=2;i<=test_reader.getRowCount(Constants.TESTSTEPS);i++)
			{
				String testcaseid1=test_reader.getCellData(Constants.TESTSTEPS, Constants.TCID, i);
				String teststepid=test_reader.getCellData(Constants.TESTSTEPS, Constants.TSID, i);
				if(teststepid.equalsIgnoreCase(tsid) && testcaseid1.equalsIgnoreCase(tcid))
					{
					elementname=test_reader.getCellData(Constants.TESTSTEPS, Constants.DATAFILE, i);
					System.out.println("ELEMENT_NAME is "+elementname);
					break;
					}
				
			
			}
			driver=Webelement_Methods.GrabfromUI(driver, xpath, elementname, tsid, tcid, TDID);
		}
		else if(action.equalsIgnoreCase(Constants.ADD_PRODUCT))
		{
			ReportWriters.AddProduct(driver, lookupcol,data1);
			if(!validation.equals(null))
			{
				String validation_xpath=readORMaster(validation);
				if(!validation_xpath.equals(null))
					WaitTool.waitForElementPresent(driver, By.xpath(validation_xpath),20);
			}
		}
		else if(action.equalsIgnoreCase(Constants.WAITTILLCLICKABLE))
		{
			WaitTool.waitForElementClickable(driver, By.xpath(xpath),20);
			System.out.println("button is clickable");
			AutomateTestNG.DeveloperLog.info("Button is clickable");
		}
		else
		{
			System.out.println("No Such Action");	
			AutomateTestNG.DeveloperLog.info("Button is clickable");
		}
	}

	@DataProvider
	public Object[][] dataProvider() throws Exception {
		if(Constants.source.equalsIgnoreCase("Webservice"))
		{
			AutomateTestNG.DeveloperLog.info("Returning from webservice");
			return dataProviderJSON();
		}

		else 
		{
			AutomateTestNG.DeveloperLog.info("Returning from Excelsheet");

			return dataProviderExcel();
		}
	}

	public Object[][] dataProviderJSON() throws Exception
	{
		int arraySize=0;

		arraySize=webservices.arraylen("http://52.72.40.149/DataService/GetData.ashx");

		AutomateTestNG.DeveloperLog.info("Arraysize  "+arraySize);
		data=new Object[arraySize+1][3];
		AutomateTestNG.DeveloperLog.info("Arraysize  "+arraySize);
		int a=0;
		int flag=0;
		for(int p=0;p<=arraySize;p++)
		{


			if(p==0)//if(testcaseid.get(i).contentEquals("Login"))
			{
				data[a][0]=testcaseid.get(0);
				data[a][1]=webservices.ReturnPost("http://52.72.40.149/Login/Login.ashx", p);
				data[a][2]=p;

			}
			else
			{
				data[a][0]=testcaseid.get(1);
				data[a][1]=webservices.ReturnPost("http://52.72.40.149/DataService/GetData.ashx", p-1);
				data[a][2]=p-1;
			}
			AutomateTestNG.DeveloperLog.info("data provider   "+data[a][1]);
			a++;
		}

		return data;
	}

	public Object[][] dataProviderExcel()
	{
		int arraySize=0;
		AutomateTestNG.DeveloperLog.info("Testcaseid.size "+testcaseid.size()  );
		for(int i=0;i<testcaseid.size();i++)
		{
			for(int p=2;p<=test_reader.getRowCount(testcaseid.get(i));p++)
			{
				String data_runmode=test_reader.getCellData(testcaseid.get(i), "RunMode", p);
				String data_firstname=test_reader.getCellData(testcaseid.get(i), "LoanType", p);
				//System.out.println("value of execute"+data_runmode);
				if(data_runmode.equals("Y"))
				{
					AutomateTestNG.DeveloperLog.info("Testcaseid  "+testcaseid.get(i));
					AutomateTestNG.DeveloperLog.info("Firstname  "+data_firstname);
					arraySize++;
					System.out.println("row is"+p);
				}
			}
		}
		AutomateTestNG.DeveloperLog.info("Arraysize  "+arraySize);
		data=new Object[arraySize][2];
		int a=0;
		for(int i=0;i<testcaseid.size();i++)
		{
			for(int p=2;p<=test_reader.getRowCount(testcaseid.get(i));p++)
			{
				String data_runmode=test_reader.getCellData(testcaseid.get(i), "RunMode", p);
				String data_firstname=test_reader.getCellData(testcaseid.get(i), "LoanType", p);
				if(data_runmode.equals("Y"))
				{	System.out.println("testcase id  "+testcaseid.get(i));
					AutomateTestNG.DeveloperLog.info("data provider before assignment    "+test_reader.getRow(testcaseid.get(i),p));
					data[a][0]=testcaseid.get(i);
					data[a][1]=test_reader.getRow(testcaseid.get(i),p);
					System.out.println("row is"+p);
					AutomateTestNG.DeveloperLog.info("testcsase    "+data[a][0]);
					AutomateTestNG.DeveloperLog.info("data provider   "+data[a][1]);
					AutomateTestNG.DeveloperLog.info("value of a  "+a);
					//AutomateTestNG.DeveloperLog.info("Firstname  "+data_firstname);
					a++;
				}
			}

		}

		return data;
	}
	@AfterTest
	public void aftertest() throws Exception
	{
		//System.out.println("The created hashmap is "+Constants.hmap);
		driver.close();
	}
	
	@BeforeTest
	public void beforeTest() throws IOException {
		Properties prop = new Properties();
	
		
		String propFileName = "setup.properties";
		ClassLoader loader = Thread.currentThread().getContextClassLoader();           
		InputStream inputStream = loader.getResourceAsStream(propFileName);
		prop.load(inputStream);		
		
		String paths_location = prop.getProperty("paths_location");
		Constants c = new Constants(paths_location);
		config_reader=new Xls_Reader(Constants.CONFIG_PATH);
		Constants.source=config_reader.getCellData(Constants.CONFIG,Constants.DATA_SOURCE,2);
		System.setProperty("logfile.name",Constants.LOGFILE_PATH);
		System.setProperty("logfile1.name",Constants.DEVLOGFILE_PATH);
		DeveloperLog = Logger.getLogger("developerlog");
		ErrorLog = Logger.getLogger("errorlog");
		AutomateTestNG.DeveloperLog.info("Inside Before Test");
		or_reader= new Xls_Reader(Constants.OR_PATH);
		config_reader= new Xls_Reader(Constants.CONFIG_PATH);
		AutomateTestNG.DeveloperLog.info("Moving to read scenario Test");
		scenario_reader= new Xls_Reader(Constants.SCENARIO_PATH);
		try
		{
			String scenario_runmode=null;
			for(int i=2;i<=scenario_reader.getRowCount(Constants.SCENARIO);i++)//Loop to iterate through the scenario sheet
			{
				scenario_runmode=scenario_reader.getCellData(Constants.SCENARIO, Constants.RUNMODE, i);//to read the runmode for each scenario
				if(scenario_runmode.equalsIgnoreCase("Y"))//To identify SCID marked with Y
				{
					scenario=scenario_reader.getCellData(Constants.SCENARIO, Constants.SCID, i);//Get the SCID
					scenario=scenario.toUpperCase();
					break;
				}
			}
			AutomateTestNG.DeveloperLog.info("Inside try Scenario..."+scenario);
			//scenario=readexcel.readScenario();
			AutomateTestNG.DeveloperLog.info("Scenario..."+scenario);
			//String testcasefile=scenario+".xlsx";
			//test_reader=new Xls_Reader(testcasefile);
			if(scenario.equalsIgnoreCase(Constants.NEWPURCHASE)) {
				test_reader=new Xls_Reader(Constants.NEWPURCHASE_PATH);
			} else if(scenario.equalsIgnoreCase(Constants.NEWREFINANCE)){
				test_reader=new Xls_Reader(Constants.NEW_REFINANCE_PATH);
			}else if(scenario.equalsIgnoreCase(Constants.NEWPROSPECTPURCHASE)){
				test_reader=new Xls_Reader(Constants.NEW_PROSPECT_PURCHASE);
			}else if(scenario.equalsIgnoreCase(Constants.NEWPROSPECTREFINANCE)){
				test_reader=new Xls_Reader(Constants.NEW_PROSPECT_REFINANCE);
				
			}
			else if(scenario.equalsIgnoreCase(Constants.LENDING_TREE))
			{
				test_reader=new Xls_Reader(Constants.LENDINGTREE);
			
			}
			String testreader_runmode=null;
			for(int l=2;l<=test_reader.getRowCount(Constants.TESTCASES);l++)//Loop to iterate through the Testcase sheet
			{
				testreader_runmode = test_reader.getCellData(Constants.TESTCASES, Constants.RUNMODE, l);//to read the runmode for each testcase
				//System.out.println("RUnmode"+testreader_runmode);
				AutomateTestNG.DeveloperLog.info("RUnmode"+testreader_runmode);
				if(testreader_runmode.equalsIgnoreCase("Y"))//Identify Testcase marked with runmode Y
				{
					String tcid=test_reader.getCellData(Constants.TESTCASES, Constants.TCID, l);
					testcaseid.add(tcid);
					//System.out.println(testcaseid.size());
					AutomateTestNG.DeveloperLog.info(testcaseid.size());
				}
			}
		}
		catch(Exception e){
			//System.out.println("Exception caught"+e.getMessage());
			AutomateTestNG.ErrorLog.info("Exception caught"+e.getMessage());

		}


	}
	public static String readORMaster(String orid)
	{
		try
		{
			Xls_Reader or_reader= new Xls_Reader(Constants.OR_PATH);
			String xpath=null;
			for(int k=2;k<=or_reader.getRowCount(Constants.OR_MASTER);k++)

			{
				if(orid.equalsIgnoreCase(or_reader.getCellData(Constants.OR_MASTER, Constants.ELEMENT, k)))
				{

					xpath= or_reader.getCellData(Constants.OR_MASTER,Constants.XPATH,k);
					break;
				}
			} 
			return xpath;

		}catch(Exception e)
		{
			AutomateTestNG.ErrorLog.error("Exception caught  "+e);
			return "";
		}
	}

}


