package applicationDependent;


import java.io.File;
import java.util.List;
import testng.*;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;


import utilities.EnviornmentConstants.Constants;
import utilities.common.WaitTool;
import utilities.common.Xls_Reader;
import utilities.functionLibrary.Webelement_Methods;

public class RunCredit {
	public static WebDriver REO(WebDriver driver,String scenario,String occupancy_type){
	String xpath="//div[contains(@ng-click,'realEstate.editRealEstateItem')]";
	try
	{
		
		AutomateTestNG.DeveloperLog.info("Inside REO    ");
	List <WebElement> navButton = driver.findElements(By.xpath(xpath));
	//String textbutton=driver.findElement(By.xpath(xpath)).getText();
	//System.out.println("The text on the button is"+textbutton);
	int count = navButton.size();
	String testcaseid=null;
	AutomateTestNG.DeveloperLog.info("Count of button  "+count);
	AutomateTestNG.DeveloperLog.info("SCENARIO  "+scenario);
	if(count==0 && scenario.equalsIgnoreCase(Constants.NEWREFINANCE))
	{
				
				
		        testcaseid="AddItem";
				WebElement additem=driver.findElement(By.xpath("//div[contains(@ng-click,'realEstate.addRealEstateItem')]"));
		        additem.click();
				AutomateTestNG.DeveloperLog.info("Clicked on ADDITEM" +testcaseid);
				Xls_Reader reo_reader= new Xls_Reader(Constants.REO_BOOK);
				//reo_reader.setCellData(sheetName, colName, rowNum, data)
				write_data_sheet(reo_reader,"AddItem",occupancy_type);
				int k = 1;
				driver=readTestSteps(reo_reader,driver,k,testcaseid);		
		}	
	
	else if(count>0 && scenario.equalsIgnoreCase(Constants.NEWREFINANCE))
	{
		testcaseid="RealEstateRefinance";
		for (int x=0;x<count;++x)
		{
			AutomateTestNG.DeveloperLog.info("Inside NEWREFI  "+testcaseid);
			String str1 = navButton.get(x).getText();
			AutomateTestNG.DeveloperLog.info(str1);
			    int k=x+1;
				navButton.get(x).click();
				AutomateTestNG.DeveloperLog.info("Clicked on REO info");
				Xls_Reader reo_reader= new Xls_Reader(Constants.REO_BOOK);
				write_data_sheet(reo_reader,"RealEstateRefinance",occupancy_type);
				driver=readTestSteps(reo_reader,driver,k,testcaseid);
				AutomateTestNG.DeveloperLog.info("Clicked on done    " +x);
		}	
	}
	
	else
	{
      testcaseid="RealEstatePurchase";
	for (int x=0;x<count;++x)
	{
		AutomateTestNG.DeveloperLog.info("Inside pdf loop  "+testcaseid);
		String str1 = navButton.get(x).getText();
		AutomateTestNG.DeveloperLog.info(str1);
		    int k=x+1;
			navButton.get(x).click();
			AutomateTestNG.DeveloperLog.info("Clicked on REO info");
			//String existing_property_xpath="//*[@id='body']/div[4]/div/div/div/form/fieldset/div/div[1]/div/div[2]/div/select";
			
			
			Xls_Reader reo_reader= new Xls_Reader(Constants.REO_BOOK);
			occupancy_type=reo_reader.getCellData("RealEstatePurchase", Constants.OCCUPANCY_TYPE, 2);
			write_data_sheet(reo_reader,"RealEstatePurchase",occupancy_type);
			driver=readTestSteps(reo_reader,driver,k,testcaseid);
			//String donepath="//*[@id='body']/div[4]/div/div/div/form/div[2]/button[2]";
			//driver.findElement(By.xpath(donepath)).click();
			AutomateTestNG.DeveloperLog.info("Clicked on done    " +x);
	}
	}
	}
	catch (Exception e)
	{
		
	}
	return driver;
}
	public static void write_data_sheet(Xls_Reader reader,String sheetname,String occupancy_type)
	{
		//if(!(sheetname.equalsIgnoreCase("RealEstatePurchase")))
		{
			AutomateTestNG.DeveloperLog.info("Inside writestep" +occupancy_type);
			reader.setCellData(sheetname, Constants.OCCUPANCY_TYPE, 2, occupancy_type);
			reader.setCellData(sheetname, Constants.OCCUPANCY_TYPE, 3, occupancy_type);
	
		}
	}
	//Function to read Teststeps	
	  public static  WebDriver readTestSteps(Xls_Reader test_reader,WebDriver driver,int x,String testcaseid) throws Exception
	  {
		  
		 //LoadURL lu=new LoadURL();
		  AutomateTestNG.DeveloperLog.info("Row count of TESTSTEPS"+test_reader.getRowCount(Constants.REO_TESTSTEPS));
			for(int i=2; i<=test_reader.getRowCount(Constants.REO_TESTSTEPS);i++)//Loop to iterate through Teststeps sheet
			{
				String tcid=test_reader.getCellData(Constants.REO_TESTSTEPS, Constants.TCID, i);//Read TCID from the teststeps sheet sequentially
				
				if(tcid.equalsIgnoreCase(testcaseid))
				{
					AutomateTestNG.DeveloperLog.info("TCID "+tcid);
				String xpath="";
				String element=test_reader.getCellData(Constants.REO_TESTSTEPS, Constants.ELEMENT, i);//Read the element corresponding to the Test step
				String action= test_reader.getCellData(Constants.REO_TESTSTEPS, Constants.ACTION, i);//Read the action corresponding to the teststep
				AutomateTestNG.DeveloperLog.info("action"+action);
				AutomateTestNG.DeveloperLog.info("value of orid"+element);
				String condition=test_reader.getCellData(Constants.REO_TESTSTEPS, Constants.CONDITIONS, i);
				String data="";
				String lookupsheet= test_reader.getCellData(Constants.REO_TESTSTEPS, Constants.TCID, i);//Reads the TCID as the name of the datasheet
				String datafile=test_reader.getCellData(Constants.REO_TESTSTEPS,Constants.DATAFILE,i);//Reads the datafile column to look for the datasheet and column pipe seperated values
				AutomateTestNG.DeveloperLog.info("Lookupsheet"+lookupsheet);
				AutomateTestNG.DeveloperLog.info("LookupCol"+datafile);
				//Steps to seperate the sheet name and column name fetched from the datafile column of teststeps sheet
				int p=datafile.indexOf('|');
				String lookupcol=datafile.substring(p+1);
				AutomateTestNG.DeveloperLog.info("Row count of lookupsheet "+test_reader.getRowCount(lookupsheet));
				if((!lookupsheet.equals(null))||(!lookupcol.equals(null)))
				 data=readData(test_reader,lookupsheet,lookupcol,x);
					if(!element.equals(null))
				   xpath=readORMaster(element);
						
					AutomateTestNG.DeveloperLog.info("XPATH"+xpath);	
					AutomateTestNG.DeveloperLog.info("condition  "+condition);	
					//WaitTool.waitForElementPresent(driver, By.xpath("//*[@id='divMainNavBar']/div/span"),30);
					if((! (condition.equals("")))&& (!(condition.equals(null))))
					{
						int rownm=2;
						for(int j=2;j<=test_reader.getRowCount(lookupsheet);j++)
						{
							String execute=test_reader.getCellData(lookupsheet, "NO_OF_REO", j);
							int exe=Integer.parseInt(execute);
							AutomateTestNG.DeveloperLog.info("value of execute"+exe);
							if(exe==x)
							{
								rownm=j;
								break;
							}
						}
								
						int index=condition.indexOf('=');
						String rhs=condition.substring(index+1);
						String lhs=condition.substring(0, index);
						AutomateTestNG.DeveloperLog.info("Condition  " +condition);
						AutomateTestNG.DeveloperLog.info("lhs  " +lhs);
						AutomateTestNG.DeveloperLog.info("rhs  " +rhs);
						
						String datasheetvalue=test_reader.getCellData(tcid, lhs, rownm);
						AutomateTestNG.DeveloperLog.info("dat  " +datasheetvalue);
						if(!(datasheetvalue.matches(rhs)))
						{
							AutomateTestNG.DeveloperLog.info("Condition doesnot match");
							continue;
						}
							}
					
					
					if(action.equalsIgnoreCase(Constants.MOUSEOVER)){
						try
						{
						WebElement menu=driver.findElement(By.xpath(xpath));
						AutomateTestNG.DeveloperLog.info(menu);
						Actions action1=new Actions(driver);
						action1.moveToElement(menu).perform();
						String validation=test_reader.getCellData(Constants.TESTSTEPS, Constants.VALIDATION,i);
						AutomateTestNG.DeveloperLog.info("Validation  " +validation);
						if(!validation.equals(null))
						{
							String validation_xpath=readORMaster(validation);
							AutomateTestNG.DeveloperLog.info("Validation xpath "+validation_xpath);
							if(!validation_xpath.equals(null))
							WaitTool.waitForElementPresent(driver, By.xpath(validation_xpath),20);
						}
						AutomateTestNG.DeveloperLog.info("Wait over");
						AutomateTestNG.DeveloperLog.info("MOUSEOVER");
						}
						catch(Exception e)
						{
							AutomateTestNG.ErrorLog.error("Exception caught"+e);
							//APP_LOGS.error("Exception caught"+e);
						}
					}
					else if(action.equalsIgnoreCase(Constants.CLICK))			{
							
						try
						{
							// Function call modified by Aruna @ May 23 2016
							Webelement_Methods.Click(driver,xpath);	
						}
						catch(Exception e)
						{
							AutomateTestNG.ErrorLog.error("Exception caught"+e);
						}
						String validation=test_reader.getCellData(Constants.REO_TESTSTEPS, Constants.VALIDATION,i);
						AutomateTestNG.DeveloperLog.info("Validation  " +validation);
						String validation_xpath=readORMaster(validation);
						AutomateTestNG.DeveloperLog.info("Validation xpath "+validation_xpath);
						if(!validation.equals(null))
						{
							if(validation.equalsIgnoreCase("button_CreditScore"))
							{
								WaitTool.waitForElementClickable(driver, By.xpath(validation_xpath),120);
								
	
							}
							else
							{
							if(!validation_xpath.equals(null))
							WaitTool.waitForElementPresent(driver, By.xpath(validation_xpath),120);
							}
							}
						
					}
					else if (action.equalsIgnoreCase(Constants.CHECK))
					{
						//Modified by Shirisha on 21/05/2016 Start
						Webelement_Methods.Select_The_Checkbox(driver.findElement(By.xpath(xpath)));
					}
					else if(action.equalsIgnoreCase(Constants.UNCHECK)){
						Webelement_Methods.DeSelect_The_Checkbox(driver.findElement(By.xpath(xpath)));
					}
						else if(action.equalsIgnoreCase(Constants.TYPE))
					{       // Function call written by Aruna @ May 23 2016
							Webelement_Methods.enterText(driver.findElement(By.xpath(xpath)),data);
					}
						else if (action.equalsIgnoreCase(Constants.DROP_SELECT)){
						
							if(lookupcol.equalsIgnoreCase("Existing Property")){
								AutomateTestNG.DeveloperLog.info("Inside Existing property");
								int index=Integer.parseInt(data);
								AutomateTestNG.DeveloperLog.info("Inside Existing property   "+index);
								String existing_property_value=Webelement_Methods.selectElementByIndexMethod(driver.findElement(By.xpath(xpath)), index);
								AutomateTestNG.DeveloperLog.info("Selected value is  "+existing_property_value);
							
								
							if(existing_property_value.contains("Not listed"))
							{
								Webelement_Methods.enterText(driver.findElement(By.xpath("//*[@id='divRealEstateItemAddressControl']/div/div[1]/input[1]")),"TBD");
								Webelement_Methods.enterText(driver.findElement(By.xpath("//*[@id='divRealEstateItemAddressControl']/div/div[2]/input[2]")),"90025");
								Webelement_Methods.enterText(driver.findElement(By.xpath("//input[contains(@data-ng-model,'getProperty().currentEstimatedValue')]")),"500000");
								AutomateTestNG.DeveloperLog.info("Entered property fields of REO");
							}
							
							}
							else if(lookupcol.equalsIgnoreCase("Same"))
							{
								if(driver.findElement(By.xpath(xpath)).isDisplayed())
									Webelement_Methods.selectElementByVisibleTextMethod(driver.findElement(By.xpath(xpath)),data);
							}
					//Function call written by Satya @ April 30 2016
							else	if((driver.findElement(By.xpath(xpath)).isEnabled()))
						Webelement_Methods.selectElementByVisibleTextMethod(driver.findElement(By.xpath(xpath)),data);
							AutomateTestNG.DeveloperLog.info("Exiting from dropdown case");
						}
					else if(action.equalsIgnoreCase("ClickNext"))
					{
						// Function call modified by Aruna @ May 23 2016
						Webelement_Methods.ClickNext(driver);
						String validation=test_reader.getCellData("TESTSTEPS", "VALIDATION",i);
						AutomateTestNG.DeveloperLog.info("Validation  " +validation);
						WaitTool.waitForElementPresent(driver, By.xpath(validation),20);
					}
					else if(action.equalsIgnoreCase(Constants.RADIO_CLICK))
						// Function call written by Aruna @ May 23 2016
						driver = Webelement_Methods.SelectRadioYesNo(driver,data,xpath);
					else if(action.equalsIgnoreCase(Constants.TYPEZIP))
					{
						// Function call written by Aruna @ May 23 2016
						Webelement_Methods.enterText(driver.findElement(By.xpath(xpath)),data);
					}
					
					else
						AutomateTestNG.DeveloperLog.info("No Such Action");
					
					
					}	
				
			}
				return driver;
	}
	  //Function to read ORMaster
	  public static String readORMaster(String orid)
	  {try
	  {
		  Xls_Reader or_reader= new Xls_Reader(Constants.REO_BOOK);
		  AutomateTestNG.DeveloperLog.info("Inside xpath");
		  String xpath=null;
		  for(int k=2;k<=or_reader.getRowCount(Constants.OR);k++)
				
		   {
			//System.out.println("Inside xpath loop");
			if(orid.equalsIgnoreCase(or_reader.getCellData(Constants.OR, Constants.ELEMENT, k)))
				{
					
					xpath= or_reader.getCellData(Constants.OR,Constants.XPATH,k);
					//System.out.println("value of action"+action);
					AutomateTestNG.DeveloperLog.info("value of xpath"+xpath);
					break;
				}
			} 
		  return xpath;
		  
	  }catch(Exception e)
	  {
		  return "";
	  }
	  }
	  
	  
	  //Function to read Datasheet
	  public static String readData(Xls_Reader test_reader,String lookupsheet,String lookupcol,int x)
	  {
		  String data=null;
		  AutomateTestNG.DeveloperLog.info("Inside data");;
		  for(int j=2;j<=test_reader.getRowCount(lookupsheet);j++)
			{
				String execute=test_reader.getCellData(lookupsheet, "NO_OF_REO", j);
				int exe=Integer.parseInt(execute);
				AutomateTestNG.DeveloperLog.info("value of execute"+exe);
				if(exe==x)
				{
					data=test_reader.getCellData(lookupsheet, lookupcol, j);
					
					AutomateTestNG.DeveloperLog.info("value of data"+data);
					break;
					//System.out.println("Row count of OR_MASTER "+or_reader.getRowCount("OR_MASTER"));
				}
			}
		  return data;
	  }
}
