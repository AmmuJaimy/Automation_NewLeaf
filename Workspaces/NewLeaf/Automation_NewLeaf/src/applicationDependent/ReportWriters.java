package applicationDependent;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import testng.AutomateTestNG;
import utilities.EnviornmentConstants.Constants;
import utilities.common.WaitTool;
import utilities.common.Xls_Reader;
import utilities.functionLibrary.Webelement_Methods;

public class ReportWriters {
	public static boolean isReportGenerated=true;
	public static void GenerateReport(WebDriver driver, String tcid) 
	{
		try
		{
			isReportGenerated=false;
			Xls_Reader reportWriter = new Xls_Reader(Constants.REPORT_WRITER_PATH);
			String timeStamp = new SimpleDateFormat("yyyyMMMdd_HHmmss").format(Calendar.getInstance().getTime());
			String sheetName = "tcid" + timeStamp;	
			reportWriter.addSheet(sheetName);
			Xls_Reader reportReader = new Xls_Reader(Constants.REPORT_READER_PATH);
			for (int i = 2; i <= reportReader.getRowCount("COLUMNNAMES"); i++) 
			{
				reportWriter.addColumn(sheetName, reportReader.getCellData("COLUMNNAMES", "COLUMN NAMES", i));
			}
			WebElement productscope = driver.findElement(By.xpath(getXpath(reportReader,"scope_EligibleProducts")));
			List<WebElement> productgroups = productscope.findElements(By.xpath(getXpath(reportReader,"scope_ProductGroups")));
			int rowNum = 2;
			int iteratedRowNum = 0;
			for (int i = 0; i < productgroups.size(); i++) {
				WebElement productgrouping = productgroups.get(i);
				WebElement productgroup = productgrouping.findElement(By.xpath(getXpath(reportReader,"label_ProductGroup")));
				reportWriter.setCellData(sheetName, reportReader.getCellData("COLUMNNAMES", "COLUMN NAMES", 2), rowNum, productgroup.getText());
				rowNum++;
				int tempRowNum = rowNum;
				for (int readerRowNum = 2; readerRowNum <= reportReader.getRowCount("COLUMNNAMES"); readerRowNum++) {
					String ColumnName = reportReader.getCellData("COLUMNNAMES", "COLUMN NAMES", readerRowNum);
					String element = reportReader.getCellData("COLUMNNAMES", "ELEMENT", readerRowNum);
					String xpath = getXpath(reportReader, element);
					List<WebElement> data = productgrouping.findElements(By.xpath(xpath));
					for (int k = 0; k < data.size(); k++) {
						reportWriter.setCellData(sheetName, ColumnName, tempRowNum, data.get(k).getText());
						tempRowNum++;
						iteratedRowNum = tempRowNum;
					}
					tempRowNum = rowNum;
				}
				rowNum = iteratedRowNum + 1;
			}
			isReportGenerated = true;
		} catch (Exception e) {
			AutomateTestNG.DeveloperLog.info("No Products Found" + e.getMessage());
			AutomateTestNG.ErrorLog.info("No Products Found" + e.getMessage());

		}
	}

	public static WebDriver AddProduct(WebDriver driver, String lookupcol, String data)  {
		Xls_Reader reportReader;

		try {
			while(!isReportGenerated)
				Thread.sleep(2000);
			reportReader = new Xls_Reader(Constants.REPORT_READER_PATH);
			WebElement productscope = driver.findElement(By.xpath(getXpath(reportReader,"scope_EligibleProducts")));
			List<WebElement> productname = productscope.findElements(By.xpath(getXpath(reportReader,"label_ProductName")));
			List<WebElement> productidentifier = productscope.findElements(By.xpath(getXpath(reportReader,"label_ProductId")));
			List<WebElement> updatelink = productscope.findElements(By.xpath(getXpath(reportReader,"link_Update")));
			List<WebElement> productgrouping = productscope.findElements(By.xpath(getXpath(reportReader,"label_ProductGroup")));
			List<WebElement> productgroups = productscope.findElements(By.xpath(getXpath(reportReader,"scope_ProductGroups")));
			Xls_Reader loanNumberWriter = new Xls_Reader(Constants.LOAN_NUMBER_WRITER_PATH);
			if (lookupcol.equalsIgnoreCase("Product ID")) {
				for (int i = 0; i < productidentifier.size(); i++) {
					String prodid = productidentifier.get(i).getText();
					if (prodid.equals(data))
					{
						System.out.println("Update link no--" + i + "--" + updatelink.get(i).getText());
						updatelink.get(i).click();
						break;
					}
				}
			} else if (lookupcol.equalsIgnoreCase("Product Name")) {
				for (int i = 0; i < productname.size(); i++) {
					String prodname = productname.get(i).getText();
					if (prodname.equals(data)){
						updatelink.get(i).click();
						break;
					}
				}
			} else if (lookupcol.equalsIgnoreCase("Product Type")) {
				for (int i = 0; i < productgrouping.size(); i++) {
					String producttype = productgrouping.get(i).getText();
					if (producttype.contains(data)) {
						productgroups.get(i).findElement(By.xpath("." + reportReader.getCellData("OR", "XPATH",
								reportReader.getCellRowNum("OR", "ELEMENT", "link_Update")))).click();
						break;
					}
				}
			}
			Xls_Reader orReader=new Xls_Reader(Constants.OR_PATH);
			if(!driver.findElements(By.xpath("//label[@class='imp-lbl-radio']")).isEmpty())
			{
				if(driver.findElement(By.xpath("//label[@class='imp-lbl-radio']")).isDisplayed())
				{
					String xpath=orReader.getCellData(Constants.OR_MASTER,"XPATH",orReader.getCellRowNum(Constants.OR_MASTER, "ELEMENT", "radio_TaxQuestions"));
					Webelement_Methods.SelectRadioYesNo(driver, "No", xpath);
					xpath=orReader.getCellData(Constants.OR_MASTER,"XPATH",orReader.getCellRowNum(Constants.OR_MASTER, "ELEMENT", "textbox_TaxQuestions"));
					Webelement_Methods.enterText(driver.findElement(By.xpath(xpath)), "0");
					xpath=orReader.getCellData(Constants.OR_MASTER,"XPATH",orReader.getCellRowNum(Constants.OR_MASTER, "ELEMENT", "button_TaxQuestions"));
					driver.findElement(By.xpath(xpath)).click();
				}
			}
			//AutomateTestNG.DeveloperLog.info("xpath"+getXpath(reportReader,"button_SaveApplication"));
			System.out.println("xpath"+getXpath(reportReader,"button_SaveApplication"));
			int wait=Integer.parseInt(getXpath(reportReader,"waittime"));
			System.out.println("Wait"+wait);
			/*WaitTool.waitForElementPresent(driver, By.xpath(getXpath(reportReader,"button_SaveApplication")),wait);
			System.out.println("Element save app found");
			Thread.sleep(5000);
			WaitTool.waitForElementClickable(driver, By.xpath(getXpath(reportReader,"button_SaveApplication")),120);
			System.out.println("button is clickable");
			Webelement_Methods.Click(driver,getXpath(reportReader,"button_SaveApplication"));
			Thread.sleep(5000);
			WaitTool.waitForElementClickable(driver, By.xpath(getXpath(reportReader,"button_SaveApplication")),120);
			System.out.println("button is clickable");*/
			WaitTool.waitForElementPresent(driver, By.xpath(getXpath(reportReader,"label_LoanNumber")),wait);
			System.out.println("Element loan number found");
			String LoanNumber="--NONE--";
			String ApplicantName="--NONE--";
			String LoanProduct="--NONE--";
			if(driver.findElements(By.xpath(getXpath(reportReader,"label_LoanNumber"))).size()>0)
				LoanNumber = driver.findElement(By.xpath(getXpath(reportReader,"label_LoanNumber"))).getText();
			if(driver.findElements(By.xpath(getXpath(reportReader,"label_ApplicantName"))).size()>0)
				ApplicantName = driver.findElement(By.xpath(getXpath(reportReader,"label_ApplicantName"))).getText();
			if(driver.findElements(By.xpath(getXpath(reportReader,"label_LoanProduct"))).size()>0)
				LoanProduct = driver.findElement(By.xpath(getXpath(reportReader,"label_LoanProduct"))).getAttribute("innerText");
			String timeStamp = new SimpleDateFormat("yyyyMMMdd_HHmmss").format(Calendar.getInstance().getTime());
			int temp=loanNumberWriter.getRowCount("LOAN_NUMBERS") + 1;
			loanNumberWriter.setCellData("LOAN_NUMBERS", "LOAN NUMBER",temp,
					LoanNumber);
			loanNumberWriter.setCellData("LOAN_NUMBERS", "APPLICANT NAME",temp, ApplicantName);
			loanNumberWriter.setCellData("LOAN_NUMBERS", "PRODUCT", temp,
					LoanProduct);
			loanNumberWriter.setCellData("LOAN_NUMBERS", "DATE/TIME"
					, temp,timeStamp);
		} catch (Exception e) {
			AutomateTestNG.ErrorLog.error("Product could not be added");
			AutomateTestNG.ErrorLog.error(e.getMessage());
		}
		return driver;
	}
	public static WebDriver ClosingCostSubmenu(WebDriver driver,String lookupcol,String data,Xls_Reader test_reader)throws Exception
	{
		Xls_Reader reportReader = new Xls_Reader(Constants.REPORT_READER_PATH);
		WebElement productscope = driver.findElement(By.xpath(getXpath(reportReader,"scope_EligibleProducts")));
		List<WebElement> productname = productscope.findElements(By.xpath(getXpath(reportReader,"label_ProductName")));
		List<WebElement> productidentifier = productscope.findElements(By.xpath(getXpath(reportReader,"label_ProductId")));
		List<WebElement> closingcostlink = productscope.findElements(By.xpath(getXpath(reportReader,"label_ClosingCosts")));
		List<WebElement> productgrouping = productscope.findElements(By.xpath(getXpath(reportReader,"label_ProductGroup")));
		List<WebElement> productgroups = productscope.findElements(By.xpath(getXpath(reportReader,"scope_ProductGroups")));
		//Xls_Reader loanNumberWriter = new Xls_Reader(Constants.LOAN_NUMBER_WRITER_PATH);
		if (lookupcol.equalsIgnoreCase("Product ID")) {
			for (int i = 0; i < productidentifier.size(); i++) {
				String prodid = productidentifier.get(i).getText();
				if (prodid.equals(data))
				{
					System.out.println("Update link no--" + i + "--" + closingcostlink.get(i).getText());
					closingcostlink.get(i).click();
					break;
				}
			}
		} else if (lookupcol.equalsIgnoreCase("Product Name")) {
			for (int i = 0; i < productname.size(); i++) {
				String prodname = productname.get(i).getText();
				if (prodname.equals(data)){
					closingcostlink.get(i).click();
					break;
				}
			}
		} else if (lookupcol.equalsIgnoreCase("Product Type")) {
			for (int i = 0; i < productgrouping.size(); i++) {
				String producttype = productgrouping.get(i).getText();
				if (producttype.contains(data)) {
					productgroups.get(i).findElement(By.xpath("." + reportReader.getCellData("OR", "XPATH",
							reportReader.getCellRowNum("OR", "ELEMENT", "label_ClosingCosts")))).click();
					break;
				}
			}
		}
		return driver;
	}
	public static WebDriver ClosingCost(WebDriver driver,String subsheet,String lookupcol,String data,Xls_Reader test_reader)throws Exception
	{
		Xls_Reader reportReader = new Xls_Reader(Constants.REPORT_READER_PATH);
		WebElement productscope = driver.findElement(By.xpath(getXpath(reportReader,"scope_EligibleProducts")));
		List<WebElement> productname = productscope.findElements(By.xpath(getXpath(reportReader,"label_ProductName")));
		List<WebElement> productidentifier = productscope.findElements(By.xpath(getXpath(reportReader,"label_ProductId")));
		List<WebElement> closingcostlink = productscope.findElements(By.xpath(getXpath(reportReader,"label_ClosingCosts")));
		List<WebElement> productgrouping = productscope.findElements(By.xpath(getXpath(reportReader,"label_ProductGroup")));
		List<WebElement> productgroups = productscope.findElements(By.xpath(getXpath(reportReader,"scope_ProductGroups")));
		//Xls_Reader loanNumberWriter = new Xls_Reader(Constants.LOAN_NUMBER_WRITER_PATH);
		if (lookupcol.equalsIgnoreCase("Product ID")) {
			for (int i = 0; i < productidentifier.size(); i++) {
				String prodid = productidentifier.get(i).getText();
				if (prodid.equals(data))
				{
					System.out.println("Update link no--" + i + "--" + closingcostlink.get(i).getText());
					closingcostlink.get(i).click();
					break;
				}
			}
		} else if (lookupcol.equalsIgnoreCase("Product Name")) {
			for (int i = 0; i < productname.size(); i++) {
				String prodname = productname.get(i).getText();
				if (prodname.equals(data)){
					closingcostlink.get(i).click();
					break;
				}
			}
		} else if (lookupcol.equalsIgnoreCase("Product Type")) {
			for (int i = 0; i < productgrouping.size(); i++) {
				String producttype = productgrouping.get(i).getText();
				if (producttype.contains(data)) {
					productgroups.get(i).findElement(By.xpath("." + reportReader.getCellData("OR", "XPATH",
							reportReader.getCellRowNum("OR", "ELEMENT", "label_ClosingCosts")))).click();
					break;
				}
			}
		}
		Xls_Reader writer=new Xls_Reader("C:\\Users\\Martin George\\Desktop\\Ammu\\Rityush Technology\\Automation\\Jar\\OUTSIDE JAR\\Reports\\xmlcomparison.xlsx");
		String timeStamp = new SimpleDateFormat("yyyyMMMdd_HHmmss").format(Calendar.getInstance().getTime());
		Constants.RESULTSHEET = "tcid" + timeStamp;	
		writer.addSheet(Constants.RESULTSHEET);
		writer.addColumn(Constants.RESULTSHEET, "ELEMENT_NAME");
		writer.addColumn(Constants.RESULTSHEET,"VALUE FROM WEBPAGE");
		writer.addColumn(Constants.RESULTSHEET, "VALUE FROM XML");
		writer.addColumn(Constants.RESULTSHEET, Constants.PASSFAIL);
		for(int i=2;i<=test_reader.getRowCount(subsheet);i++)
		{
			String element_name=test_reader.getCellData(subsheet, "ELEMENT_NAME", i);
			String element_id=test_reader.getCellData(subsheet, "ELEMENT_ID", i);
			String xpath=AutomateTestNG.readORMaster(element_id);
			String value=driver.findElement(By.xpath(xpath)).getText();
			writer.setCellData(Constants.RESULTSHEET, "ELEMENT_NAME", i, element_name);
			writer.setCellData(Constants.RESULTSHEET, "VALUE FROM WEBPAGE", i, value);
			System.out.println("Element_name   " +element_name+"      Value   "+value);
		}
		return driver;
	}
	public static WebDriver download(WebDriver driver,String xpath) throws InterruptedException
	{
		System.out.println("Inside download");
		String integrationxpath=AutomateTestNG.readORMaster("label_IntegrationEvent");
    	List <WebElement> downloadlist = driver.findElements(By.xpath(xpath));
    	List <WebElement> integrationlist = driver.findElements(By.xpath(integrationxpath));
    	int length= downloadlist.size();
    	System.out.println("Download link count "+length);
    	
    	for(int i=length-1;i>=0;i--)
    	{
    		String integrationname=integrationlist.get(i).getText();
    		System.out.println("Integration name "+integrationname);
    		if(integrationname.contains("Incoming Response"))
    		{
    			System.out.println("Download xml match found");
    			downloadlist.get(i).click();
    			break;
    		}
    	}
    	
    	Thread.sleep(25000);
    	File getLatestFile = getLatestFilefromDir(Constants.DOWNLOADPATH);
	   String filename = getLatestFile.getName();
	    Constants.DOWNLOADEDFILE=Constants.DOWNLOADPATH+"\\"+filename;
    	System.out.println("Clicked" +Constants.DOWNLOADEDFILE);
    	String urltoopen="file:///"+Constants.DOWNLOADEDFILE;
    	System.out.println("Clicked" +urltoopen);
    	//WebDriver driver1=new ChromeDriver();
    	//driver1.get(urltoopen);
    	//WaitTool.waitForElementPresent(driver, By.xpath("//*[@id='collapsible3']/div[1]/div[2]/div[1]/span/span[2]/span[2]"),20);
    	
    	// String escrow=driver1.findElement(By.xpath("//*[@id='collapsible27']/div[1]/div[1]/span[2]/span[5]/span[2]")).getText();
		//   System.out.println("Settlement Escrow   "+escrow);
    	return driver;
		
	}
	public static File getLatestFilefromDir(String dirPath){
	    File dir = new File(dirPath);
	    File[] files = dir.listFiles();
	    if (files == null || files.length == 0) {
	        return null;
	    }

	    File lastModifiedFile = files[0];
	    for (int i = 1; i < files.length; i++) {
	       if (lastModifiedFile.lastModified() < files[i].lastModified()) {
	           lastModifiedFile = files[i];
	       }
	    }
	    return lastModifiedFile;
	}

	public static String getXpath(Xls_Reader reportReader, String element)
	{
		return reportReader.getCellData("OR", "XPATH",
				reportReader.getCellRowNum("OR", "ELEMENT", element));
	}
}


