package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Logindao;

public class LoginServlet extends HttpServlet{

    private static final long serialVersionUID = 1L;

    public void doPost(HttpServletRequest request, HttpServletResponse response)  
            throws ServletException, IOException {  
    	String action=request.getParameter("action");
    	System.out.println(action);
        response.setContentType("text/html");  
        PrintWriter out = response.getWriter();  
        if(action.equals("Login"))
        {
        String n=request.getParameter("username");  
        String p=request.getParameter("userpass"); 
        
        HttpSession session = request.getSession(false);
        if(session!=null)
        session.setAttribute("name", n);

        if(Logindao.validate(n, p)){  
            RequestDispatcher rd=request.getRequestDispatcher("welcome.jsp");  
            rd.forward(request,response);  
        }  
        else{  
           // out.print("<p style=\"color:red\">Sorry username or password error</p>");  
        	  request.setAttribute("errorMessage", "Invalid Credentials");
        	RequestDispatcher rd=request.getRequestDispatcher("Package.jsp");  
            rd.include(request,response); 
            //out.print("<p style=\"color:red\">Sorry username or password error</p>");  
        }  
        }
        if(action.equals("SearchFlight"))
        {
        	
        	String Fromval = request.getParameter("From");
        	String from=null;
        	if(Fromval.equals("0"))
        	 from="LONDON";
        	else if(Fromval.equals("1"))
        	from="PARIS";
        	else if(Fromval.equals("2")) 		
        	from="DUBAI";
        	else if(Fromval.equals("3"))		
        	from="MANDRID";
    				
        	
        	
        	
        	String Toval = request.getParameter("To");
        	String To=null;
        	if(Toval.equals("0"))
           	 To="LONDON";
           	else if(Toval.equals("1"))
           	To="PARIS";
           	else if(Toval.equals("2")) 		
           	To="DUBAI";
           	else if(Toval.equals("3"))		
           	To="MANDRID";
        	
            String startdate = request.getParameter("StartDate");
            String returndate = request.getParameter("ReturnDate");
            String nooftickets = request.getParameter("NoOfTickets");
            if(from.isEmpty()||To.isEmpty()||startdate.isEmpty()||returndate.isEmpty()||nooftickets.isEmpty())
				{
					RequestDispatcher rd = request.getRequestDispatcher("Gallery.jsp");
					out.println("<font color=red>Please fill all the fields</font>");
					rd.include(request, response);
				}
            
            String ret=Logindao.SearchTickets(from, To,nooftickets);
            if(ret!=null){ 
            	if(ret.equals("Insufficient Balance in your account"))
            	{
            		 request.setAttribute("errorMessage", "Sorry, Your request could not be processed because of insufficient balance in our account.");
                 	RequestDispatcher rd=request.getRequestDispatcher("welcome.jsp");  
                     rd.include(request,response); 	
            	}
            	else
            	{
            	String[] parts = ret.split("%");
            	String UPFLIGHTNUM = parts[0]; // 004
            	String DOWNFLIGHTNUM = parts[1]; // 034556
            	String UPPRICE=parts[2];
            	String DOWNPRICE=parts[3];
            	String TOTALPRICE=parts[4];
            	HttpSession session = request.getSession(false);
                if(session!=null)
                {
              
                    session.setAttribute("startdate", startdate);
                    session.setAttribute("returndate", returndate);
                    session.setAttribute("from",from);
                    session.setAttribute("To", To);
                    session.setAttribute("nooftickets", nooftickets);
                    session.setAttribute("UPFLIGHTNUM",UPFLIGHTNUM);
                    session.setAttribute("DOWNFLIGHTNUM",DOWNFLIGHTNUM);
                    session.setAttribute("UPPRICE", UPPRICE);
                    session.setAttribute("DOWNPRICE", DOWNPRICE);
                    session.setAttribute("TOTALPRICE",TOTALPRICE);
                    
                    
                }
            	//request.setAttribute("errorMessage", "Credited Successfully");
                RequestDispatcher rd=request.getRequestDispatcher("Ticket.jsp");  
                rd.forward(request,response);  
            }  
            }
            else{  
               // out.print("<p style=\"color:red\">Sorry username or password error</p>");  
            	  request.setAttribute("errorMessage", "Sorry, Your request could not be processed. Please try again");
            	RequestDispatcher rd=request.getRequestDispatcher("welcome.jsp");  
                rd.include(request,response); 
                //out.print("<p style=\"color:red\">Sorry username or password error</p>");  
            }  
        }
        if(action.equals("Confirm"))
        {
        	String DOWNFLIGHTNUM = request.getParameter("DOWNFLIGHTNUM");
            String DOWNPRICE = request.getParameter("DOWNPRICE");
            System.out.println("DOWNFLIGHTNUM  "+DOWNFLIGHTNUM);
            System.out.println("DOWNPRICE  "+DOWNPRICE);
           // String nooftickets = request.getParameter("NoOfTickets");
        	RequestDispatcher rd=request.getRequestDispatcher("Ticket.jsp");  
            rd.include(request,response); 
        }
        if(action.equals("Register"))
        {
        	   String name = request.getParameter("username");
               String password = request.getParameter("userpass");
               String confirmpassword = request.getParameter("confirmpass");
               String emailid = request.getParameter("email");
               String currencyval=request.getParameter("Currency");
               String currency="USD";
               if(currencyval.equals("0"))
            	   currency="USD";
               if(currencyval.equals("1"))
            	   currency="EUR";
               
               String openingbal=request.getParameter("balance");
               int bal=Integer.parseInt(openingbal);
               HttpSession session = request.getSession(false);
               if(session!=null)
               session.setAttribute("name", name);		
               		System.out.println(name);
               		System.out.println(password);
               		System.out.println(confirmpassword);
               		System.out.println(emailid);
       				if(name.isEmpty()||password.isEmpty()||confirmpassword.isEmpty()||emailid.isEmpty())
       				{
       					RequestDispatcher rd = request.getRequestDispatcher("Gallery.jsp");
       					out.println("<font color=red>Please fill all the fields</font>");
       					rd.include(request, response);
       				}
       				if(!(password.equals(confirmpassword)))
       						{
       					request.setAttribute("errorMessage", "Confirm Password must match with the Password you entered");
       					RequestDispatcher rd = request.getRequestDispatcher("Gallery.jsp");
       					//out.println("<font color=red>Confirm Password must match with the Password you entered</font>");
       					rd.include(request, response);
       						}
       				if(bal<1000)
       				{
       					request.setAttribute("errorMessage", "Opening Balance has to be more than 1000 USD");
       					RequestDispatcher rd = request.getRequestDispatcher("Gallery.jsp");
       					//out.println("<font color=red>Confirm Password must match with the Password you entered</font>");
       					rd.include(request, response);
       				}
       				else
       				{
       					 if(Logindao.register(name, password, confirmpassword, emailid,currency,bal)){  
       				            RequestDispatcher rd=request.getRequestDispatcher("welcome.jsp");  
       				            rd.forward(request,response);  
       				        }  
       				        else{  
       				           // out.print("<p style=\"color:red\">Sorry username or password error</p>");  
       				        	request.setAttribute("errorMessage", "UserName Already Exists. Please select a different one");
       				        	RequestDispatcher rd=request.getRequestDispatcher("Gallery.jsp");  
       				            rd.include(request,response);  
       				        }  
        }
        }
        if(action.equals("Credit"))
        {
        
        	String n=request.getParameter("Username");  
            String p=request.getParameter("Amount"); 
            
          //  HttpSession session = request.getSession(false);
            if(Logindao.BuyCredit(n, p)){  
            	request.setAttribute("errorMessage", "Credited Successfully");
                RequestDispatcher rd=request.getRequestDispatcher("BuyCredit.jsp");  
                rd.forward(request,response);  
            }  
            else{  
               // out.print("<p style=\"color:red\">Sorry username or password error</p>");  
            	  request.setAttribute("errorMessage", "Sorry, Amount could not be credited. Please check your user name");
            	RequestDispatcher rd=request.getRequestDispatcher("BuyCredit.jsp");  
                rd.include(request,response); 
                //out.print("<p style=\"color:red\">Sorry username or password error</p>");  
            }  
        }
        if(action.equals("CheckBalance"))
        {
        	String n=request.getParameter("Username");  
            
            
            HttpSession session = request.getSession(false);
            String ret=Logindao.CheckBalance(n);
            if(!ret.equals("ERROR")){ 
            	
            	String message="Your account balance is"+ret;
            	request.setAttribute("errorMessage", message);
                RequestDispatcher rd=request.getRequestDispatcher("CheckBalance.jsp");  
                rd.forward(request,response);  
            }  
            else{  
               // out.print("<p style=\"color:red\">Sorry username or password error</p>");  
            	  request.setAttribute("errorMessage", "Sorry, Request could not be processed. Please check your user name");
            	RequestDispatcher rd=request.getRequestDispatcher("CheckBalance.jsp");  
                rd.include(request,response); 
                //out.print("<p style=\"color:red\">Sorry username or password error</p>");  
            }  
        }
        if(action.equals("Withdraw"))
        {
        	String n=request.getParameter("Username");  
            String p=request.getParameter("Amount"); 
            
            HttpSession session = request.getSession(false);
            if(Logindao.Withdraw(n, p)==0){  
            	request.setAttribute("errorMessage", "Withdrawn Successfully");
                RequestDispatcher rd=request.getRequestDispatcher("Withdraw.jsp");  
                rd.forward(request,response);  
            }  
            else if(Logindao.Withdraw(n, p)==1){  
               // out.print("<p style=\"color:red\">Sorry username or password error</p>");  
            	  request.setAttribute("errorMessage", "Sorry, Amount cannot be withdrawn. Please check your user name");
            	RequestDispatcher rd=request.getRequestDispatcher("Withdraw.jsp");  
                rd.include(request,response); 
                //out.print("<p style=\"color:red\">Sorry username or password error</p>");  
            }  
            else if(Logindao.Withdraw(n, p)==2){  
                // out.print("<p style=\"color:red\">Sorry username or password error</p>");  
             	  request.setAttribute("errorMessage", "Sorry, Amount cannot be withdrawn. Insufficient Balance");
             	RequestDispatcher rd=request.getRequestDispatcher("BuyCredit.jsp");  
                 rd.include(request,response); 
                 //out.print("<p style=\"color:red\">Sorry username or password error</p>");  
             }  
        }
    }  
} 