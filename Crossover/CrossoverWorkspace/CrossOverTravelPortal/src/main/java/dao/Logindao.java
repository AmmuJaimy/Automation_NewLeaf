package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Logindao {
	static String url = "jdbc:mysql://localhost:3306/";
     static String dbName = "TRAVELPORTAL";
     static String driver = "com.mysql.jdbc.Driver";
     static String userName = "user1";
     static String password = "password";
     static String uid=null;
     //Added
    public static boolean validate(String name, String pass) {        
        boolean status = false;
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;

       
        try {
            Class.forName(driver).newInstance();
            conn = DriverManager
                    .getConnection(url + dbName, userName, password);

            pst = conn
                    .prepareStatement("select * from LOGINDETAILS where USERNAME=? and PWD=?");
            pst.setString(1, name);
            pst.setString(2, pass);

            rs = pst.executeQuery();
            status = rs.next();
            uid=name;
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (pst != null) {
                try {
                    pst.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return status;
    }
    public static boolean register(String name,String pass, String confirmpassword,String emailid,String currency,int balance)
    {
    	  boolean status = false;
          Connection conn = null;
          PreparedStatement pst = null;
          ResultSet rs = null;
          PreparedStatement pst1 = null;
          ResultSet rs1 = null;

         
          try {
              Class.forName(driver).newInstance();
              conn = DriverManager
                      .getConnection(url + dbName, userName, password);

              pst = conn
                      .prepareStatement("insert into LOGINDETAILS values(?,?,?)");
              pst.setString(1, name);
              pst.setString(2, pass);
              pst.setString(3, emailid);
              pst1 = conn
                      .prepareStatement("insert into paypallet values(?,?,?)");
              pst1.setString(1, name);
              pst1.setString(2, currency);
              pst1.setInt(3, balance);
              int i = pst.executeUpdate();
              int j=pst1.executeUpdate();
              if ((i > 0)&&(j>0))
                  status=true;
   
          } catch (Exception e2) {
              System.out.println(e2);
              status=false;
          }
  
              if (conn != null) {
                  try {
                      conn.close();
                  } catch (SQLException e) {
                      e.printStackTrace();
                  }
              }
              if (pst != null) {
                  try {
                      pst.close();
                  } catch (SQLException e) {
                      e.printStackTrace();
                  }
              }
              if (pst1 != null) {
                  try {
                      pst1.close();
                  } catch (SQLException e) {
                      e.printStackTrace();
                  }
              }
          
          return status;
    	
    }
    public static boolean BuyCredit(String username, String Amount)
    {
    	 boolean status = false;
         Connection conn = null;
         PreparedStatement pst = null;
         ResultSet rs = null;

        
         try {
             Class.forName(driver).newInstance();
             conn = DriverManager
                     .getConnection(url + dbName, userName, password);

             pst = conn
                     .prepareStatement("update paypallet set BALANCE=BALANCE+?  where USERNAME=?");
             int bal=Integer.parseInt(Amount);
             pst.setString(2, uid);
             pst.setInt(1, bal);
             

             int st = pst.executeUpdate();
             if(st>0)
             status = true;

         } catch (Exception e) {
             System.out.println(e);
         } finally {
             if (conn != null) {
                 try {
                     conn.close();
                 } catch (SQLException e) {
                     e.printStackTrace();
                 }
             }
             if (pst != null) {
                 try {
                     pst.close();
                 } catch (SQLException e) {
                     e.printStackTrace();
                 }
             }
             if (rs != null) {
                 try {
                     rs.close();
                 } catch (SQLException e) {
                     e.printStackTrace();
                 }
             }
         }
         return status;
    	
    }
    public static String CheckBalance(String username)
    {
    	 boolean status = false;
         Connection conn = null;
         PreparedStatement pst = null;
         ResultSet rs = null;
         String retval="ERROR";
        
         try {
             Class.forName(driver).newInstance();
             conn = DriverManager
                     .getConnection(url + dbName, userName, password);

             pst = conn
                     .prepareStatement("select balance,currency from paypallet  where USERNAME=?");
            
             pst.setString(1, uid);
             
             

             rs = pst.executeQuery();
             
             status = rs.next();
             if(status==false)
            	 retval="ERROR";
             else
             {
            	 String amount=rs.getString("BALANCE");
            	 String cur=rs.getString("CURRENCY");
            	 retval=amount+cur;
             }
            	 
         } catch (Exception e) {
             System.out.println(e);
         } finally {
             if (conn != null) {
                 try {
                     conn.close();
                 } catch (SQLException e) {
                     e.printStackTrace();
                 }
             }
             if (pst != null) {
                 try {
                     pst.close();
                 } catch (SQLException e) {
                     e.printStackTrace();
                 }
             }
             if (rs != null) {
                 try {
                     rs.close();
                 } catch (SQLException e) {
                     e.printStackTrace();
                 }
             }
         }
         return retval;
    	
    }
    public static int Withdraw(String username, String Amount)
    {
    	 boolean status = false;
         Connection conn = null;
         PreparedStatement pst = null;
         ResultSet rs = null;
         int error=0;
        
         try {
             Class.forName(driver).newInstance();
             conn = DriverManager
                     .getConnection(url + dbName, userName, password);

             pst = conn
                     .prepareStatement("select balance from paypallet  where USERNAME=?");
             pst.setString(1, uid);
             rs=pst.executeQuery();
             status=rs.next();
             if(status==false)
               error=1;
             int balance=rs.getInt("BALANCE");
             int bal=Integer.parseInt(Amount);
             if((balance-bal)<1000)
            	 error=2;
             else
             {
            	  pst = conn
                          .prepareStatement("update paypallet set BALANCE=BALANCE-?  where USERNAME=?");
                  int balan=Integer.parseInt(Amount);
                  pst.setString(2, username);
                  pst.setInt(1, balan);
                  

                  int st = pst.executeUpdate();
                  if(st>0)
                  error=0;
             }
             
             
             
             
    

         } catch (Exception e) {
             System.out.println(e);
         } finally {
             if (conn != null) {
                 try {
                     conn.close();
                 } catch (SQLException e) {
                     e.printStackTrace();
                 }
             }
             if (pst != null) {
                 try {
                     pst.close();
                 } catch (SQLException e) {
                     e.printStackTrace();
                 }
             }
             if (rs != null) {
                 try {
                     rs.close();
                 } catch (SQLException e) {
                     e.printStackTrace();
                 }
             }
         }
         return error;
    	
    }
    public static String SearchTickets(String from,String To,String Numberoftickets)
    {
    	boolean status1 = false;
    	boolean status2 = false;
    	boolean status3 = false;
    	boolean status4 = false;
    	Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        String retval=null;
        PreparedStatement pst2=null;
        PreparedStatement pst3=null;
        try {
            Class.forName(driver).newInstance();
            conn = DriverManager
                    .getConnection(url + dbName, userName, password);

            pst = conn
                    .prepareStatement("select FLIGHTNUM,PRICE from GAMMAAIRLINES where ORIGIN=? and DESTINATION=?");
            pst.setString(1, from);
            pst.setString(2, To);

            rs = pst.executeQuery();
            status1 = rs.next();
            
            String UPFLIGHTNUM=rs.getString("FLIGHTNUM");
            int UPPRICE=rs.getInt("PRICE");
            pst = conn
                    .prepareStatement("select FLIGHTNUM,PRICE from GAMMAAIRLINES where ORIGIN=? and DESTINATION=?");
            pst.setString(1, To);
            pst.setString(2, from);

            rs = pst.executeQuery();
            status2 = rs.next();
            String DOWNFLIGHTNUM=rs.getString("FLIGHTNUM");
            int DOWNPRICE=rs.getInt("PRICE");
            int numtckt=Integer.parseInt(Numberoftickets);
            int totalprice=(UPPRICE+DOWNPRICE)*numtckt;
            pst2 = conn
                    .prepareStatement("select BALANCE from paypallet where USERNAME=?");
            
            pst2.setString(1, uid);

            rs = pst2.executeQuery();
            status3 = rs.next();
            int bal=rs.getInt("BALANCE");
            if((bal-totalprice)<0){
            	retval="Insufficient Balance in your account";
            	status3=false;
            }
            else
            {
            	 pst2 = conn
                         .prepareStatement("update paypallet set BALANCE=BALANCE-? where USERNAME=?");
                 pst2.setInt(1, totalprice);
                 pst2.setString(2, uid);

                 int suc = pst2.executeUpdate();
                 if(suc>0)
                	 status3=true;
                 else
                	 status3=false;
                 
                 pst3 = conn
                         .prepareStatement("insert into TRAVELSUMMARY values (?,?,?,?,?,?)");
                
                 pst3.setString(1, uid);
                 pst3.setString(2, UPFLIGHTNUM);
                 pst3.setString(3, from);
                 pst3.setString(4, To);
                 pst3.setString(5, DOWNFLIGHTNUM);
                 pst3.setInt(6, totalprice);
                 int suc1= pst3.executeUpdate();
                // status4=rs.next();
                if(suc1>0)
                	 status4=true;
                 else
                	 status4=false;
            }
            
            if(status1 && status2 && status3  &&status4)
            {
            	retval=UPFLIGHTNUM+"%"+DOWNFLIGHTNUM+"%"+UPPRICE+"%"+DOWNPRICE+"%"+totalprice;
            }
            

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (pst != null) {
                try {
                    pst.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return retval;
    
    }
}
