<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html>
<head>
    <title>Travelholic </title>
    <!--REQUIRED STYLE SHEETS-->
    <!-- BOOTSTRAP CORE STYLE CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLE CSS -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
    <!--ANIMATED FONTAWESOME STYLE CSS -->
    <link href="assets/css/font-awesome-animation.css" rel="stylesheet" />
     <!--PRETTYPHOTO MAIN STYLE -->
    <link href="assets/css/prettyPhoto.css" rel="stylesheet" />
       <!-- CUSTOM STYLE CSS -->
    <link href="assets/css/style.css" rel="stylesheet" />
    <!-- GOOGLE FONT -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <link rel="stylesheet" href="assets\css\jquery-ui.css">
  
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>
   <script>
  $( function() {
    $( "#datepicker1" ).datepicker();
  } );
  </script>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]--></head>

</head>
<body >
 <!-- NAV SECTION -->
         <div class="navbar navbar-inverse navbar-fixed-top">
       
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <h4 class="navbar-brand" >Hello
        <%=session.getAttribute("name")%></h4>
         
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="welcome.jsp">HOME</a></li>
                    <li><a href="Package.jsp">LOG OUT</a></li>
                     <li><a href="BuyCredit.jsp">BUY CREDIT</a></li>
                     <li><a href="Withdraw.jsp">WITHDRAW</a></li>
                     <li><a href="CheckBalance.jsp">CHECK BALANCE</a></li>
                    <li><a href="Contact.jsp">CONTACT</a></li>
                </ul>
            </div>
           
        </div>
    </div>
     <!--END NAV SECTION -->
     
        <!--HOME SECTION-->
    <div id="home-sec">

   
    <div class="container"  >
        <div class="row text-left">
            <div class="col-ss-1">
                
          <form action="loginServlet?action=SearchFlight" method="post" style="margin-left:200px">
        
        <fieldset style="width: 200px">
            
            <table border =0 style="color:black" align:"left">
                <tr>
                    <td>From</td>
                    <td>To</td>
                    <td>StartingOn</td>
                    <td>Return Date</td>
                    <td>Number of Tickets</td>
                   
                </tr>
                <tr>
                     <td><select  name="From" required="required" />
                    <option value="0">London</option>
       				 <option value="1">Paris</option>
       				 <option value="2">Dubai</option>
       				 <option value="3">Mandrid</option>
       				
                	</select>
                    </td>
                    
              		<td><select  name="To" required="required" />
              		 <option value="0">London</option>
       				 <option value="1">Paris</option>
       				 <option value="2">Dubai</option>
       				 <option value="3">Mandrid</option>
       				
                	</select>
              		</td>
              		<td><input type="text" name="StartDate" id="datepicker" required="required" /></td>
              		 <td><input type="text" name="ReturnDate"  id="datepicker1" required="required" /></td>
              		 <td><input type="number" name="NoOfTickets" required="required" /></td>
               		<td><input type="submit" value="Search" /></td>
                </tr>
                <tr>
                 
                
                </tr>
               
                
            </table>
        </fieldset>
        <div style="color: #FF0000;">${errorMessage}</div>
        
    </form>
         <!-- PORTFOLIO SECTION-->
   <section id="port-sec">
       <div class="container">
            
           <div class="row g-pad-bottom" >
                               <div class="col-md-10 col-md-offset-1 col-sm-12" >
                    <ul class="portfolio-items col-3">
                        <li class="portfolio-item">
                            <div class="item-main">
                                <div class="portfolio-image">
                                    <img src="assets/img/portfolio/thumb/Mandrid.jpg" alt="">
                                    <div class="overlay">
                                        <a class="preview btn btn-danger" title="Image Title Here" href="assets/img/portfolio/big/Mandrid.jpg"><i class=" fa fa-eye"></i></a>
                                    </div>
                                </div>
                                <h5>London-Mandrid USD 2000</h5>
                            </div>
                        </li>
                       
                        <li class="portfolio-item">
                            <div class="item-main">
                                <div class="portfolio-image">
                                    <img src="assets/img/portfolio/thumb/Dubai.jpg" alt="">
                                    <div class="overlay">
                                        <a class="preview btn btn-danger" title="Image Title Here" href="assets/img/portfolio/big/Dubai.jpg"><i class=" fa fa-eye"></i></a>
                                    </div>
                                </div>
                                <h5>London-Dubai USD 3000</h5>
                            </div>
                        </li>
                        <li class="portfolio-item">
                            <div class="item-main">
                                <div class="portfolio-image">
                                    <img src="assets/img/portfolio/thumb/Paris.jpg" alt="">
                                    <div class="overlay">
                                        <a class="preview btn btn-danger" title="Image Title Here" href="assets/img/portfolio/big/Paris.jpg"><i class=" fa fa-eye"></i></a>
                                    </div>
                                </div>
                                <h5>London-Paris USD 500</h5>
                            </div>
                        </li>
                       
                       
                        
                       
                       
                        
                    </ul>
                </div>
           </div>
       </div>
   </section>
     <!-- END PORTFOLIO SECTION-->    
                     
            </div>
        </div>
    </div>
         </div>
          

    <section  id="services-sec">
        <div class="container">
            <div class="row ">
               
                
                  </div>
               
                            
                    </div>
                    
                            
                    </div>
                </div>
          
        </div>
    </section>
    <!--END HOME SECTION-->     
  
                
  
</body>
</html>