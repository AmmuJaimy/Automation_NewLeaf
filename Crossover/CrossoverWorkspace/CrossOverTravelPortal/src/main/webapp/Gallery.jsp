<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
   <title>LOGIN-Travelic </title>
    <!--REQUIRED STYLE SHEETS-->
    <!-- BOOTSTRAP CORE STYLE CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLE CSS -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
    <!--ANIMATED FONTAWESOME STYLE CSS -->
    <link href="assets/css/font-awesome-animation.css" rel="stylesheet" />
     <!--PRETTYPHOTO MAIN STYLE -->
    <link href="assets/css/prettyPhoto.css" rel="stylesheet" />
       <!-- CUSTOM STYLE CSS -->
    <link href="assets/css/style.css" rel="stylesheet" />
    <!-- GOOGLE FONT -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style>
  article, aside, figure, footer, header, hgroup,
  menu, nav, section { display: block; }
  

  #username { margin-left: 100px; }
  #password { margin-left: 100px; }
  #confirmpassword { margin-left: 100px; }
  #emailid { margin-left: 100px; }
</style>
    </head>
 
<body>
     <!-- NAV SECTION -->
         <div class="navbar navbar-inverse navbar-fixed-top">
       
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">AROUND THE WORLD</a>
            </div>
            <div class="navbar-collapse collapse">
                 <ul class="nav navbar-nav navbar-right">
                    <li><a href="index.jsp">HOME</a></li>
                    <li><a href="Package.jsp">LOGIN</a></li>
                     <li><a href="Gallery.jsp">SIGN UP</a></li>
                    <li><a href="Contact.jsp">CONTACT</a></li>
                </ul>
            </div>
           
        </div>
    </div>
     <!--END NAV SECTION -->
      <!--HOME SECTION-->
    <div id="home-sec">

   
    <div class="container"  >
        <div class="row text-center">
            <div  class="col-md-12" >
                
        <h1>Registration Form</h1>
	<div class="ex">
		 <form action="loginServlet?action=Register" method="post">
 		 <center>
        <fieldset style="width: 300px">
            
            <table border =0 style="color:black">
                <tr>
                    <td>User Name</td>
                    <td><input type="text" name="username" required="required" /></td>
                </tr>
                <tr>
                	<td> </td>
                	<td> </td>
                </tr>
                	
                <tr>
                    <td>Password</td>
                    <td><input type="password" name="userpass" required="required" /></td>
                </tr>
                <tr>
                	<td> </td>
                	<td> </td>
                </tr>
                <tr>
                    <td>Confirm Password</td>
                    <td><input type="password" name="confirmpass" required="required" /></td>
                </tr>
                <tr>
                	<td> </td>
                	<td> </td>
                </tr>
                <tr>
                    <td>EMAIL ID</td>
                    <td><input type="text" name="email" required="required" /></td>
                </tr>
                <tr>
                	<td> </td>
                	<td> </td>
                </tr>
                <tr>
                	<td>CURRENCY</td>
                	<td><select name="Currency">
                	 <option value="0">USD</option>
       				 <option value="1">EUR</option>
       				
                	</select>
                	</td>
                	
                	</tr>
                <tr>
                <td>Opening Balance</td>
                <td><input type="number"  name="balance" required="required" /></td>
                </tr>
                <tr>
                    <td><input type="submit" value="Register" /></td>
                </tr>
                
            </table>
        </fieldset>
        <div style="color: #FF0000;">${errorMessage}</div>
        </center>
 		
    </form>

	</div>
                     
            </div>
        </div>
    </div>
         </div>
    

    <section  id="services-sec">
        <div class="container">
            <div class="row ">
               
                
                  </div>
               
                            
                    </div>
                    
                            
                    </div>
                </div>
          
        </div>
    </section>
    <!--END HOME SECTION-->
   
  

  
    

   

  
    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY  -->
    <script src="assets/plugins/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP CORE SCRIPT   -->
    <script src="assets/plugins/bootstrap.min.js"></script>  
     <!-- ISOTOPE SCRIPT   -->
    <script src="assets/plugins/jquery.isotope.min.js"></script>
    <!-- PRETTY PHOTO SCRIPT   -->
    <script src="assets/plugins/jquery.prettyPhoto.js"></script>    
    <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>

</body>
</html>
