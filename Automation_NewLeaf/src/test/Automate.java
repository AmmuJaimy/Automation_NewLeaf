
package test;


import utilities.common.*;

import utilities.readExcel.*;

import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import utilities.EnviornmentConstants.*;

public class Automate {
	//Initializing Log file writers
			public static Logger DeveloperLog = null;
			public static Logger ErrorLog = null;
	
public static void main(String[] args) throws Exception{
	Properties prop = new Properties();
	String propFileName = "setup.properties";
	ClassLoader loader = Thread.currentThread().getContextClassLoader();           
	InputStream inputStream = loader.getResourceAsStream(propFileName);
	prop.load(inputStream);		
	//System.out.println("Location is"+prop.getProperty("paths_location"));
	String paths_location = prop.getProperty("paths_location");
	Constants c = new Constants(paths_location);
	System.out.println(Constants.LOGFILE_PATH);
	System.setProperty("logfile.name",Constants.LOGFILE_PATH);
	DeveloperLog= Logger.getLogger("developerlog");
	ErrorLog = Logger.getLogger("errorlog");
	ErrorLog.info("Outside Workspace");
	
	//Examples to write into Log files
	DeveloperLog.info("This message is written to the DeveloperLog file");
	ErrorLog.error("This message is written to the ErrorLog file");
	
	
	//Xls_Reader xls_reader= new Xls_Reader("D:\\Selenium\\Satya\\Sri\\src\\config\\testcases\\config_1.xlsx");
	DeveloperLog.info("Constructor invoked"+System.getProperty("user.dir"));
	WebDriver driver=null; //Initializing Web Driver
	ReadExcelLoops readexcel=new ReadExcelLoops(); //Creating and Initializing object for readexcelloops that navigates within the excel sheets
	//Xls_Reader or_reader= new Xls_Reader("D:\\Selenium\\Satya\\Sri\\src\\config\\testcases\\OR_MASTER.xlsx");
	Xls_Reader test_reader_newpurchase= new Xls_Reader(Constants.NEWPURCHASE_PATH);
	//Xls_Reader test_reader= new Xls_Reader("D:\\Selenium\\Satya\\Sri\\src\\config\\testcases\\Sample_new_1.xlsx");
	//Xls_Reader scenario_reader= new Xls_Reader("D:\\Selenium\\Satya\\Sri\\src\\config\\testcases\\Scenario.xlsx");
	Xls_Reader test_reader_NewRefinance= new Xls_Reader(Constants.NEW_REFINANCE_PATH);
	Xls_Reader test_reader_NewProspectPurchase= new Xls_Reader(Constants.NEW_PROSPECT_PURCHASE);
	Xls_Reader test_reader_NewProspectRefinance= new Xls_Reader(Constants.NEW_PROSPECT_REFINANCE);
	DeveloperLog.info("Constructor invoked");
	String scenario=null;//To read the scenario id
	String testcaseid=null;
	scenario=readexcel.readScenario();//Read the Scenario sheet and returns the SCID
	//Modified on 5/5/2016 by Ammu for changing excel path.. start
	/*String testcase_path=System.getProperty("user.dir")+"\\Excel\\Test\\"+scenario+".xlsx";//Creates the name of the testcase sheet to be read from the SCID
	Xls_Reader test_reader=new Xls_Reader(testcase_path);//Object to read testcase file
	testcaseid=readexcel.readTestCase(test_reader, driver);*/  //Reads the test case sheet and returns TCID
	if(scenario.equalsIgnoreCase(Constants.NEWPURCHASE))
	{
	testcaseid=readexcel.readTestCase(test_reader_newpurchase,driver,scenario);
    //if(testcaseid.equalsIgnoreCase("CreateNewPurchase"))
    	//tests=readexcel.readTestSteps(test_reader, driver, xls_reader);
	
				}
	else if(scenario.equalsIgnoreCase(Constants.NEWREFINANCE)){
		testcaseid=readexcel.readTestCase(test_reader_NewRefinance, driver,scenario);
		
	}
	else if(scenario.equalsIgnoreCase(Constants.NEWPROSPECTPURCHASE))
		testcaseid=readexcel.readTestCase(test_reader_NewProspectPurchase, driver,scenario);
	else if(scenario.equalsIgnoreCase(Constants.NEWPROSPECTREFINANCE))
		testcaseid=readexcel.readTestCase(test_reader_NewProspectRefinance, driver,scenario);
	
	//End of modification
	//driver.close();//Driver closed
}	
		
	

}






