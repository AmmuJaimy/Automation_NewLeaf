package testng;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;

public class TestNGMain {
	public static void main(String[] args) {
		 
		TestListenerAdapter tla = new TestListenerAdapter();
		TestNG testng = new TestNG();
		 
		testng.setTestClasses(new Class[] { testng.AutomateTestNG.class });
		testng.addListener(new TestAnnoTransformer());
		testng.addListener(new CustomReporter());
		testng.addListener(tla);
		 
		testng.run();
		 
		}

}
