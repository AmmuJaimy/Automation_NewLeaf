package testng;

import org.testng.annotations.Test;

import applicationDependent.ReportWriters;
import applicationDependent.RunCredit;
import test.Automate;
import utilities.Webservices.*;
import org.apache.poi.xssf.usermodel.*;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import utilities.Webservices.*;
import utilities.EnviornmentConstants.Constants;
import utilities.common.WaitTool;
import utilities.common.Xls_Reader;
import utilities.functionLibrary.Webelement_Methods;

import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.testng.IAnnotationTransformer;
import org.testng.ITest;
import org.testng.TestNG;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

public class AutomateTestNG {
	public Xls_Reader or_reader;
	public Xls_Reader config_reader;
	public Xls_Reader scenario_reader;
	public Xls_Reader test_reader;
	public Xls_Reader test_reader_NewPurchase;
	public Xls_Reader test_reader_NewRefinance;
	public WebDriver driver=null;
	public String scenario=null;
	public ArrayList<String> testcaseid=new ArrayList<String>();
	public static Logger DeveloperLog = null;
	public static Logger ErrorLog = null;
	public static Object[][] data;

  /*@Test(priority=1, dataProvider="testcasedataProvider")
  public void executeTestCase(String testcaseid){
	  System.out.println("Inside Testcase"+testcaseid);
	  
  }*/
    
	@Test(dataProvider = "dataProvider")
	// public void invoketest(Object testcaseid, Object data)
	/*public void invoketest(String testcaseid,XSSFRow datarow=null,JSONObject obj=null,int flag=0) 
	{
		if(Constants.source.equalsIgnoreCase("Webservice"))
		{
			
		}
		else
		{
			XSSFRow datarow=(XSSFRow)data;
			automateLoanCenter()
		}
	 }*/
	  public void automateLoanCenter(String testcaseid, XSSFRow dataRow) throws Exception {
			System.out.println("Testcase is"+testcaseid);
			System.out.println("Row is"+dataRow.getRowNum());
			int dataSheetRowNum=dataRow.getRowNum();
			config_reader=new Xls_Reader(Constants.CONFIG_PATH);
			for(int i=2; i<=test_reader.getRowCount(Constants.TESTSTEPS);i++)//Loop to iterate through Teststeps sheet
			{
			String tcid=test_reader.getCellData(Constants.TESTSTEPS, Constants.TCID, i);//Read TCID from the teststeps sheet sequentially
		if(tcid.equalsIgnoreCase(testcaseid))//Checks whether the TCID from teststeps sheet and testcase sheet match
		{
		String xpath="";
		String element=test_reader.getCellData(Constants.TESTSTEPS, Constants.ELEMENT, i);//Read the element corresponding to the Test step
		String action= test_reader.getCellData(Constants.TESTSTEPS, Constants.ACTION, i);//Read the action corresponding to the teststep
		AutomateTestNG.DeveloperLog.info("Action is"+action);
		//System.out.println("action"+action);
		//System.out.println("value of orid"+element);
		String data="";
		String lookupsheet= test_reader.getCellData(Constants.TESTSTEPS, Constants.TCID, i);//Reads the TCID as the name of the datasheet
		String datafile=test_reader.getCellData(Constants.TESTSTEPS,Constants.DATAFILE,i);//Reads the datafile column to look for the datasheet and column pipe seperated values
		//System.out.println("Lookupsheet"+lookupsheet);
		//System.out.println("LookupCol"+datafile);
		//Steps to seperate the sheet name and column name fetched from the datafile column of teststeps sheet
		int p=datafile.indexOf('|');
		String lookupcol=datafile.substring(p+1);
		//System.out.println("Row count of lookupsheet "+test_reader.getRowCount(lookupsheet));
		if((!lookupsheet.equals(null))||(!lookupcol.equals(null)))
		 data=test_reader.getCellData(lookupsheet, lookupcol, dataSheetRowNum);
			if(!element.equals(null))
		   xpath=readORMaster(element);
				
				//System.out.println("XPATH"+xpath);	
			//WaitTool.waitForElementPresent(driver, By.xpath("//*[@id='divMainNavBar']/div/span"),30);
			if (action.equalsIgnoreCase(Constants.OPENBROWSER)){
				//System.out.println("Openbrowser");
				driver= Webelement_Methods.openBrowser(config_reader.getCellData(Constants.CONFIG,Constants.BROWSER,2));
			}
			else if(action.equalsIgnoreCase(Constants.NAVIGATE))		{
				String Url= config_reader.getCellData(Constants.CONFIG,Constants.URL,2);
				driver= Webelement_Methods.navigate(driver,Url);
				String validation=test_reader.getCellData(Constants.TESTSTEPS, Constants.VALIDATION,i);
				//System.out.println("Validation  " +validation);
				if(!validation.equals(null))
				{
					String validation_xpath=readORMaster(validation);
					//System.out.println("Validation xpath "+validation_xpath);
					if(!validation_xpath.equals(null))
					WaitTool.waitForElementPresent(driver, By.xpath(validation_xpath),20);
				}
				
			}
			else if(action.equalsIgnoreCase(Constants.MOUSEOVER)){
				try
				{
				WebElement menu=driver.findElement(By.xpath(xpath));
				//System.out.println(menu);
				Actions action1=new Actions(driver);
				action1.moveToElement(menu).perform();
				String validation=test_reader.getCellData(Constants.TESTSTEPS, Constants.VALIDATION,i);
				//System.out.println("Validation  " +validation);
				if(!validation.equals(null))
				{
					String validation_xpath=readORMaster(validation);
					//System.out.println("Validation xpath "+validation_xpath);
					if(!validation_xpath.equals(null))
					WaitTool.waitForElementPresent(driver, By.xpath(validation_xpath),20);
				}
				//System.out.println("Wait over");
				//System.out.println("MOUSEOVER");
				}
				catch(Exception e)
				{
					//System.out.println("Exception caught"+e);
					//APP_LOGS.error("Exception caught"+e);
				}
			}
			else if(action.equalsIgnoreCase(Constants.CLICK))			{
				
				try
				{
					// Function call modified by Aruna @ May 23 2016
					String Button_Text=driver.findElement(By.xpath(xpath)).getText();
					Webelement_Methods.Click(driver,xpath);
					System.out.println("Button Text is"+Button_Text);
					
					if(Button_Text.contains("New") && driver.findElement(By.xpath("//button[@class='imp-button-div-hs-ws-prim ok']")).isDisplayed())
					{
						driver.findElement(By.xpath("//button[@class='imp-button-div-hs-ws-prim ok']")).click();
					}

	/*							String parenthandle = driver.getWindowHandle();
						Set<String> windowhandles = driver.getWindowHandles();
						System.out.println("Window Handles " + windowhandles.size());
						System.out.println("Parent Handle " + parenthandle);
						if (windowhandles.size() > 1) {
							System.out.println("Window Handles " + windowhandles.size());
							System.out.println("Parent Handle " + parenthandle);
							for (String window : driver.getWindowHandles()) {
								System.out.println("Window " + window);
								if (!window.equals(parenthandle)) {
									Thread.sleep(5000);
									driver.switchTo().window(window);
									if (driver.findElements(By.xpath("//button[@class='imp-button-div-hs-ws-prim ok']"))
											.size() > 0) {

										System.out.println("Modal dialog found");
										driver.findElement(By.xpath("//button[@class='imp-button-div-hs-ws-prim ok']")).click();
										WaitTool.waitForElement(driver, By.xpath("//input[contains(@ng-class,'firstName')]"),
												120);
									}
								}
							}
						}*/
				}
				catch(Exception e)
				{
					//System.out.println("Exception caught"+e);
				}
				String validation=test_reader.getCellData(Constants.TESTSTEPS, Constants.VALIDATION,i);
				//System.out.println("Validation  " +validation);
				String validation_xpath=readORMaster(validation);
				//System.out.println("Validation xpath "+validation_xpath);
				if(!validation.equals(null))
				{
					if(validation.equalsIgnoreCase("button_CreditScore"))
					{
						WaitTool.waitForElementClickable(driver, By.xpath(validation_xpath),120);
						
							WebElement elementcredit=driver.findElement(By.xpath("//*[@id='CreditTab']/a/div"));
							if(!(elementcredit.isSelected()))
							{
							//System.out.println("Inside accept");
							driver=RunCredit.REO(driver,scenario);
							}
					}
					else
					{
					if(!validation_xpath.equals(null))
					WaitTool.waitForElementPresent(driver, By.xpath(validation_xpath),360);
					}
					}
				
			}
			else if (action.equalsIgnoreCase(Constants.CHECK))
			{
				//Modified by Shirisha on 21/05/2016 Start
				Webelement_Methods.Checkbox(driver.findElement(By.xpath(xpath)));
			}
				else if(action.equalsIgnoreCase(Constants.TYPE))
			{       // Function call written by Aruna @ May 23 2016
					Webelement_Methods.enterText(driver.findElement(By.xpath(xpath)),data);
			}
				else if (action.equalsIgnoreCase(Constants.DROP_SELECT)){
				// Function call written by Satya @ April 30 2016
				Webelement_Methods.selectElementByVisibleTextMethod(driver.findElement(By.xpath(xpath)),data);
			}
			else if(action.equalsIgnoreCase("ClickNext"))
			{
				// Function call modified by Aruna @ May 23 2016
				Webelement_Methods.ClickNext(driver);
				String validation=test_reader.getCellData("TESTSTEPS", "VALIDATION",i);
				//System.out.println("Validation  " +validation);
				WaitTool.waitForElementPresent(driver, By.xpath(validation),20);
			}
			else if(action.equalsIgnoreCase(Constants.RADIO_CLICK))
				// Function call written by Aruna @ May 23 2016
				driver = Webelement_Methods.SelectRadioYesNo(driver,data,xpath);
			else if(action.equalsIgnoreCase(Constants.TYPEZIP))
			{
				// Function call written by Aruna @ May 23 2016
				Webelement_Methods.enterText(driver.findElement(By.xpath(xpath)),data);
			}
			else if(action.equalsIgnoreCase(Constants.TAKE_SCREENSHOT))
			{
				String DestFolder=config_reader.getCellData(Constants.CONFIG,Constants.SNAPSHOT,2);
				String timeStamp = new SimpleDateFormat("yyyyMMMdd_HHmmss").format(Calendar.getInstance().getTime());
				String Destination = DestFolder+"\\"+"TCID"+tcid+timeStamp+".png";
				Webelement_Methods.takeScreenshotMethod(driver, Destination);
			}
			else if(action.equalsIgnoreCase("GenerateReport"))
			{
				Thread.sleep(6000);
				ReportWriters.GenerateReport(driver,tcid);
			}
			else if(action.equalsIgnoreCase("Give Delay"))
					Thread.sleep(20000);
			else if(action.equalsIgnoreCase("Add Product"))
				ReportWriters.AddProduct(driver, lookupcol,data);
			else
			System.out.println("No Such Action");
			
			
			}	
		

			}	
			
			
			}

	@Test(dataProvider = "dataProvider")
	  public void automateLoanCenterJSON(String testcaseid, JSONObject dataRow,int dataSheetRowNum) throws Exception {
			System.out.println("Testcase is"+testcaseid);
			//System.out.println("Row is"+dataRow.getRowNum());
		//int dataSheetRowNum=dataRow.getRowNum();
AutomateTestNG.DeveloperLog.info("Config"+Constants.CONFIG_PATH);
			config_reader=new Xls_Reader(Constants.CONFIG_PATH);
			for(int i=2; i<=test_reader.getRowCount(Constants.TESTSTEPS);i++)//Loop to iterate through Teststeps sheet
			{
			String tcid=test_reader.getCellData(Constants.TESTSTEPS, Constants.TCID, i);//Read TCID from the teststeps sheet sequentially
		if(tcid.equalsIgnoreCase(testcaseid))//Checks whether the TCID from teststeps sheet and testcase sheet match
		{
		String xpath="";
		String lookupcol="";
		String lookupsheet="";
		String lookupfile="";
		String element=test_reader.getCellData(Constants.TESTSTEPS, Constants.ELEMENT, i);//Read the element corresponding to the Test step
		String action= test_reader.getCellData(Constants.TESTSTEPS, Constants.ACTION, i);//Read the action corresponding to the teststep
		System.out.println("action"+action);
		System.out.println("value of orid"+element);
		String data="";
	   // String datalookup=
		lookupsheet= test_reader.getCellData(Constants.TESTSTEPS, Constants.TCID, i);//Reads the TCID as the name of the datasheet
		String datafile=test_reader.getCellData(Constants.TESTSTEPS,Constants.DATAFILE,i);//Reads the datafile column to look for the datasheet and column pipe seperated values
		System.out.println("Lookupsheet"+lookupsheet);
		System.out.println("datafile"+datafile);
		//Steps to seperate the sheet name and column name fetched from the datafile column of teststeps sheet
		int p=datafile.indexOf('|');
		if(!(datafile.equals("")))
		{
		lookupcol=datafile.substring(p+1);
		lookupfile=datafile.substring(0, p);
		
		System.out.println("Lookupfile   "+lookupfile);
		System.out.println("Row count of lookupsheet   "+test_reader.getRowCount(lookupsheet));
		}
		if((!(lookupcol.equals("")))&&(!(lookupfile.equalsIgnoreCase("Config"))))
		 {
			System.out.println("Lookup col not null");
			
			if((tcid.equalsIgnoreCase("Login"))&&(lookupfile.equalsIgnoreCase("Login")))
				data=webservices.executePost("http://52.72.40.149/Login/Login.ashx",lookupcol,dataSheetRowNum);
			else
			data=webservices.executePost("http://52.72.40.149/DataService/GetData.ashx",lookupcol,dataSheetRowNum);
			System.out.println("data from webservice" +data);
		 }
		 
		//System.out.println("Row count of lookupsheet "+test_reader.getRowCount(lookupsheet));
		/*if((!lookupsheet.equals(null))||(!lookupcol.equals(null)))
		 data=test_reader.getCellData(lookupsheet, lookupcol, dataSheetRowNum);*/
			if(!(element.equals(null)))
		   xpath=readORMaster(element);
				
				System.out.println("XPATH"+xpath);	
			//WaitTool.waitForElementPresent(driver, By.xpath("//*[@id='divMainNavBar']/div/span"),30);
			if (action.equalsIgnoreCase(Constants.OPENBROWSER)){
				//System.out.println("Openbrowser");
				driver= Webelement_Methods.openBrowser(config_reader.getCellData(Constants.CONFIG,Constants.BROWSER,2));
			}
			else if(action.equalsIgnoreCase(Constants.NAVIGATE))		{
				String Url= config_reader.getCellData(Constants.CONFIG,Constants.URL,2);
				driver= Webelement_Methods.navigate(driver,Url);
				String validation=test_reader.getCellData(Constants.TESTSTEPS, Constants.VALIDATION,i);
				//System.out.println("Validation  " +validation);
				if(!(validation.equals(null)))
				{
					String validation_xpath=readORMaster(validation);
					//System.out.println("Validation xpath "+validation_xpath);
					if(!validation_xpath.equals(null))
					WaitTool.waitForElementPresent(driver, By.xpath(validation_xpath),20);
				}
				
			}
			else if(action.equalsIgnoreCase(Constants.MOUSEOVER)){
				try
				{
				WebElement menu=driver.findElement(By.xpath(xpath));
				//System.out.println(menu);
				Actions action1=new Actions(driver);
				action1.moveToElement(menu).perform();
				String validation=test_reader.getCellData(Constants.TESTSTEPS, Constants.VALIDATION,i);
				//System.out.println("Validation  " +validation);
				if(!(validation.equals(null)))
				{
					String validation_xpath=readORMaster(validation);
					//System.out.println("Validation xpath "+validation_xpath);
					if(!validation_xpath.equals(null))
					WaitTool.waitForElementPresent(driver, By.xpath(validation_xpath),20);
				}
				//System.out.println("Wait over");
				//System.out.println("MOUSEOVER");
				}
				catch(Exception e)
				{
					//System.out.println("Exception caught"+e);
					//APP_LOGS.error("Exception caught"+e);
				}
			}
			else if(action.equalsIgnoreCase(Constants.CLICK))			{
				
				try
				{
					// Function call modified by Aruna @ May 23 2016
					System.out.println("Inside click");
					String Button_Text=driver.findElement(By.xpath(xpath)).getText();
					System.out.println("Inside click gettext" +Button_Text);
					Webelement_Methods.Click(driver,xpath);
					System.out.println("Button Text is"+Button_Text);
					
					if(Button_Text.contains("New") && driver.findElement(By.xpath("//button[@class='imp-button-div-hs-ws-prim ok']")).isDisplayed())
					{
						driver.findElement(By.xpath("//button[@class='imp-button-div-hs-ws-prim ok']")).click();
					}

	/*							String parenthandle = driver.getWindowHandle();
						Set<String> windowhandles = driver.getWindowHandles();
						System.out.println("Window Handles " + windowhandles.size());
						System.out.println("Parent Handle " + parenthandle);
						if (windowhandles.size() > 1) {
							System.out.println("Window Handles " + windowhandles.size());
							System.out.println("Parent Handle " + parenthandle);
							for (String window : driver.getWindowHandles()) {
								System.out.println("Window " + window);
								if (!window.equals(parenthandle)) {
									Thread.sleep(5000);
									driver.switchTo().window(window);
									if (driver.findElements(By.xpath("//button[@class='imp-button-div-hs-ws-prim ok']"))
											.size() > 0) {

										System.out.println("Modal dialog found");
										driver.findElement(By.xpath("//button[@class='imp-button-div-hs-ws-prim ok']")).click();
										WaitTool.waitForElement(driver, By.xpath("//input[contains(@ng-class,'firstName')]"),
												120);
									}
								}
							}
						}*/
				}
				catch(Exception e)
				{
					//System.out.println("Exception caught"+e);
				}
				String validation=test_reader.getCellData(Constants.TESTSTEPS, Constants.VALIDATION,i);
				//System.out.println("Validation  " +validation);
				String validation_xpath=readORMaster(validation);
				//System.out.println("Validation xpath "+validation_xpath);
				if(!validation.equals(null))
				{
					if(validation.equalsIgnoreCase("button_CreditScore"))
					{
						WaitTool.waitForElementClickable(driver, By.xpath(validation_xpath),120);
						
							WebElement elementcredit=driver.findElement(By.xpath("//*[@id='CreditTab']/a/div"));
							if(!(elementcredit.isSelected()))
							{
							//System.out.println("Inside accept");
							driver=RunCredit.REO(driver,scenario);
							}
					}
					else
					{
					if(!validation_xpath.equals(null))
					WaitTool.waitForElementPresent(driver, By.xpath(validation_xpath),360);
					}
					}
				
			}
			else if (action.equalsIgnoreCase(Constants.CHECK))
			{
				//Modified by Shirisha on 21/05/2016 Start
				Webelement_Methods.Checkbox(driver.findElement(By.xpath(xpath)));
			}
				else if(action.equalsIgnoreCase(Constants.TYPE))
			{       // Function call written by Aruna @ May 23 2016
					Webelement_Methods.enterText(driver.findElement(By.xpath(xpath)),data);
			}
				else if (action.equalsIgnoreCase(Constants.DROP_SELECT)){
				// Function call written by Satya @ April 30 2016
				Webelement_Methods.selectElementByVisibleTextMethod(driver.findElement(By.xpath(xpath)),data);
			}
			else if(action.equalsIgnoreCase("ClickNext"))
			{
				// Function call modified by Aruna @ May 23 2016
				Webelement_Methods.ClickNext(driver);
				String validation=test_reader.getCellData("TESTSTEPS", "VALIDATION",i);
				//System.out.println("Validation  " +validation);
				WaitTool.waitForElementPresent(driver, By.xpath(validation),20);
			}
			else if(action.equalsIgnoreCase(Constants.RADIO_CLICK))
				// Function call written by Aruna @ May 23 2016
				driver = Webelement_Methods.SelectRadioYesNo(driver,data,xpath);
			else if(action.equalsIgnoreCase(Constants.TYPEZIP))
			{
				// Function call written by Aruna @ May 23 2016
				Webelement_Methods.enterText(driver.findElement(By.xpath(xpath)),data);
			}
			else if(action.equalsIgnoreCase(Constants.TAKE_SCREENSHOT))
			{
				String DestFolder=config_reader.getCellData(Constants.CONFIG,Constants.SNAPSHOT,2);
				String timeStamp = new SimpleDateFormat("yyyyMMMdd_HHmmss").format(Calendar.getInstance().getTime());
				String Destination = DestFolder+"\\"+"TCID"+tcid+timeStamp+".png";
				Webelement_Methods.takeScreenshotMethod(driver, Destination);
			}
			else if(action.equalsIgnoreCase("GenerateReport"))
			{
				Thread.sleep(4000);
				ReportWriters.GenerateReport(driver,tcid);
			}
			else if(action.equalsIgnoreCase("Give Delay"))
			{
				System.out.println("Inside delay");	
				Thread.sleep(10000);
			}
			else if(action.equalsIgnoreCase("Add Product")){
				
				Thread.sleep(4000);
				ReportWriters.AddProduct(driver, lookupcol,data);
			
			}else
			System.out.println("No Such Action");
			
			
			}	
		

			}	
			
			
			}
	@DataProvider
	  public Object[][] dataProvider() throws Exception {
//		System.out.println("Source in dataprovider"+Constants.source);
		  if(Constants.source.equalsIgnoreCase("Webservice"))
		  {
			  AutomateTestNG.DeveloperLog.info("Returning from webservice");
			  return dataProviderJSON();
		  }
		     
		  else 
		  {
			  AutomateTestNG.DeveloperLog.info("Returning from Excelsheet");
		  
			  return dataProviderExcel();
		  }
		 // else
		//	  return null;
	  }
	
	public Object[][] dataProviderJSON() throws Exception
	{
		  int arraySize=0;
		  
		  arraySize=webservices.arraylen("http://52.72.40.149/DataService/GetData.ashx");
		  
		  	AutomateTestNG.DeveloperLog.info("Arraysize  "+arraySize);
			  data=new Object[arraySize+1][3];
			 AutomateTestNG.DeveloperLog.info("Arraysize  "+arraySize);
			  int a=0;
			  int flag=0;
			  
			// for(int i=0;i<testcaseid.size();i++)
			// {
				  for(int p=0;p<=arraySize;p++)
					{
						
					  
					 if(p==0)//if(testcaseid.get(i).contentEquals("Login"))
					 {
						 data[a][0]=testcaseid.get(0);
						 data[a][1]=webservices.ReturnPost("http://52.72.40.149/Login/Login.ashx", p);
						 data[a][2]=p;
						 
					 }
					 else
					 {
						 data[a][0]=testcaseid.get(1);
						 data[a][1]=webservices.ReturnPost("http://52.72.40.149/DataService/GetData.ashx", p-1);
						 data[a][2]=p-1;
					 }
					 AutomateTestNG.DeveloperLog.info("data provider   "+data[a][1]);
					  a++;
					}
					
			//  } 
					  
					
					
					 
						
						
				 // AutomateTestNG.DeveloperLog.info("Value of loop iterator" +p);
				  AutomateTestNG.DeveloperLog.info("Data provider" +data);
				  return data;
		 // }
		 // }
		 // }
		  /*catch(Exception e)
		  {
			  AutomateTestNG.DeveloperLog.info("Exception in dataprovider  "+e);
			  return null;
		  }*/
	}
	
public Object[][] dataProviderExcel()
{
	int arraySize=0;
	  for(int i=0;i<testcaseid.size();i++)
	  {
		  for(int p=2;p<=test_reader.getRowCount(testcaseid.get(i));p++)
			{
				String data_runmode=test_reader.getCellData(testcaseid.get(i), "RunMode", p);
				String data_firstname=test_reader.getCellData(testcaseid.get(i), "FirstName", p);
				//System.out.println("value of execute"+execute);
				if(data_runmode.equals("Y"))
				{
					AutomateTestNG.DeveloperLog.info("Testcaseid  "+testcaseid.get(i));
					AutomateTestNG.DeveloperLog.info("Firstname  "+data_firstname);
					arraySize++;
				}
				}
	  }
	  AutomateTestNG.DeveloperLog.info("Arraysize  "+arraySize);
		 data=new Object[arraySize][2];
		  int a=0;
		  for(int i=0;i<testcaseid.size();i++)
		  {
			  for(int p=2;p<=test_reader.getRowCount(testcaseid.get(i));p++)
				{
					String data_runmode=test_reader.getCellData(testcaseid.get(i), "RunMode", p);
					//System.out.println("value of execute"+execute);
					if(data_runmode.equals("Y"))
					{
data[a][0]=testcaseid.get(i);
data[a][1]=test_reader.getRow(testcaseid.get(i),p);
AutomateTestNG.DeveloperLog.info("data row is"+test_reader.getRow(testcaseid.get(i),p).getRowNum());
AutomateTestNG.DeveloperLog.info("value of a  "+a);
a++;
					}
					}
			
	  }
		  return data;
}
  /*@DataProvider
  public Object[][] dataProvider() {
	  int arraySize=0;
	  for(int i=0;i<testcaseid.size();i++)
	  {
		  for(int p=2;p<=test_reader.getRowCount(testcaseid.get(i));p++)
			{
				String data_runmode=test_reader.getCellData(testcaseid.get(i), "RunMode", p);
				//System.out.println("value of execute"+execute);
				if(data_runmode.equals("Y"))
				{
					arraySize++;
				}
				}
	  }
		  Object[][] data=new Object[arraySize][2];
		  int a=0;
		  for(int i=0;i<testcaseid.size();i++)
		  {
			  for(int p=2;p<=test_reader.getRowCount(testcaseid.get(i));p++)
				{
					String data_runmode=test_reader.getCellData(testcaseid.get(i), "RunMode", p);
					//System.out.println("value of execute"+execute);
					if(data_runmode.equals("Y"))
					{
data[a][0]=testcaseid.get(i);
data[a][1]=test_reader.getRow(testcaseid.get(i),p);
AutomateTestNG.DeveloperLog.info("data provider   "+data[a][1]);
a++;
					}
					}
			
	  }
		  AutomateTestNG.DeveloperLog.info("Data provider" +data);
		  return data;
  }*/
/*  @DataProvider
  public Object[][] testcasedataProvider(){
	  String testcase_runmode=null;
	  int count=0;
	  for(int l=2;l<=test_reader.getRowCount(Constants.TESTCASES);l++)//Loop to iterate through the Testcase sheet
		{
		  testcase_runmode = test_reader.getCellData(Constants.TESTCASES, Constants.RUNMODE, l);//to read the runmode for each testcase
		  if(testcase_runmode.equalsIgnoreCase("Y"))//Identify Testcase marked with runmode Y
			{
			  count++;
			}
		}
	  Object testcaseid[][]=new Object[count][1];
	  int i=0;
	  for(int l=2;l<=test_reader.getRowCount(Constants.TESTCASES);l++)//Loop to iterate through the Testcase sheet
		{
		  testcase_runmode = test_reader.getCellData(Constants.TESTCASES, Constants.RUNMODE, l);//to read the runmode for each testcase
		  if(testcase_runmode.equalsIgnoreCase("Y"))//Identify Testcase marked with runmode Y
			{
			  testcaseid[i][0]=test_reader.getCellData(Constants.TESTCASES, Constants.TCID, l);
			  i++;
			}
		}
	  return testcaseid;
  }*/
  @BeforeTest
  public void beforeTest() throws IOException {
	  Properties prop = new Properties();
		String propFileName = "setup.properties";
		ClassLoader loader = Thread.currentThread().getContextClassLoader();           
		InputStream inputStream = loader.getResourceAsStream(propFileName);
		prop.load(inputStream);		
		String paths_location = prop.getProperty("paths_location");
		//Constants.source=prop.getProperty("source");
		
		Constants c = new Constants(paths_location);
		config_reader=new Xls_Reader(Constants.CONFIG_PATH);
		Constants.source=config_reader.getCellData(Constants.CONFIG,Constants.DATA_SOURCE,2);
		System.out.println("Source"+Constants.source);
		System.setProperty("logfile.name",Constants.LOGFILE_PATH);
	DeveloperLog = Logger.getLogger("developerlog");
	ErrorLog = Logger.getLogger("errorlog");
		AutomateTestNG.DeveloperLog.info("Inside Before Test");
		or_reader= new Xls_Reader(Constants.OR_PATH);
		config_reader= new Xls_Reader(Constants.CONFIG_PATH);
		scenario_reader= new Xls_Reader(Constants.SCENARIO_PATH);
		try
		{
		  String scenario_runmode=null;
		  for(int i=2;i<=scenario_reader.getRowCount(Constants.SCENARIO);i++)//Loop to iterate through the scenario sheet
			{
				scenario_runmode=scenario_reader.getCellData(Constants.SCENARIO, Constants.RUNMODE, i);//to read the runmode for each scenario
				if(scenario_runmode.equalsIgnoreCase("Y"))//To identify SCID marked with Y
				{
					scenario=scenario_reader.getCellData(Constants.SCENARIO, Constants.SCID, i);//Get the SCID
					scenario=scenario.toUpperCase();
					break;
				}
			}
		  AutomateTestNG.DeveloperLog.info("Scenario..."+scenario);
		  if(scenario.equalsIgnoreCase(Constants.NEWPURCHASE)) {
			  test_reader=new Xls_Reader(Constants.NEWPURCHASE_PATH);
			  AutomateTestNG.DeveloperLog.info(Constants.NEWPURCHASE_PATH);	
		  } else if(scenario.equalsIgnoreCase(Constants.NEWREFINANCE)){
					 test_reader=new Xls_Reader(Constants.NEW_REFINANCE_PATH);
				 }
		  String testreader_runmode=null;
		for(int l=2;l<=test_reader.getRowCount(Constants.TESTCASES);l++)//Loop to iterate through the Testcase sheet
		{
		testreader_runmode = test_reader.getCellData(Constants.TESTCASES, Constants.RUNMODE, l);//to read the runmode for each testcase
		//System.out.println("RUnmode"+testreader_runmode);
		if(testreader_runmode.equalsIgnoreCase("Y"))//Identify Testcase marked with runmode Y
		{
			String tcid=test_reader.getCellData(Constants.TESTCASES, Constants.TCID, l);
			testcaseid.add(tcid);
			AutomateTestNG.DeveloperLog.info(testcaseid.size());
		}
		}
		}
		catch(Exception e){
			System.out.println("Exception caught"+e.getMessage());
			
		}
		
		
  }
  public String readORMaster(String orid)
  {try
  {
	  Xls_Reader or_reader= new Xls_Reader(Constants.OR_PATH);
	  //System.out.println("Inside xpath");
	  String xpath=null;
	  for(int k=2;k<=or_reader.getRowCount(Constants.OR_MASTER);k++)
			
	   {
		////System.out.println("Inside xpath loop");
		if(orid.equalsIgnoreCase(or_reader.getCellData(Constants.OR_MASTER, Constants.ELEMENT, k)))
			{
				
				xpath= or_reader.getCellData(Constants.OR_MASTER,Constants.XPATH,k);
				////System.out.println("value of action"+action);
				//System.out.println("value of xpath"+xpath);
				break;
			}
		} 
	  return xpath;
	  
  }catch(Exception e)
  {
	  return "";
  }
  }

}


