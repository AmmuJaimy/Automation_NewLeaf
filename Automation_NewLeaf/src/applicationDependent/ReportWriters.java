package applicationDependent;

import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import testng.*;
import test.Automate;
import utilities.EnviornmentConstants.Constants;
import utilities.common.WaitTool;
import utilities.common.Xls_Reader;
import utilities.functionLibrary.Webelement_Methods;

public class ReportWriters {
	public static boolean isReportGenerated=true;
	public static void GenerateReport(WebDriver driver, String tcid) {
		try{
			isReportGenerated=false;
			Xls_Reader reportWriter = new Xls_Reader(Constants.REPORT_WRITER_PATH);
		String timeStamp = new SimpleDateFormat("yyyyMMMdd_HHmmss").format(Calendar.getInstance().getTime());
		String sheetName = "tcid" + timeStamp;
		reportWriter.addSheet(sheetName);
		Xls_Reader reportReader = new Xls_Reader(Constants.REPORT_READER_PATH);
		for (int i = 2; i <= reportReader.getRowCount("COLUMNNAMES"); i++) {
			reportWriter.addColumn(sheetName, reportReader.getCellData("COLUMNNAMES", "COLUMN NAMES", i));
		}
		WebElement productscope = driver.findElement(By.xpath(getXpath(reportReader,"scope_EligibleProducts")));
		List<WebElement> productgroups = productscope.findElements(By.xpath(getXpath(reportReader,"scope_ProductGroups")));
		int rowNum = 2;
		int iteratedRowNum = 0;
		for (int i = 0; i < productgroups.size(); i++) {
			WebElement productgrouping = productgroups.get(i);
			WebElement productgroup = productgrouping.findElement(By.xpath(getXpath(reportReader,"label_ProductGroup")));
			reportWriter.setCellData(sheetName, reportReader.getCellData("COLUMNNAMES", "COLUMN NAMES", 2), rowNum, productgroup.getText());
			rowNum++;
			int tempRowNum = rowNum;
			for (int readerRowNum = 2; readerRowNum <= reportReader.getRowCount("COLUMNNAMES"); readerRowNum++) {
				String ColumnName = reportReader.getCellData("COLUMNNAMES", "COLUMN NAMES", readerRowNum);
				String element=reportReader.getCellData("COLUMNNAMES", "ELEMENT", readerRowNum);
				String xpath = getXpath(reportReader,element);
				List<WebElement> data = productgrouping.findElements(By.xpath(xpath));
				for (int k = 0; k < data.size(); k++) {
					reportWriter.setCellData(sheetName, ColumnName, tempRowNum, data.get(k).getText());
					tempRowNum++;
					iteratedRowNum = tempRowNum;
				}
				tempRowNum = rowNum;
			}
			rowNum = iteratedRowNum + 1;
		}
		isReportGenerated=true;
	}
		catch(Exception e)
		{
			AutomateTestNG.DeveloperLog.info("No Products Found"+ e.getMessage());
			Automate.ErrorLog.info("No Products Found"+ e.getMessage());
			
		}
	}

	public static WebDriver AddProduct(WebDriver driver, String lookupcol, String data)  {
		Xls_Reader reportReader;
		
		try {
			while(!isReportGenerated)
				Thread.sleep(1000);
			System.out.println("Entering addproduct");
			reportReader = new Xls_Reader(Constants.REPORT_READER_PATH);
		WebElement productscope = driver.findElement(By.xpath(getXpath(reportReader,"scope_EligibleProducts")));
		List<WebElement> productname = productscope.findElements(By.xpath(getXpath(reportReader,"label_ProductName")));
		List<WebElement> productidentifier = productscope.findElements(By.xpath(getXpath(reportReader,"label_ProductId")));
		List<WebElement> updatelink = productscope.findElements(By.xpath(getXpath(reportReader,"link_Update")));
		List<WebElement> productgrouping = productscope.findElements(By.xpath(getXpath(reportReader,"label_ProductGroup")));
		List<WebElement> productgroups = productscope.findElements(By.xpath(getXpath(reportReader,"scope_ProductGroups")));
		Xls_Reader loanNumberWriter = new Xls_Reader(Constants.LOAN_NUMBER_WRITER_PATH);
		if (lookupcol.equalsIgnoreCase("Product ID")) {
			for (int i = 0; i < productidentifier.size(); i++) {
				String prodid = productidentifier.get(i).getText();
				if (prodid.equals(data))
				{
					System.out.println("Update link no--" + i + "--" + updatelink.get(i).getText());
					updatelink.get(i).click();
				}
			}
		} else if (lookupcol.equalsIgnoreCase("Product Name")) {
			for (int i = 0; i < productname.size(); i++) {
				String prodname = productname.get(i).getText();
				if (prodname.equals(data))
					updatelink.get(i).click();
			}
		} else if (lookupcol.equalsIgnoreCase("Product Type")) {
			System.out.println("Inside product type");
			for (int i = 0; i < productgrouping.size(); i++) {
				String producttype = productgrouping.get(i).getText();
				if (producttype.contains(data)) {
					productgroups.get(i).findElement(By.xpath("." + reportReader.getCellData("OR", "XPATH",
							reportReader.getCellRowNum("OR", "ELEMENT", "link_Update")))).click();
				break;
				}
			}
		}
		Xls_Reader orReader=new Xls_Reader(Constants.OR_PATH);
		if(!driver.findElements(By.xpath("//label[@class='imp-lbl-radio']")).isEmpty())
		{
			if(driver.findElement(By.xpath("//label[@class='imp-lbl-radio']")).isDisplayed())
			{
			String xpath=orReader.getCellData(Constants.OR_MASTER,"XPATH",orReader.getCellRowNum(Constants.OR_MASTER, "ELEMENT", "radio_TaxQuestions"));
			Webelement_Methods.SelectRadioYesNo(driver, "No", xpath);
			xpath=orReader.getCellData(Constants.OR_MASTER,"XPATH",orReader.getCellRowNum(Constants.OR_MASTER, "ELEMENT", "textbox_TaxQuestions"));
			Webelement_Methods.enterText(driver.findElement(By.xpath(xpath)), "0");
			xpath=orReader.getCellData(Constants.OR_MASTER,"XPATH",orReader.getCellRowNum(Constants.OR_MASTER, "ELEMENT", "button_TaxQuestions"));
			driver.findElement(By.xpath(xpath)).click();
		}
		}
		WaitTool.waitForElementPresent(driver, By.xpath(getXpath(reportReader,"label_LoanProduct")),200);
		String LoanNumber="--NONE--";
		String ApplicantName="--NONE--";
		String LoanProduct="--NONE--";
		if(driver.findElements(By.xpath(getXpath(reportReader,"label_LoanNumber"))).size()>0)
		LoanNumber = driver.findElement(By.xpath(getXpath(reportReader,"label_LoanNumber"))).getText();
		if(driver.findElements(By.xpath(getXpath(reportReader,"label_ApplicantName"))).size()>0)
		ApplicantName = driver.findElement(By.xpath(getXpath(reportReader,"label_ApplicantName"))).getText();
		if(driver.findElements(By.xpath(getXpath(reportReader,"label_LoanProduct"))).size()>0)
		LoanProduct = driver.findElement(By.xpath(getXpath(reportReader,"label_LoanProduct"))).getAttribute("innerText");
		String timeStamp = new SimpleDateFormat("yyyyMMMdd_HHmmss").format(Calendar.getInstance().getTime());
		int temp=loanNumberWriter.getRowCount("LOAN_NUMBERS") + 1;
		loanNumberWriter.setCellData("LOAN_NUMBERS", "LOAN NUMBER",temp,
				LoanNumber);
		loanNumberWriter.setCellData("LOAN_NUMBERS", "APPLICANT NAME",temp, ApplicantName);
		loanNumberWriter.setCellData("LOAN_NUMBERS", "PRODUCT", temp,
				LoanProduct);
		loanNumberWriter.setCellData("LOAN_NUMBERS", "DATE/TIME"
, temp,timeStamp);
				} catch (Exception e) {
			System.out.println("Product could not be added");
			System.out.println(e.getMessage());
		}
		return driver;
	}

public static String getXpath(Xls_Reader reportReader, String element)
{
	return reportReader.getCellData("OR", "XPATH",
			reportReader.getCellRowNum("OR", "ELEMENT", element));
}
}

