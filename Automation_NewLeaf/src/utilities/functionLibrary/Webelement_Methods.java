// Class modified by Aruna @ May 23 2016 to write methods to call actions on Web Elements

package utilities.functionLibrary;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.Select;

import applicationDependent.RunCredit;
import test.*;
import testng.AutomateTestNG;

public class Webelement_Methods {

		public static WebDriver openBrowser(String Browser)
		{
			WebDriver driver=null;
			if(driver==null)
			{
			if (Browser.equalsIgnoreCase("Firefox"))
			{
				driver = new FirefoxDriver();			
				driver.manage().window().maximize();
				driver.manage().window().setSize(new Dimension(1920,1080));		
				}
			else if(Browser.equalsIgnoreCase("Chrome")||Browser.equalsIgnoreCase("GoogleChrome"))
			{
				System.setProperty("webdriver.chrome.driver", "C:\\chromedriver_win32\\chromedriver.exe");
				driver = new ChromeDriver();
				driver.manage().window().maximize();
				AutomateTestNG.DeveloperLog.info("Chrome Browser");
				//driver.manage().window().setSize(new Dimension(1920,1080));		
				}
			else if(Browser.equalsIgnoreCase("IE")||Browser.equalsIgnoreCase("InternetExplorer"))
			{
				System.setProperty("webdriver.ie.driver","C:\\Users\\Srinivas\\Downloads\\IEDriverServer.exe");
				driver = new InternetExplorerDriver();
				driver.manage().window().maximize();
				//driver.manage().window().setSize(new Dimension(1920,1080));		
				}
			}
			return driver;
		}
		public static WebDriver navigate(WebDriver driver, String Url)
		{
			driver.get(Url);
	    	return driver;
		}
		//Method to take screenshot of the webpage
		public static void takeScreenshotMethod(WebDriver driver, String Destination)
				throws Exception {
				File f = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(f, new File(Destination));
				}
	 	public static void selectElementByVisibleTextMethod(WebElement element, String Name) {
			AutomateTestNG.DeveloperLog.info("Inside Select");
	 		Select selectitem = new Select(element);
			selectitem.selectByVisibleText(Name);
			}
	 	public static void selectElementByValueMethod(WebElement element, String value) {
	 			Select selectitem = new Select(element);
	 			selectitem.selectByValue(value);
	 			}

	 	public static String selectElementByIndexMethod(WebElement element, int index) {
	 			Select selectitem = new Select(element);
	 			selectitem.selectByIndex(index);
	 			WebElement option=selectitem.getFirstSelectedOption();
	 			
	 			return option.getText();
	 			}
	 	public static void Select_The_Checkbox(WebElement element) {
			try {
	            if (element.isSelected()) {
	               System.out.println("Checkbox: " + element + "is already selected");
	            } else {
	            	// Select the checkbox
	                element.click();
	            }
	        } catch (Exception e) {
	        	System.out.println("Unable to select the checkbox: " + element);
	        }
			
		}
	 	public static void Checkbox(WebElement element) 
	 	{
	 		try
	 		{
	 			element.click();
	 		}
	 		catch(Exception e)
	 		{
	 			
	 		}
	 	}



		public static void DeSelect_The_Checkbox(WebElement element) {
			try {
	            if (element.isSelected()) {
	            	//De-select the checkbox
	                element.click();
	            } else {
	            	System.out.println("Checkbox: "+element+"is already deselected");
	            }
	        } catch (Exception e) {
	        	System.out.println("Unable to deselect checkbox: "+element);
	        }
	    }		
        public static WebDriver SelectRadioYesNo(WebDriver driver, String data, String xpath) {
			
			List <WebElement> navButton = driver.findElements(By.xpath(xpath));
			int count = navButton.size();

			  	for(int i=0;i<count;++i)
			  	{
			  		String YesNoValue = navButton.get(i).getText();
			  		if (YesNoValue.toLowerCase().equals(data.toLowerCase()))
			  		{
			  			navButton.get(i).click();
			  			break;
			  		}
			  	}
			  	return driver;
			  }
        public static void enterText (WebElement element, String data)throws Exception {
	
        	
        	if(element.isEnabled())
        	{
				element.clear();
	
                  element.sendKeys(data);
                  Thread.sleep(300);
              }	
        }

        public static void ClickNext(WebDriver driver){
        	
               	String xpath = ".//button[contains(@ng-click,'nextPreviousNavigation')]";
	            List <WebElement> navButton = driver.findElements(By.xpath(xpath));
	            int count = navButton.size();
	
	              for (int i=0;i<count;++i)
	                 {
		               String str1 = navButton.get(i).getText();
		               if (str1.equals("Next"))
		                  {  
			                 navButton.get(i).click();
		}
	}
}
        	
		public static void Click(WebDriver driver, String xpath) {
			//Modified by Ammu on 26th May 2016 for introduing try catch loop
			try
			{
        	List <WebElement> navButton = driver.findElements(By.xpath(xpath));
        	System.out.println("Inside click n webelements");
        	int count = navButton.size();
        	// AutomateTestNG.DeveloperLog.info("Count of button  "+count);
        	AutomateTestNG.DeveloperLog.info(count);
        	System.out.println("Count of button  "+count);
			if(count>1)
			for (int x=0;x<count;++x)
			{
				String str1 = navButton.get(x).getText();
				//System.out.println(str1);
				if (str1.equals("Next"))
				{
					navButton.get(x).click();
					AutomateTestNG.DeveloperLog.info("Clicked");
					break;
				}
			}
			
			else{
		driver.findElement(By.xpath(xpath)).click();
		AutomateTestNG.DeveloperLog.info("Clicked outside the count");
		System.out.println("Clicked outside the count");
			}
			
			}
		catch (Exception e)
			{
			Automate.ErrorLog.error("Exception caught"+e);
			System.out.println("Exception caught"+e);
			}
		}
}



