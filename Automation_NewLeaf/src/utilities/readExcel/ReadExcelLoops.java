/*
` Function Created by Ammu on 27th April 2016
 For reading Excel sheets used in the application and perform actions accordingly
 *
 */

//Ammu added
package utilities.readExcel;
import org.openqa.selenium.interactions.*;
import test.*;
import applicationDependent.ReportWriters;
import applicationDependent.RunCredit;
import utilities.common.*;
import utilities.functionLibrary.*;
import utilities.Webservices.*;
import org.openqa.selenium.Dimension;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List; 
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utilities.EnviornmentConstants.*;

public class ReadExcelLoops {
	//public String scenario=null;
	public String browser=null;
	public ReadExcelLoops()
	{
		
	}
	//Function to read Scenario sheet
	public String readScenario() 
	{
		try
		{
			//Xls_reader object to be replaced in the constants.java
		Xls_Reader scenario_reader= new Xls_Reader(Constants.SCENARIO_PATH);//Object to read celldata
		String runmode=null;//to select the runmode
		String scenario=null;
		for(int i=2;i<=scenario_reader.getRowCount(Constants.SCENARIO);i++)//Loop to iterate through the scenario sheet
		{
			runmode=scenario_reader.getCellData(Constants.SCENARIO, Constants.RUNMODE, i);//to read the runmode for each scenario
			if(runmode.equalsIgnoreCase("Y"))//To identify SCID marked with Y
			{
				scenario=scenario_reader.getCellData(Constants.SCENARIO, Constants.SCID, i);//Get the SCID
				scenario=scenario.toUpperCase();
				Automate.DeveloperLog.info("Scenario   "+scenario);
			}
		}
		return scenario;//Return the SCID
		}catch(Exception e)
		{
			Automate.ErrorLog.error("Exception  "+e);
			return "";
		}
		}
	
	//Function to read TestCases
	public String readTestCase(Xls_Reader test_reader,WebDriver driver,String scenario)
	{
		try
		{
			Automate.DeveloperLog.info("Reading Testcases" );
			Xls_Reader xls_reader= new Xls_Reader(Constants.CONFIG_PATH);//Object to read the cell value of Testcase sheet
			//Xls_Reader test_reader= new Xls_Reader("D:\\Selenium\\Satya\\Sri\\src\\config\\testcases\\Sample_new_1.xlsx");
			String runmode=null;
			String testcaseid=null;
		for(int l=2;l<=test_reader.getRowCount(Constants.TESTCASES);l++)//Loop to iterate through the Testcase sheet
		{
		runmode = test_reader.getCellData(Constants.TESTCASES, Constants.RUNMODE, l);//to read the runmode for each testcase
		Automate.DeveloperLog.info("Runmode    "+runmode);;
		if(runmode.equalsIgnoreCase("Y"))//Identify Testcase marked with runmode Y
		{
	         testcaseid = test_reader.getCellData(Constants.TESTCASES, Constants.TCID, l);//Identify the TCID
	         Automate.DeveloperLog.info("Testcaseid    "+testcaseid);
			//break;
	         if(testcaseid.equalsIgnoreCase("LOGIN"))
	        	 driver=readTestSteps(test_reader,xls_reader,testcaseid,driver,scenario,0);//Read the test steps corresponding to the selected testcase.Move to teststeps read function

	         else
	         for(int p=0;p<webservices.arraylen("http://52.72.40.149/DataService/GetData.ashx");p++)
	         {
					//String execute=test_reader.getCellData(testcaseid, "RunMode", p);
					//System.out.println("value of execute"+execute);
					//if(execute.equals("Y"))
					//{
					//	Automate.DeveloperLog.info(test_reader.getCellData(testcaseid, "FirstName", p));
			driver=readTestSteps(test_reader,xls_reader,testcaseid,driver,scenario,p);//Read the test steps corresponding to the selected testcase.Move to teststeps read function
		//}
		}
		}
		}
		return testcaseid;
		}catch(Exception e)
		{
			Automate.ErrorLog.error("Exception  "+e);
			return "";
		}
	}
	//Function to read Teststeps	
  public  WebDriver readTestSteps(Xls_Reader test_reader,Xls_Reader xls_reader,String testcaseid,WebDriver driver,String scenario, int rownum) throws Exception
  {
	  
	 //LoadURL lu=new LoadURL();
	  Automate.DeveloperLog.info("Row count of TESTSTEPS    "+test_reader.getRowCount(Constants.TESTSTEPS));
		for(int i=2; i<=test_reader.getRowCount(Constants.TESTSTEPS);i++)//Loop to iterate through Teststeps sheet
		{
			String tcid=test_reader.getCellData(Constants.TESTSTEPS, Constants.TCID, i);//Read TCID from the teststeps sheet sequentially
			Automate.DeveloperLog.info("TCID     "+tcid);
			if(tcid.equalsIgnoreCase(testcaseid))//Checks whether the TCID from teststeps sheet and testcase sheet match
			{
			String xpath="";
			String lookupcol="";
			String lookupfile="";
			String element=test_reader.getCellData(Constants.TESTSTEPS, Constants.ELEMENT, i);//Read the element corresponding to the Test step
			String action= test_reader.getCellData(Constants.TESTSTEPS, Constants.ACTION, i);//Read the action corresponding to the teststep
			Automate.DeveloperLog.info("action     "+action);
			Automate.DeveloperLog.info("value of orid    "+element);
			String data="";
			String lookupsheet= test_reader.getCellData(Constants.TESTSTEPS, Constants.TCID, i);//Reads the TCID as the name of the datasheet
			String datafile=test_reader.getCellData(Constants.TESTSTEPS,Constants.DATAFILE,i);//Reads the datafile column to look for the datasheet and column pipe seperated values
			Automate.DeveloperLog.info("Lookupsheet      "+lookupsheet);
			Automate.DeveloperLog.info("LookupCol       "+datafile);
			//Steps to seperate the sheet name and column name fetched from the datafile column of teststeps sheet
			int p=datafile.indexOf('|');
			if(!datafile.equals(""))
			{
			lookupcol=datafile.substring(p+1);
			lookupfile=datafile.substring(0, p);
			}
			Automate.DeveloperLog.info("Lookupfile   "+lookupfile);
			Automate.DeveloperLog.info("Row count of lookupsheet   "+test_reader.getRowCount(lookupsheet));
			if((!lookupcol.equals(""))&&(!lookupfile.equalsIgnoreCase("Config")))
			 {
				Automate.DeveloperLog.info("Lookup col not null");
				
				if((tcid.equalsIgnoreCase("Login"))&&(lookupfile.equalsIgnoreCase("Login")))
					data=webservices.executePost("http://52.72.40.149/Login/Login.ashx",lookupcol,rownum);
				else
				data=webservices.executePost("http://52.72.40.149/DataService/GetData.ashx",lookupcol,rownum);
			 }
				if(!element.equals(null))
			   xpath=readORMaster(element);
					
				Automate.DeveloperLog.info("XPATH     "+xpath);	
				//WaitTool.waitForElementPresent(driver, By.xpath("//*[@id='divMainNavBar']/div/span"),30);
				if (action.equalsIgnoreCase(Constants.OPENBROWSER)){
					Automate.DeveloperLog.info("Openbrowser");
					browser=xls_reader.getCellData(Constants.CONFIG,Constants.BROWSER,2);
					driver= Webelement_Methods.openBrowser(xls_reader.getCellData(Constants.CONFIG,Constants.BROWSER,2));
				}
				else if(action.equalsIgnoreCase(Constants.NAVIGATE))		{
					String Url= xls_reader.getCellData(Constants.CONFIG,Constants.URL,2);
					driver= Webelement_Methods.navigate(driver,Url);
					String validation=test_reader.getCellData(Constants.TESTSTEPS, Constants.VALIDATION,i);
					Automate.DeveloperLog.info("Validation  " +validation);
					if(!validation.equals(null))
					{
						String validation_xpath=readORMaster(validation);
						Automate.DeveloperLog.info("Validation xpath "+validation_xpath);
						if(!validation_xpath.equals(null))
						WaitTool.waitForElementPresent(driver, By.xpath(validation_xpath),20);
					}
					
				}
				else if(action.equalsIgnoreCase(Constants.MOUSEOVER)){
					try
					{
					WebElement menu=driver.findElement(By.xpath(xpath));
					Automate.DeveloperLog.info(menu);
					Actions action1=new Actions(driver);
					action1.moveToElement(menu).perform();
					String validation=test_reader.getCellData(Constants.TESTSTEPS, Constants.VALIDATION,i);
					Automate.DeveloperLog.info("Validation    " +validation);
					if(!validation.equals(null))
					{
						String validation_xpath=readORMaster(validation);
						Automate.DeveloperLog.info("Validation xpath    "+validation_xpath);
						if(!validation_xpath.equals(null))
						WaitTool.waitForElementPresent(driver, By.xpath(validation_xpath),20);
					}
					Automate.DeveloperLog.info("Wait over");
					Automate.DeveloperLog.info("MOUSEOVER");
					}
					catch(Exception e)
					{
						Automate.ErrorLog.error("Exception caught"+e);
						//APP_LOGS.error("Exception caught"+e);
					}
				}
					else if(action.equalsIgnoreCase(Constants.CLICK)){
				try
				{
					// Function call modified by Aruna @ May 23 2016
					String Button_Text=driver.findElement(By.xpath(xpath)).getText();
					Webelement_Methods.Click(driver,xpath);
					System.out.println("Button Text is"+Button_Text);
					
					if(Button_Text.contains("New") && driver.findElement(By.xpath("//button[@class='imp-button-div-hs-ws-prim ok']")).isDisplayed())
					{
						driver.findElement(By.xpath("//button[@class='imp-button-div-hs-ws-prim ok']")).click();
					}
				}
					catch(Exception e)
					{
						Automate.ErrorLog.error("Exception caught"+e);
					}
					String validation=test_reader.getCellData(Constants.TESTSTEPS, Constants.VALIDATION,i);
					Automate.DeveloperLog.info("Validation  " +validation);
					String validation_xpath=readORMaster(validation);
					Automate.DeveloperLog.info("Validation xpath "+validation_xpath);
					if(!validation.equals(null))
					{
						if(validation.equalsIgnoreCase("button_CreditScore"))
						{
							WaitTool.waitForElementClickable(driver, By.xpath(validation_xpath),120);
							
								WebElement elementcredit=driver.findElement(By.xpath("//*[@id='CreditTab']/a/div"));
								if(!(elementcredit.isSelected()))
								{
									Automate.DeveloperLog.info("Inside accept");
								driver=RunCredit.REO(driver,scenario);
								}
						}
						else
						{
						if(!validation_xpath.equals(null))
						WaitTool.waitForElementClickable(driver, By.xpath(validation_xpath),360);
						}
						}
					
				}
				/*else if (action.equalsIgnoreCase(Constants.CHECK))
				{
					//Modified by Shirisha on 21/05/2016 Start
					Webelement_Methods.Select_The_Checkbox(driver.findElement(By.xpath(xpath)));
				}
				else if(action.equalsIgnoreCase(Constants.UNCHECK)){
					Webelement_Methods.DeSelect_The_Checkbox(driver.findElement(By.xpath(xpath)));
				}*/
				else if(action.equalsIgnoreCase(Constants.CHECK))
				{
					Webelement_Methods.Checkbox(driver.findElement(By.xpath(xpath)));
				}
					else if(action.equalsIgnoreCase(Constants.TYPE))
				{       // Function call written by Aruna @ May 23 2016
						Webelement_Methods.enterText(driver.findElement(By.xpath(xpath)),data);
				}
					else if (action.equalsIgnoreCase(Constants.DROP_SELECT)){
					// Function call written by Satya @ April 30 2016
					Webelement_Methods.selectElementByVisibleTextMethod(driver.findElement(By.xpath(xpath)),data);
				}
				else if(action.equalsIgnoreCase("ClickNext"))
				{
					// Function call modified by Aruna @ May 23 2016
					Webelement_Methods.ClickNext(driver);
					String validation=test_reader.getCellData("TESTSTEPS", "VALIDATION",i);
					Automate.DeveloperLog.info("Validation  " +validation);
					WaitTool.waitForElementPresent(driver, By.xpath(validation),20);
				}
				else if(action.equalsIgnoreCase(Constants.RADIO_CLICK))
					// Function call written by Aruna @ May 23 2016
				{
				
					driver = Webelement_Methods.SelectRadioYesNo(driver,data,xpath);
				
				}
				else if(action.equalsIgnoreCase(Constants.TYPEZIP))
				{
					// Function call written by Aruna @ May 23 2016
					Webelement_Methods.enterText(driver.findElement(By.xpath(xpath)),data);
				}
				else if(action.equalsIgnoreCase(Constants.TAKE_SCREENSHOT))
				{
					String DestFolder=xls_reader.getCellData(Constants.CONFIG,Constants.SNAPSHOT,2);
					String timeStamp = new SimpleDateFormat("yyyyMMMdd_HHmmss").format(Calendar.getInstance().getTime());
					String Destination = DestFolder+"\\"+"tcid"+tcid+timeStamp+".png";
					Webelement_Methods.takeScreenshotMethod(driver, Destination);
				}
				else if(action.equalsIgnoreCase(Constants.GENERATE_REPORT))
				{
					Thread.sleep(4000);
					ReportWriters.GenerateReport(driver,tcid);
				}
				else if(action.equalsIgnoreCase(Constants.GIVE_DELAY))
						Thread.sleep(5000);
				else if(action.equalsIgnoreCase(Constants.ADD_PRODUCT))
					ReportWriters.AddProduct(driver, lookupcol,data);
				
				else
					Automate.DeveloperLog.info("No Such Action");
				
				
				}	
			
			}
			return driver;
}
  //Function to read ORMaster
  public String readORMaster(String orid)
  {try
  {
	  Xls_Reader or_reader= new Xls_Reader(Constants.OR_PATH);
	  Automate.DeveloperLog.info("Inside xpath");
	  String xpath=null;
	  for(int k=2;k<=or_reader.getRowCount(Constants.OR_MASTER);k++)
			
	   {
		//System.out.println("Inside xpath loop");
		if(orid.equalsIgnoreCase(or_reader.getCellData(Constants.OR_MASTER, Constants.ELEMENT, k)))
			{
				
				xpath= or_reader.getCellData(Constants.OR_MASTER,Constants.XPATH,k);
				//System.out.println("value of action"+action);
				Automate.DeveloperLog.info("value of xpath"+xpath);
				break;
			}
		} 
	  return xpath;
	  
  }catch(Exception e)
  {
	  return "";
  }
  }
  
  
  //Function to read Datasheet
  public String readData(Xls_Reader test_reader,String lookupsheet,String lookupcol,int rownum)
  {
	  String data=null;
	  for(int j=2;j<=test_reader.getRowCount(lookupsheet);j++)
		{
			String execute=test_reader.getCellData(lookupsheet, "RunMode", j);
			//Automate.DeveloperLog.info("value of execute  "+execute);
			if(execute.equals("Y"))
			{
				//System.out.println(test_reader.getCellData(lookupsheet, "FirstName", j));
				data=test_reader.getCellData(lookupsheet, lookupcol, j);
				
				Automate.DeveloperLog.info("value of data   "+data);
				break;
				//System.out.println("Row count of OR_MASTER "+or_reader.getRowCount("OR_MASTER"));
			}
		}
	  return data;
  }
}

